﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class KJWindowSetButtonSoundEditor : EditorWindow
{
    const string s_audioClipButtonhKey = "KJWindowSetButtonSoundEditor.s_soundPathKey";
    const string s_inHierarchyKey = "KJWindowSetButtonSoundEditor.s_inHierarchyKey";
    const string s_addIfNotExistsUIPlaySoundKey = "KJWindowSetButtonSoundEditor.s_addIfNotExistsUIPlaySoundKey";
    const string s_usingKJCustomPlaySoundKey = "KJWindowSetButtonSoundEditor.s_usingKJCustomPlaySoundKey";

    AudioClip audioClipButton = null;
    GameObject goTarget = null;
    bool inHierarchy = true;
    bool addIfNotExistsUIPlaySound = true;
    bool usingKJCustomPlaySound = true;

    UIPlaySound.Trigger uiPlaySoundTrigger = UIPlaySound.Trigger.OnClick;


    [MenuItem("Window/KJ/NGUI/Open Set Button and Toggle Sound", priority = -10)]
    static void Open()
    {
        KJWindowSetButtonSoundEditor window = EditorWindow.GetWindow<KJWindowSetButtonSoundEditor>();
        window.Initialize();
    }

    void Initialize()
    {
        string soundPath = UnityEditor.EditorPrefs.GetString(s_audioClipButtonhKey, string.Empty);
        if (false == string.IsNullOrEmpty(soundPath))
        {
            audioClipButton = Resources.LoadAssetAtPath<AudioClip>(soundPath);
        }

        inHierarchy = UnityEditor.EditorPrefs.GetBool(s_inHierarchyKey, true);
        addIfNotExistsUIPlaySound = UnityEditor.EditorPrefs.GetBool(s_addIfNotExistsUIPlaySoundKey, true);
        usingKJCustomPlaySound = UnityEditor.EditorPrefs.GetBool(s_usingKJCustomPlaySoundKey, true);
    }

    void OnProceed()
    {
        if (null == audioClipButton || null == goTarget)
        {
            return;
        }

        System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
        stringBuilder.Append("On Set Button Sound\n");
        stringBuilder.Append(string.Format("audioClip = {0}, inHierarchy = {1}, addIfNotExistsUIPlaySound = {2}, usingKJCustomPlaySound = {3}\n", audioClipButton.name, inHierarchy, addIfNotExistsUIPlaySound, usingKJCustomPlaySound));

        UIButton[] uiButtons = goTarget.GetComponentsInChildren<UIButton>(true);

        stringBuilder.Append(string.Format("*** Button Count = {0}\n", uiButtons.Length));

        foreach (UIButton button in uiButtons)
        {
            UIPlaySound uiPlaySound = button.gameObject.GetComponent<UIPlaySound>();
            if (null == uiPlaySound)
            {
                if (true == usingKJCustomPlaySound)
                {
                    uiPlaySound = button.gameObject.AddComponent<KJCustomPlaySound>();
                }
                else
                {
                    uiPlaySound = button.gameObject.AddComponent<UIPlaySound>();
                }

                stringBuilder.Append(string.Format("{0} {{\n", button.name));
                stringBuilder.Append("Add UIPlaySound\n");
                stringBuilder.Append("}\n");
            }
            else
            {
                if (true == usingKJCustomPlaySound && false == (uiPlaySound is KJCustomPlaySound))
                {
                    Object.DestroyImmediate(uiPlaySound);
                    uiPlaySound = button.gameObject.AddComponent<KJCustomPlaySound>();

                    stringBuilder.Append(string.Format("{0} {{\n", button.name));
                    stringBuilder.Append("Change UIPlaySound\n");
                    stringBuilder.Append("}\n");
                }
                else
                {
                    stringBuilder.Append(string.Format("{0} {{}}\n", button.name));
                }
            }

            uiPlaySound.audioClip = audioClipButton;
            uiPlaySound.trigger = uiPlaySoundTrigger;
        }

        UIToggle[] uiToggles = goTarget.GetComponentsInChildren<UIToggle>(true);

        stringBuilder.Append(string.Format("*** Toggle Count = {0}\n", uiToggles.Length));

        foreach (UIToggle toggle in uiToggles)
        {
            UIPlaySound uiPlaySound = toggle.gameObject.GetComponent<UIPlaySound>();
            if (null == uiPlaySound)
            {
                if (true == usingKJCustomPlaySound)
                {
                    uiPlaySound = toggle.gameObject.AddComponent<KJCustomPlaySound>();
                }
                else
                {
                    uiPlaySound = toggle.gameObject.AddComponent<UIPlaySound>();
                }

                stringBuilder.Append(string.Format("{0} {{\n", toggle.name));
                stringBuilder.Append("Add UIPlaySound\n");
                stringBuilder.Append("}\n");
            }
            else
            {
                if (true == usingKJCustomPlaySound && false == (uiPlaySound is KJCustomPlaySound))
                {
                    Object.DestroyImmediate(uiPlaySound);
                    uiPlaySound = toggle.gameObject.AddComponent<KJCustomPlaySound>();

                    stringBuilder.Append(string.Format("{0} {{\n", toggle.name));
                    stringBuilder.Append("Change UIPlaySound\n");
                    stringBuilder.Append("}\n");
                }
                else
                {
                    stringBuilder.Append(string.Format("{0} {{}}\n", toggle.name));
                }
            }

            uiPlaySound.audioClip = audioClipButton;
            uiPlaySound.trigger = uiPlaySoundTrigger;
        }

        Debug.Log(stringBuilder.ToString());
    }

    void OnGUI()
    {
        audioClipButton = EditorGUILayout.ObjectField("Button Sound", audioClipButton, typeof(AudioClip), false) as AudioClip;
        goTarget = EditorGUILayout.ObjectField("Target", goTarget, typeof(GameObject), true) as GameObject;

        uiPlaySoundTrigger = (UIPlaySound.Trigger)EditorGUILayout.EnumPopup("UIPlaySound Trigger", uiPlaySoundTrigger);

        inHierarchy = EditorGUILayout.Toggle("In Hierarchy", inHierarchy);
        addIfNotExistsUIPlaySound = EditorGUILayout.Toggle("Add If Not Exists UIPlaySound", addIfNotExistsUIPlaySound);
        usingKJCustomPlaySound = EditorGUILayout.Toggle("Using KJCustomPlaySound", usingKJCustomPlaySound);

        EditorGUILayout.BeginHorizontal();
        {
            if (true == GUILayout.Button("Proceed"))
            {
                this.OnProceed();

//				if(true == Application.isPlaying)
//				{
//					GameManager mGameManager = GameObject.Find(ObjectNames.m_DataManager).GetComponent<GameManager>();
//
//					Debug.Log(mGameManager);
//				}
            }
        }
        EditorGUILayout.EndHorizontal();

        if (true == GUI.changed)
        {
            string path = AssetDatabase.GetAssetPath(audioClipButton);

            UnityEditor.EditorPrefs.SetString(s_audioClipButtonhKey, path);

            UnityEditor.EditorPrefs.SetBool(s_inHierarchyKey, inHierarchy);
            UnityEditor.EditorPrefs.SetBool(s_addIfNotExistsUIPlaySoundKey, addIfNotExistsUIPlaySound);
            UnityEditor.EditorPrefs.SetBool(s_usingKJCustomPlaySoundKey, usingKJCustomPlaySound);
        }
    }
}
