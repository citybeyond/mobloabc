﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class KJWindowSetButtonColorEditor : EditorWindow
{
    const string s_normalColorRedkey = "KJWindowSetButtonColorEditor.s_normalColorRedkey";
    const string s_normalColorGreenkey = "KJWindowSetButtonColorEditor.s_normalColorGreenkey";
    const string s_normalColorBluekey = "KJWindowSetButtonColorEditor.s_normalColorBluekey";

    const string s_hoverColorRedkey = "KJWindowSetButtonColorEditor.s_hoverColorRedkey";
    const string s_hoverColorGreenkey = "KJWindowSetButtonColorEditor.s_hoverColorGreenkey";
    const string s_hoverColorBluekey = "KJWindowSetButtonColorEditor.s_hoverColorBluekey";

    const string s_pressedColorRedkey = "KJWindowSetButtonColorEditor.s_pressedColorRedkey";
    const string s_pressedColorGreenkey = "KJWindowSetButtonColorEditor.s_pressedColorGreenkey";
    const string s_pressedColorBluekey = "KJWindowSetButtonColorEditor.s_pressedColorBluekey";

    const string s_includePrefabskey = "KJWindowSetButtonColorEditor.includePrefabs";


    Color normalColor = Color.white;
    Color hoverColor = Color.white;
    Color pressedColor = Color.white;

    bool includePrefabs = false;

    [MenuItem("Window/KJ/NGUI/Open Set Button Color", priority = -10)]
    static void Open()
    {
        KJWindowSetButtonColorEditor window = EditorWindow.GetWindow<KJWindowSetButtonColorEditor>();
        window.Initialize();
    }

    void Initialize()
    {
        if (UnityEditor.EditorPrefs.HasKey(s_normalColorRedkey))
        {
            normalColor.r = UnityEditor.EditorPrefs.GetFloat(s_normalColorRedkey, 0);
            normalColor.g = UnityEditor.EditorPrefs.GetFloat(s_normalColorGreenkey, 0);
            normalColor.b = UnityEditor.EditorPrefs.GetFloat(s_normalColorBluekey, 0);

            hoverColor.r = UnityEditor.EditorPrefs.GetFloat(s_hoverColorRedkey, 0);
            hoverColor.g = UnityEditor.EditorPrefs.GetFloat(s_hoverColorGreenkey, 0);
            hoverColor.b = UnityEditor.EditorPrefs.GetFloat(s_hoverColorBluekey, 0);

            pressedColor.r = UnityEditor.EditorPrefs.GetFloat(s_pressedColorRedkey, 0);
            pressedColor.g = UnityEditor.EditorPrefs.GetFloat(s_pressedColorGreenkey, 0);
            pressedColor.b = UnityEditor.EditorPrefs.GetFloat(s_pressedColorBluekey, 0);
        }

        includePrefabs = UnityEditor.EditorPrefs.GetBool(s_includePrefabskey, false);
    }

    void OnGUI()
    {
        normalColor = EditorGUILayout.ColorField("Normal Color", normalColor);
        hoverColor = EditorGUILayout.ColorField("Hover Color", normalColor);
        pressedColor = EditorGUILayout.ColorField("Pressed Color", normalColor);

        includePrefabs = EditorGUILayout.ToggleLeft("Include prefabs", includePrefabs);

        if (true == GUILayout.Button("Proceed"))
        {
            UIButton[] arrButtons = Resources.FindObjectsOfTypeAll<UIButton>();

            foreach (UIButton button in arrButtons)
            {
                if (false == includePrefabs)
                {
                    if (PrefabUtility.GetPrefabParent(button) == null && PrefabUtility.GetPrefabObject(button) != null)
                    {
                        continue;
                    }
                }

                button.defaultColor = normalColor;
                button.hover = hoverColor;
                button.pressed = pressedColor;

                //if (null != button.normalSprite && button.normalSprite.Contains("_off"))
                //{
                //    button.normalSprite = button.normalSprite.Replace("_off", "_on");
                //}
                //if (null != button.pressedSprite && button.pressedSprite.Contains("_on"))
                //{
                //    button.pressedSprite = button.pressedSprite.Replace("_on", "_off");
                //}
            }
        }

        if (true == GUI.changed)
        {
            UnityEditor.EditorPrefs.SetFloat(s_normalColorRedkey, normalColor.r);
            UnityEditor.EditorPrefs.SetFloat(s_normalColorGreenkey, normalColor.g);
            UnityEditor.EditorPrefs.SetFloat(s_normalColorBluekey, normalColor.b);

            UnityEditor.EditorPrefs.SetFloat(s_hoverColorRedkey, hoverColor.r);
            UnityEditor.EditorPrefs.SetFloat(s_hoverColorGreenkey, hoverColor.g);
            UnityEditor.EditorPrefs.SetFloat(s_hoverColorBluekey, hoverColor.b);

            UnityEditor.EditorPrefs.SetFloat(s_pressedColorRedkey, pressedColor.r);
            UnityEditor.EditorPrefs.SetFloat(s_pressedColorGreenkey, pressedColor.g);
            UnityEditor.EditorPrefs.SetFloat(s_pressedColorBluekey, pressedColor.b);
        }
    }
}
