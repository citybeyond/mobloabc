﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class KJWindowSetLabelFontEditor : EditorWindow
{
    const string s_selectedFontkey = "KJWindowSetLabelFontEditor.s_selectedFontkey";
    const string s_includePrefabskey = "KJWindowSetLabelFontEditor.includePrefabs";


    Font selectedFont = null;

    bool includePrefabs = false;

    [MenuItem("Window/KJ/NGUI/Open Set Label Font", priority = -10)]
    static void Open()
    {
        KJWindowSetLabelFontEditor window = EditorWindow.GetWindow<KJWindowSetLabelFontEditor>();
        window.Initialize();
    }

    void Initialize()
    {
        string fontPath = UnityEditor.EditorPrefs.GetString(s_selectedFontkey, string.Empty);
        if (false == string.IsNullOrEmpty(fontPath))
        {
            selectedFont = Resources.LoadAssetAtPath<Font>(fontPath);
        }

        includePrefabs = UnityEditor.EditorPrefs.GetBool(s_includePrefabskey, false);
    }

    void OnGUI()
    {
        selectedFont = EditorGUILayout.ObjectField("Font", selectedFont, typeof(Font), false) as Font;
        if (null == selectedFont)
        {
            EditorUtility.DisplayDialog("Error", "Please, Select font and proceed", "OK");
            return;
        }

        includePrefabs = EditorGUILayout.ToggleLeft("Include prefabs", includePrefabs);

        if (true == GUILayout.Button("Proceed"))
        {
            UILabel[] arrLabels = Resources.FindObjectsOfTypeAll<UILabel>();

            foreach (UILabel label in arrLabels)
            {
                if (false == includePrefabs)
                {
                    if (PrefabUtility.GetPrefabParent(label) == null && PrefabUtility.GetPrefabObject(label) != null)
                    {
                        continue;
                    }
                }

                label.trueTypeFont = selectedFont;
            }
        }

        if (true == GUI.changed)
        {
            string path = AssetDatabase.GetAssetPath(selectedFont);

            UnityEditor.EditorPrefs.SetString(s_selectedFontkey, path);
        }
    }
}
