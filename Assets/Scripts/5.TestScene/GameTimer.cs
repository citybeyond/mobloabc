﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	public CtrTest ctrTest;
	public Slider sliderBar;
	public AudioSource soundClock;

	public GameObject objSlideHandle;
	
	public GameObject[] objStars;
	
	private float fStartTime;
	private float fCurrentTime;
	private bool bStartTimer;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if( bStartTimer )
		{
			fCurrentTime = Time.time - fStartTime;
			if( fCurrentTime >= 20f)
			{
				fCurrentTime = 20f;
				sliderBar.value = 1f;
				StopTimer();
				ctrTest.WrongWord();
			}
			else
			{
				sliderBar.value = fCurrentTime*0.05f;
				if( sliderBar.value >= 0.25f && sliderBar.value < 0.5f )
				{
					if( objStars[0].activeSelf )
					{
						objStars[0].SetActive(false);
						SoundManager.Instance.PlayEffect("eff_star");
					}
				}
				else if( sliderBar.value >= 0.5f&& sliderBar.value < 0.75f  )
				{
					if( objStars[1].activeSelf )
					{
						objStars[1].SetActive(false);
						SoundManager.Instance.PlayEffect("eff_star");
					}
				}
				else if( sliderBar.value >= 0.75f )
				{
					if( objStars[2].activeSelf )
					{
						objStars[2].SetActive(false);
						SoundManager.Instance.PlayEffect("eff_star");
					}
				}
			}
		}
	}

	public void StopTimer()
	{
		bStartTimer = false;
		soundClock.Stop();
	}
	public void ResetTimer()
	{
		sliderBar.value = 0f;
		objStars[0].SetActive(true);
		objStars[1].SetActive(true);
		objStars[2].SetActive(true);
		objSlideHandle.SetActive(false);
	}

	public void StartTimer()
	{
		bStartTimer = true;
		fStartTime = Time.time;
		soundClock.Play();
		
		sliderBar.value = 0f;
		objStars[0].SetActive(true);
		objStars[1].SetActive(true);
		objStars[2].SetActive(true);
		
		objSlideHandle.SetActive(true);
	}
}
