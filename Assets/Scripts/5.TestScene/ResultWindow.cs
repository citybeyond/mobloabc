﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ResultWindow : MonoBehaviour {

	public CtrTest crtTest;
	public Text textScore;
	public Text[] textRecords;
	public GameObject objResult;
	// Use this for initialization
	private List<int> listRecord = new List<int>();
	void Start () {
	
	}

	public void ShowResult()
	{
		SoundManager.Instance.PlayEffect("eff_test_score");
		textScore.text = "<color=#ffc966>"+World.Instance.iTestScore.ToString("n0")+"</color>점";
		objResult.SetActive(true);

		listRecord.Clear();
		listRecord.Add(World.Instance.iTestScore);
		for(int i = 0 ; i < 5; ++i )
		{
			listRecord.Add(PlayerPrefs.GetInt("Record"+i.ToString(),0));
		}
		listRecord.Sort();
		listRecord.Reverse();
		int _record = 0;
		for( int _index = 0 ; _index < 5; ++_index )
		{
			if( _index < listRecord.Count )
				_record = listRecord[_index];
			else
				_record = 0;
			
			PlayerPrefs.SetInt("Record"+_index.ToString(),_record);
			textRecords[_index].text = _record.ToString("n0")+"점";

		}

			//textRecords[i].text = PlayerPrefs.GetInt("Record0"+i.ToString(),0).ToString("n0")+"점";
	}

	public void ClickRusume()
	{
		objResult.SetActive(false);
		crtTest.InitTest();
	}
	public void ClickHome()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.LoadLevel("3.MainScene");
	}
}
