﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class CtrTest : MonoBehaviour {
	
	public CtrAlphabet ctrAlphabet;
	public CtrGameCharacter ctrCharacter;
	
	public GameObject objReady;
	public GameObject objStart;
	public ResultWindow scriptResult;
	
	public Button btnOK;
	public GameTimer timer;
	public Image imgPic;
	public Text textAlphabet01;
	public Text textAlphabet02;
	public Text textStep;
	public GameObject objCorrect;
	public GameObject objWrong;
	public GameObject objNotice;
	
	public List<string> strTestWords  = new List<string>();
	public List<string> strTestWordTypes  = new List<string>();


	private int totalTestCount = 10;
	private int indexTest = 0;

	private List<int> listTypeNumber = new List<int>();
	private List<WordItem> listWordItems = new List<WordItem>();

	public static CtrTest Instance;

	private bool bProcessNFCTag = false;

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	IEnumerator Start () {
		
		SoundManager.Instance.PlayBGM("S_SLP01_Tr12_01 A Sunny Day",0.25f);
#if UNITY_EDITOR
		btnOK.interactable = true;
#else
		btnOK.interactable = false;
#endif

		bool _bDetailCard = World.Instance.bDetailCardTestMode;
		yield return new WaitForSeconds(0.5f);
		if( !World.Instance.bDetailCardTestMode )
			GenerateWordList();
		else 
			GenerateWordList_DetailCard();
		//SetWord(indexTest);

		//Request Moblo MemoryRead
		KJPluginMethods.methods.bluetooth.requestCommand(
			KJBluetoothValues.BluetoothCommands.MemoryRead);

	}

	public void InitTest()
	{
		indexTest = 0;
		GenerateWordList();
		SetWord(indexTest);
	}

	public void ClickCloseReady()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objReady.SetActive(false);
		objStart.GetComponent<CanvasGroup>().DOFade(1f,0.5f);

		SetWord(indexTest);

		ctrCharacter.objBalloon.SetActive(false);
		ctrCharacter.bDisappearBalloon = true;
	}



	
	public void ClickHome()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.LoadLevel("3.MainScene");
	}

	
	public void ClickCorrectWord()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		CorrectWord();
	}
	public void ClickWrongWord()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		WrongWord();
	}
	
	public void CorrectWord()
	{
		if( !bProcessNFCTag ) return;
		bProcessNFCTag = false;

		timer.StopTimer();

		//Calculate Score
		World.Instance.iTestScore += (int)(1000f*(1f-timer.sliderBar.value));

		SoundManager.Instance.PlayEffect("eff_correct");
		objCorrect.transform.DOScale(1f,0.5f).SetEase(Ease.OutExpo);

		ctrCharacter.PlayTalk(Random.Range(0,8));
		ctrAlphabet.PlayAnimCorrectWord();
		StartCoroutine(CorrectWordCo());
		
		World.Instance.SaveCorrectWord_Test(
			ctrAlphabet.strTypeWord.ToUpper() ,ctrAlphabet.strWord.ToUpper());
		
	}
	IEnumerator CorrectWordCo()
	{
		yield return new WaitForSeconds(SoundManager.Instance.lengthSound);
		string _type = ctrAlphabet.strTypeWord.ToLower();
		string _sound = "nar_english_"+_type+_type+_type
			+"_"+ctrAlphabet.strWord.ToLower();
		//Debug.Log ("CorrectWord "+_sound);
		SoundManager.Instance.PlayEffect(_sound,1f);
		yield return new WaitForSeconds(SoundManager.Instance.lengthSound);
		
		StartCoroutine(NextWordCo());
	}
	public void WrongWord()
	{
		if( !bProcessNFCTag ) return;
		bProcessNFCTag = false;

		timer.StopTimer();
		SoundManager.Instance.PlayEffect("eff_wrong");
		objWrong.transform.DOScale(1f,0.5f).SetEase(Ease.OutExpo);

		ctrCharacter.PlayTalk(Random.Range(8,12));
		ctrAlphabet.PlayAnimWrongWord();
		StartCoroutine(WrongWordCo());
	}
	
	IEnumerator WrongWordCo()
	{
		yield return new WaitForSeconds(ctrCharacter.lengthSound+0.5f);
		ctrAlphabet.StopAnimWrongWord();
//		yield return new WaitForSeconds(0.3f);
//		ctrAlphabet.StopAnimWrongWord();
		
		StartCoroutine(NextWordCo());
	}
	
	IEnumerator NextWordCo()
	{
		//yield return new WaitForSeconds(1.5f);
		
		ctrAlphabet.PlayAnimClose();
		imgPic.GetComponent<CanvasGroup>().DOFade(0f,1f).SetDelay(0.5f);
		yield return new WaitForSeconds(1.5f);
		objCorrect.transform.localScale = Vector3.zero;
		objWrong.transform.localScale = Vector3.zero;
		
		++indexTest;
		if( indexTest != totalTestCount )
			SetWord(indexTest);
		else
		{
			//Finish
			if( World.Instance.bDetailCardTestMode )
			{
				World.Instance.LoadLevel("3.MainScene");
			}
			else
				scriptResult.ShowResult();
		}
		
	}



	
	void SetWord( int _indexTest )
	{
		//ctrAlphabet.GetAlphabet("DINOSAUR","D");

		ctrAlphabet.GetAlphabet(strTestWords[_indexTest],strTestWordTypes[_indexTest] );
		string _name = "Images/ill_english_"+ctrAlphabet.strTypeWord.ToLower()
			+"_"+ctrAlphabet.strWord.ToLower();
		//Debug.Log ("SetWord "+_name);
		imgPic.sprite = Resources.Load<Sprite>(_name);// as Sprite;
		
		imgPic.GetComponent<CanvasGroup>().DOFade(1f,1f).SetDelay(1f);
		textAlphabet01.text = ctrAlphabet.strTypeWord.ToUpper();
		textAlphabet02.text = ctrAlphabet.strTypeWord.ToLower();

		
		textStep.text = (indexTest+1).ToString()+"/"+totalTestCount.ToString();

		StartCoroutine(StartTimerCo());

	}

	IEnumerator StartTimerCo()
	{
		timer.ResetTimer();
		yield return new WaitForSeconds(1.5f);
		timer.StartTimer();
		bProcessNFCTag = true;
		SoundManager.Instance.PlayEffect("eff_alphabet_show");
	}

	
	void GenerateWordList_DetailCard()
	{
		World.Instance.iTestScore = 0;
		strTestWords.Clear();
		strTestWordTypes.Clear();
		listWordItems.Clear();

		strTestWordTypes.Add (World.Instance.strType_DetailCard);
		strTestWords.Add ( World.Instance.strWord_DetailCard);
		totalTestCount = 1;

	}
	void GenerateWordList()
	{
		World.Instance.iTestScore = 0;
		strTestWords.Clear();
		strTestWordTypes.Clear();
		listWordItems.Clear();

		int i = 0;
		int _count = World.Instance.wordItems_Success.Length;
		int _totalCountSuccessWord = 0;
		for( i = 0; i < World.Instance.wordItems_Success.Length; ++i )
		{
			if( World.Instance.wordItems_Success[i].listWords.Count != 0 )
			{
				WordItem _tempItem = new WordItem();
				_tempItem.strWordType = World.Instance.wordItems_Success[i].strWordType;
				for( int j = 0; j < World.Instance.wordItems_Success[i].listWords.Count; ++j )
				{
					_tempItem.listWords.Add(World.Instance.wordItems_Success[i].listWords[j]);
				}
				//_tempItem.listWords = World.Instance.wordItems_Success[i].listWords;
				_totalCountSuccessWord += _tempItem.listWords.Count;

				listWordItems.Add(_tempItem);
				listTypeNumber.Add(i);
			}
		}
		listTypeNumber.Clear();
		for( i = 0; i < listWordItems.Count; ++i)
				listTypeNumber.Add(i);

		if( _totalCountSuccessWord > 10 )
			totalTestCount = 10;
		else
			totalTestCount = _totalCountSuccessWord;
		indexTest = 0;

		textStep.text = (indexTest+1).ToString()+"/"+totalTestCount.ToString();

		int _indexWordType = 0;
		int _indexWord = 0;
		string _strTestWord;
		//Debug.Log( "totalTestCount "+totalTestCount);

		for( i = 0; i < totalTestCount; ++i )
		{
			if( listTypeNumber.Count != 0 )
			{
				//Debug.Log( "i "+i);
				int _temp = Random.Range(0,listTypeNumber.Count);
				//Debug.Log( "listTypeNumber.Count "+listTypeNumber.Count);
				//Debug.Log( "_temp "+_temp);

				_indexWordType = listTypeNumber[_temp];
				//Debug.Log( "_indexWordType "+_indexWordType);

				_indexWord = Random.Range(0,listWordItems[_indexWordType].listWords.Count);
				_strTestWord = listWordItems[_indexWordType].listWords[_indexWord];
				strTestWordTypes.Add (listWordItems[_indexWordType].strWordType);
				strTestWords.Add (_strTestWord);

				listWordItems[_indexWordType].listWords.Remove(_strTestWord);

				ResetIndexTypeNumberList(_indexWordType);
			}

		}
	}
	void ResetIndexTypeNumberList(int _preIndex)
	{
		listTypeNumber.Clear();
		for( int _index = 0; _index < listWordItems.Count; ++_index)
		{
			if( _preIndex != _index )
			{
				if( listWordItems[_index].listWords.Count != 0 )
					listTypeNumber.Add(_index);
			}
		}
	}
	
	public void ReceiveData( string _rawdata)
	{
		Debug.Log ("ReceiveData "+_rawdata);
		char[] _data = new char[17];
		_data = _rawdata.ToCharArray();
		
		int _x = int.Parse(_data[2].ToString())-1;
		int _y = int.Parse(_data[3].ToString())-1;
		
		int _state = int.Parse(_data[6].ToString());	
		
		int _idTag = int.Parse(_data[13].ToString())*100 
			+ int.Parse(_data[14].ToString())*10 +  int.Parse(_data[15].ToString());
		int _tagstate = int.Parse(_data[16].ToString());
		
		
		if( _data[5].ToString() == "N" )//NFC Block Check
		{
			if( _state == 1 )//Add
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetNFCBlockImage();
				//bNFCBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
				btnOK.interactable = true;
				objNotice.SetActive(false);
				Time.timeScale = 1f;
			}
			else if(_state == 3)// Hold
			{
				if( _tagstate == 1 )
				{
					if( bProcessNFCTag )
					{
						string _strTag = World.Instance.GetAlphabetFromTag(_idTag);
						
						if( strTestWordTypes[indexTest].ToUpper() == _strTag.ToUpper())
						{
							CorrectWord();
						}
						else
						{
							WrongWord();
						}
					}
				}
				
				
			}
		}
		else if( _data[5].ToString() == "T")//Touch Block Check
		{
			if( _state == 1 )//Add
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetTouchBlockImage();
				//bTouchBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
			}
			
			if(  _state == 3)//Add, Hold
			{
				//Touch Input
				//ClickTouch();
			}
		}
		else
		{
			if(_state == 2) // Remove
			{
				MgrGameScene.Instance.blocks[_y].block[_x].DisableBlockImage();
				if( MgrGameScene.Instance.blocks[_y].block[_x].bNFCMode )
				{
					//bNFCBlock = false;
					btnOK.interactable = false;

					SoundManager.Instance.StopEffectSound();
					ctrCharacter.PlayTalk(19);
					objNotice.SetActive(true);
					Time.timeScale = 0f;
				}
				else
				{
					//bTouchBlock = false;
				}
			}
			else
			{
				//Error
				//SoundManager.Instance.PlayEffect("eff_block_miss");
			}
		}
		
		
	}


}
