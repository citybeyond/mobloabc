﻿using UnityEngine;
using System.Collections;

public class MainScene : KJ.Model.KJAScene, KJ.Handler.KJMessageHandler.IMessageReceiver
{



    protected override void DoInitailize()
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.SetView(beginView));
        BlockDataManager.Construct();

        this._RegistMessage();
    }
    protected override void DoFinalizeThis()
    {
        this._UnregistMessage();
    }


    #region On Message
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {
	}
	public void OnClickButtonQuitPopup()
	{
		Application.Quit();
	}

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message
}
