﻿using UnityEngine;
using System.Collections.Generic;

public class MainBluetoothView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    public UILabel m_LabelVersion;
    [SerializeField]
    private UIGrid uiGridList = null;

    [SerializeField]
    private GameObject prefabListItem = null;

    [SerializeField]
    private UIButton uiButtonSearch = null;

    [SerializeField]
    private UIButton uiButtonCancel = null;

    List<BluetoothSerialize.FoundDevice> listFoundDevices = new List<BluetoothSerialize.FoundDevice>();

    public GameObject KJBluetoothReceiver; 

    protected override void Start()
    {
        base.Start();

        NGUITools.SetActiveSelf(goRootView, false);
    }

    protected override void DoLoadData()
    {
        listFoundDevices = new List<BluetoothSerialize.FoundDevice>(KJPluginMethods.methods.bluetooth.getBondedDevices());
        this.CreateListItem();
    }
    protected override void DoSaveData()
    {
        
    }

    protected override void DoOpen()
    {

        //s_bundleVersion = PlayerSettings.bundleVersion;
        //s_bundleCode = PlayerSettings.Android.bundleVersionCode;

        CurrentBundleVersion ver = new CurrentBundleVersion();
        m_LabelVersion.text = "Ver." + ver.version;

        this._RegistMessage();

        NGUITools.SetActiveSelf(goRootView, true);

        if (uiGridList == null)
        {
            uiGridList = goRootView.transform.FindChild("BluetoothList/Scroll View_list/Grid").GetComponent<UIGrid>();
            Debug.Log("DoOpen : " + uiGridList);

        }
        Debug.Log ("DoOpen : " + uiGridList.transform.childCount);
        if (uiGridList.transform.childCount > 0)
        {
            for (int i = 0; i < uiGridList.transform.childCount; i++)
            {
                GameObject go = uiGridList.transform.GetChild(i).gameObject;
                Destroy (go);
            }
        }

        //KJ.Utilities.GameObjectUtility.DestroyAllChildren(uiGridList.gameObject);

        this.AddFoundedDevices();
    }
    protected override void DoClose()
    {
        NGUITools.SetActiveSelf(goRootView, false);

        this._UnregistMessage();
    }

    void AddFoundedDevices()
    {
        for (int i = 0; i < listFoundDevices.Count; ++i)
        {
            this.AddListItem(listFoundDevices[i]);
        }

        uiGridList.repositionNow = true;
    }
    void AddListItem(BluetoothSerialize.FoundDevice i_foundDevice)
    {

        if (uiGridList == null)
        {
            Debug.Log("AddListItem 1: uiGridList null");

            return;
        }

        GameObject item = NGUITools.AddChild(uiGridList.gameObject, prefabListItem);
        Debug.Log("AddListItem 1: " + item);
        item.name = i_foundDevice.address;
        item.GetComponentInChildren<UILabel>().text = i_foundDevice.name;

        EventDelegate.Add(item.GetComponent<UIButton>().onClick, this.OnClickButtonListItem);

        uiGridList.repositionNow = true;

        //GameObject go = NGUITools.AddChild(uiGridList.gameObject, prefabListItem);
        //go.name = i_foundDevice.address;

        //go.GetComponentInChildren<UILabel>().text = i_foundDevice.name;

        //EventDelegate.Add(go.GetComponent<UIButton>().onClick, this.OnClickButtonListItem);
    }

    #region On Message
    void OnMessageFoundDevice(KJ.Message.Bluetooth.FoundDevice i_message)
    {
        if (i_message == null)
        {
            Debug.Log("OnMessageFoundDevice : null");
            return;
        }
        Debug.Log("OnMessageFoundDevice : " + i_message.foundDevice);
        

        this.AddListItem(i_message.foundDevice);
    }
    void OnMessageDiscoveryFinished(KJ.Message.Bluetooth.DiscoveryFinished i_message)
    {
        NGUITools.SetActiveSelf(uiButtonSearch.gameObject, true);
        NGUITools.SetActiveSelf(uiButtonCancel.gameObject, false);
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.FoundDevice>(this.OnMessageFoundDevice);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.DiscoveryFinished>(this.OnMessageDiscoveryFinished);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    int iiii = 0;
    #region On UI
    public void OnClickButtonSearch()
    {
        if (uiGridList.transform.childCount > 0)
        {
            for (int i = 0; i < uiGridList.transform.childCount; i++)
            {
                GameObject go = uiGridList.transform.GetChild(i).gameObject;
                Destroy(go);
            }
        }
        
        //KJ.Utilities.GameObjectUtility.DestroyAllChildren(uiGridList.gameObject);

        this.AddFoundedDevices();

        NGUITools.SetActiveSelf(uiButtonSearch.gameObject, false);
        NGUITools.SetActiveSelf(uiButtonCancel.gameObject, true);

        KJPluginMethods.methods.bluetooth.doDiscovery();


    }
    public void OnClickButtonCancel()
    {
        NGUITools.SetActiveSelf(uiButtonSearch.gameObject, true);
        NGUITools.SetActiveSelf(uiButtonCancel.gameObject, false);

        KJPluginMethods.methods.bluetooth.cancelDiscovery();
    }

    private void OnClickButtonListItem()
    {
        ResultData resultData = new ResultData();
        resultData.str = UIButton.current.name;

        KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName = UIButton.current.GetComponentInChildren<UILabel>().text;
        

        KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(typeof(MainBluetoothConnectingView), resultData);
        KJ.Handler.KJMessageHandler.SendMessage(message);
    }
    #endregion On UI


    #region Condition Debug
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void CreateListItem()
    {
        BluetoothSerialize.FoundDevice foundDevice = new BluetoothSerialize.FoundDevice();
        foundDevice.name = "TEST";
        foundDevice.address = "TEST_ADDRESS";
        listFoundDevices.Add(foundDevice);
    }
    #endregion Condition Debug
}
