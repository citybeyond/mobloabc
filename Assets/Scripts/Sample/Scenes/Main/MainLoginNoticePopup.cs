﻿using UnityEngine;
using System.Collections;

public class MainLoginNoticePopup : MonoBehaviour
{
    WebViewObject webViewObject = null;
    public GUIText status;
    [SerializeField]
    GameObject goNoticePopup = null;

    [SerializeField]
    UILabel uiLabelMessage = null;

    int id = 0;
    System.Action<int> onClickButtonOKCallback = null;

    public void Open(string i_message, int i_id, System.Action<int> i_onClickButtonOKCallback)
    {
        NGUITools.SetActiveSelf(goNoticePopup, true);

        uiLabelMessage.text = i_message;
        id = i_id;
        onClickButtonOKCallback = i_onClickButtonOKCallback;
    }
    public void Close()
    {
        NGUITools.SetActiveSelf(goNoticePopup, false);
    }


    #region On UI
    public void OnClickButtonOK()
    {
        if (null != onClickButtonOKCallback)
        {
            onClickButtonOKCallback(id);
        }
    }
#if TESTOUT
    IEnumerator openFileAndroid(string filename)
    {
        string result;
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
        if (filePath.Contains("://"))
        {
            WWW www = new WWW(filePath);
            yield return www;
            result = www.text;
        }
        else
            result = System.IO.File.ReadAllText(filePath);

        Application.OpenURL(result);
    }
#if !UNITY_WEBPLAYER
	IEnumerator WebViewOpen(string Url)
#else
	void WebViewOpen(string Url)
#endif

    {
        //WebViewObject webViewObject;
        

        webViewObject = GetComponent<WebViewObject>();
        if (webViewObject == null)
        {
            webViewObject =
                (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
            webViewObject.Init((msg) =>
            {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
                status.text = msg;
                status.GetComponent<Animation>().Play();
            });

            webViewObject.SetMargins(5, 100, 5, 5);
        }
        webViewObject.SetVisibility(true);

        switch (Application.platform)
        {
#if !UNITY_WEBPLAYER
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.IPhonePlayer:
            case RuntimePlatform.Android:
                var src = System.IO.Path.Combine(Application.streamingAssetsPath, Url);
                var dst = System.IO.Path.Combine(Application.persistentDataPath, Url);
                var result = "";
                if (src.Contains("://"))
                {
                    var www = new WWW(src);
                    yield return www;
                    result = www.text;
                }
                else
                {
                    result = System.IO.File.ReadAllText(src);
                }
                System.IO.File.WriteAllText(dst, result);
                webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
                if (Application.platform != RuntimePlatform.Android)
                {
                    webViewObject.EvaluateJS(
                        "window.addEventListener('load', function() {" +
                        "	window.Unity = {" +
                        "		call:function(msg) {" +
                        "			var iframe = document.createElement('IFRAME');" +
                        "			iframe.setAttribute('src', 'unity:' + msg);" +
                        "			document.documentElement.appendChild(iframe);" +
                        "			iframe.parentNode.removeChild(iframe);" +
                        "			iframe = null;" +
                        "		}" +
                        "	}" +
                        "}, false);");
                }
                break;
#else
	case RuntimePlatform.OSXWebPlayer:
	case RuntimePlatform.WindowsWebPlayer:
		webViewObject.LoadURL(Url.Replace(" ", "%20"));
		webViewObject.EvaluateJS(
			"parent.$(function() {" +
			"	window.Unity = {" +
			"		call:function(msg) {" +
			"			parent.unityWebView.sendMessage('WebViewObject', msg)" +
			"		}" +
			"	};" +
			"});");
		break;
#endif
        }
    }


    public void OnClickButtonURL()
    {
        string path;
#if UNITY_EDITOR
        path = Application.dataPath + "/StreamingAssets/moblo1.html";
        Application.OpenURL(path);
#elif UNITY_ANDROID
        if (webViewObject != null)
        {
            webViewObject.SetVisibility(false);
            webViewObject = null;
            return;
        }
        StartCoroutine (WebViewOpen("moblo1.html") );

#elif UNITY_IPHONE

#endif

    }



#else



    public void OnClickButtonURL()
    {
        BluetoothManager.Instance.CreateBTPopup("BTPopupHelp");
    }



#endif






    #endregion On UI
}
