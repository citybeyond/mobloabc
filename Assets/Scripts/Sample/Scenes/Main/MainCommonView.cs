﻿using UnityEngine;
using System.Collections.Generic;

using KJ.ExtensionMethods;

public class MainCommonView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver, IBlockDataReceiver
{
    static int s_increamentFrame = 0;
    static int s_timeFrame = 0;


    [SerializeField]
    private GameObject goDummyRoot = null;

    [SerializeField]
    private GameObject[] blockPositionObject = null;

	private GameObject[] blockPositionObject2 = null; //block9
    [SerializeField]
    UITextList uiTextListLog = null;


    [SerializeField]
    GameObject prefabBlockGame = null;

    [SerializeField]
    GameObject prefabBlockRed = null;
    [SerializeField]
    GameObject prefabBlockGreen = null;
    [SerializeField]
    GameObject prefabBlockBlue = null;
    [SerializeField]
    GameObject prefabBlockYellow = null;
    [SerializeField]
    GameObject prefabBlockViolet = null;

    [SerializeField]
    GameObject prefabLongBlockRed = null;
    [SerializeField]
    GameObject prefabLongBlockGreen = null;
    [SerializeField]
    GameObject prefabLongBlockBlue = null;
    [SerializeField]
    GameObject prefabLongBlockYellow = null;
    [SerializeField]
    GameObject prefabLongBlockViolet = null;

    [SerializeField]
    GameObject prefabBlockLed = null;

    [SerializeField]
    GameObject prefabBlockTouch = null;

    [SerializeField]
    GameObject prefabBlockLcd = null;

    [SerializeField]
    GameObject prefabBlockMotor = null;

    [SerializeField]
    GameObject[] prefabItems = null;

    [SerializeField]
    GameObject prefabCharacter = null;

    [SerializeField]
    GameObject prefabSkin = null;

    [SerializeField]
    UILabel uiLabelVersion = null;

    [SerializeField]
    UILabel uiLabelChangeDummyBlockPopupMessage = null;

    [SerializeField]
    UIToggle uiToggleChangeDummyBlockPopupAuto = null;

    [SerializeField]
    UIToggle uiToggleChangeDummyBlockPopupCommandRed = null;

    float startBlockSeconds = 0;
    float startReadAckSeconds = 0;

    int memoryReadCount = 0;

    bool enableBlink = false;
    bool setLedLightAllOn = false;


    string lcdContent = string.Empty;
    int lcdContentIndex = 0;


    protected override void DoLoadData()
    {
        this._RegistMessage();
    }
    protected override void DoSaveData()
    {
        this._UnregistMessage();
    }

    protected override void DoOpen()
    {
        BlockDataManager.blockDataReceiver = this;

        uiLabelVersion.text = string.Format("Version: {0}", new CurrentBundleVersion().version);
		InitBlockPosition ();//block9
    }
    protected override void DoClose()
    {
        BlockDataManager.blockDataReceiver = null;
        BlockDataManager.ClearBlockDataAll(true);
    }


    void Update()
    {
        if (false == Input.GetMouseButtonUp(0))
        {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit))
        {
            string[] spliteName = raycastHit.collider.transform.parent.name.Split('_');

            int row = BlockDataManager.s_boardRow;
            int column = BlockDataManager.s_boardColumn;
            int layer = BlockDataManager.s_boardLayer;

            try
            {
                row = int.Parse(spliteName[1]);
                column = int.Parse(spliteName[2]);
                layer = int.Parse(spliteName[3]);

                if (null != raycastHit.collider.gameObject.GetComponent<Animator>())
                {
                    byte bright = 1;

                    RgbBlockData led = BlockDataManager.shared[row, column, layer] as RgbBlockData;
                    if (null != led)
                    {
                        if (led.bright != 0)
                        {
                            bright = 0;
                        }

                        KJ.Message.Bluetooth.ReceiveDataRgbColor message = new KJ.Message.Bluetooth.ReceiveDataRgbColor(row, column, layer, led.id, bright, new Color32(255, 255, 255, 255));
                        KJ.Handler.KJMessageHandler.SendMessage(message);

                        KJPluginMethods.methods.bluetooth.setRgb((byte)row, (byte)column, (byte)layer, new Color32(255, 255, 255, 255), bright);
                    }
                    else
                    {
                        BlockCommonData block = BlockDataManager.shared[row, column, layer];
                        if (null != block)
                        {
                            if (block.category == KJBluetoothValues.BlockCategory.ThreeD)
                            {
                                ThreeDBlockData three = (ThreeDBlockData)block;

                                //three.ledState ++;

                                KJPluginMethods.methods.bluetooth.setThreed((byte)row, (byte)column, (byte)layer, three.type, three.ledState, three.cardID);
                            }
                            else if (block.category == KJBluetoothValues.BlockCategory.Art)
                            {
                                ArtBlockData art = (ArtBlockData)block;
                                KJPluginMethods.methods.bluetooth.setArt((byte)row, (byte)column, (byte)layer, art.type, art.ledState);
                            }
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, e.ToString());
            }
        }
    }

    void AddLog(string i_message, bool i_send)
    {
        if (Time.frameCount != s_timeFrame)
        {
            ++s_increamentFrame;
            s_timeFrame = Time.frameCount;
        }

        if (true == i_send)
        {
            uiTextListLog.Add(string.Format("[send - {0}]\n{1}", s_increamentFrame, i_message));
        }
        else
        {
            uiTextListLog.Add(string.Format("[recv - {0}]\n{1}", s_increamentFrame, i_message));
		}
    }


    void PlayLcdContent()
    {
        /*List<LcdBlockData> list = BlockDataManager.shared.GetBlockData<LcdBlockData>();
        if (0 == list.Count || true == string.IsNullOrEmpty(lcdContent))
        {
            return;
        }

        if ('0' <= lcdContent[lcdContentIndex] && '9' >= lcdContent[lcdContentIndex])
        {
            KJPluginMethods.methods.bluetooth.setLcd((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.LcdContentType.Number, lcdContent[lcdContentIndex]);
        }
        else if ('a' <= lcdContent[lcdContentIndex] && 'z' >= lcdContent[lcdContentIndex])
        {
            KJPluginMethods.methods.bluetooth.setLcd((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.LcdContentType.LowerCase, lcdContent[lcdContentIndex]);
        }
        else if ('A' <= lcdContent[lcdContentIndex] && 'Z' >= lcdContent[lcdContentIndex])
        {
            KJPluginMethods.methods.bluetooth.setLcd((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.LcdContentType.UppserCase, lcdContent[lcdContentIndex]);
        }

        ++lcdContentIndex;
        if (lcdContentIndex >= lcdContent.Length)
        {
            lcdContentIndex = 0;
        }*/
    }

    
	#region Block Data Receive
	public void AddCensorBlock(CensorBlockData i_blockData)
	{
	}
	public void RemoveCensorBlock(BlockCommonData i_blockData)
	{
	}
	public void ChangeCensorValue(CensorBlockData i_blockData)
	{
	}

    public void AddGameBlock(GameBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);
        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockGame);

		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);

        this.AddLog(string.Format("AddGameBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    /*
    public void AddDummyBlock(DummyBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        switch (i_blockData.type)
        {
            case KJBluetoothValues.BlockType.ThreeRed:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockRed);
                break;

            case KJBluetoothValues.BlockType.ThreeGreen:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockGreen);
                break;

            case KJBluetoothValues.BlockType.DummyBlue:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockBlue);
                break;

            case KJBluetoothValues.BlockType.DummyYellow:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockYellow);
                break;

            case KJBluetoothValues.BlockType.DummyViolet:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockViolet);
                break;


            case KJBluetoothValues.BlockType.DummyLongRed:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabLongBlockRed);
                break;

            case KJBluetoothValues.BlockType.DummyLongGreen:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabLongBlockGreen);
                break;

            case KJBluetoothValues.BlockType.DummyLongBlue:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabLongBlockBlue);
                break;

            case KJBluetoothValues.BlockType.DummyLongYellow:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabLongBlockYellow);
                break;

            case KJBluetoothValues.BlockType.DummyLongViolet:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabLongBlockViolet);
                break;
        }

		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);

        this.AddLog(string.Format("Add block: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);


        if (true == uiToggleChangeDummyBlockPopupAuto.value && 0 == i_blockData.x && 0 == i_blockData.y && 0 == i_blockData.z)
        {
            this.OnClickButtonChangeDummyBlockPopupOk();
        }
    }*/

    /*public void AddLedBlock(LedBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockLed);
		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);
    }*/
    public void AddTouchBlock(TouchBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockTouch);
		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);

        this.AddLog(string.Format("AddTouchBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    /*
    public void AddLcdBlock(LcdBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockLcd);
		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);
    }
     * */
    public void AddMotorBlock(MotorBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockMotor);
		KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
		//KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject[index], i_blockData.goBlock.transform);
        this.AddLog(string.Format("AddMotorBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    public void AddThreeDBlock(ThreeDBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        Debug.Log("AddThreeDBlock : " + i_blockData.ledState + "cardID:" + i_blockData.cardID);

        switch (i_blockData.type)
        {
            case KJBluetoothValues.BlockType.ThreeRed:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockRed);
                break;

            case KJBluetoothValues.BlockType.ThreeGreen:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockGreen);
                break;

            case KJBluetoothValues.BlockType.ThreeBlue:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockBlue);
                break;

            case KJBluetoothValues.BlockType.ThreeYellow:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockYellow);
                break;

            case KJBluetoothValues.BlockType.ThreeViolet:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockViolet);
                break;

            case KJBluetoothValues.BlockType.ThreeOrange:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockYellow);
                break;

            case KJBluetoothValues.BlockType.ThreeIndigo:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockViolet);
                break;
        }

        KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
        this.AddLog(string.Format("AddThreeDBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);


        /*if (true == uiToggleChangeDummyBlockPopupAuto.value && 0 == i_blockData.x && 0 == i_blockData.y && 0 == i_blockData.z)
        {
            this.OnClickButtonChangeDummyBlockPopupOk();
        }*/
    }
    public void AddArtBlock(ArtBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);
        Debug.Log("AddArtBlock : " + i_blockData.ledState);
        switch (i_blockData.type)
        {
            case KJBluetoothValues.BlockType.ArtRed:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockRed);
                break;

            case KJBluetoothValues.BlockType.ArtGreen:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockGreen);
                break;

            case KJBluetoothValues.BlockType.ArtBlue:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockBlue);
                break;

            case KJBluetoothValues.BlockType.ArtYellow:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockYellow);
                break;

            case KJBluetoothValues.BlockType.ArtViolet:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockViolet);
                break;

            case KJBluetoothValues.BlockType.ArtOrange:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockYellow);
                break;

            case KJBluetoothValues.BlockType.ArtIndigo:
                i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockViolet);
                break;
        }

        KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9
        this.AddLog(string.Format("AddArtBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);


        /*if (true == uiToggleChangeDummyBlockPopupAuto.value && 0 == i_blockData.x && 0 == i_blockData.y && 0 == i_blockData.z)
        {
            this.OnClickButtonChangeDummyBlockPopupOk();
        }*/
    }
    public void AddRgbBlock(RgbBlockData i_blockData)
    {
        int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(i_blockData.x, i_blockData.y, i_blockData.z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);

        i_blockData.goBlock = (GameObject)GameObject.Instantiate(prefabBlockLed);
        KJ.Utilities.GameObjectUtility.AttachChild(blockPositionObject2[index], i_blockData.goBlock.transform);//block9

        this.AddLog(string.Format("AddRgbBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }

    public void RemoveGameBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveGameBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    /*public void RemoveDummyBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
    }*/
    /*public void RemoveLedBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
    }*/
    public void RemoveTouchBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveGameBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    /*
    public void RemoveLcdBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
    }*/
    public void RemoveMotorBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveMotorBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    public void RemoveThreeDBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveThreeDBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    public void RemoveArtBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveArtBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    public void RemoveRgbBlock(BlockCommonData i_blockData)
    {
        GameObject.Destroy(i_blockData.goBlock);
        this.AddLog(string.Format("RemoveRgbBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }

    /*public void AddPlayerBlock(SubBlockCommonData i_subBlockData)
    {
        i_subBlockData.goBlock = (GameObject)GameObject.Instantiate(prefabCharacter);
        KJ.Utilities.GameObjectUtility.AttachChild(i_subBlockData.mainBlockData.goBlock, i_subBlockData.goBlock.transform);

        i_subBlockData.goBlock.transform.localPosition = prefabCharacter.transform.localPosition;
        i_subBlockData.goBlock.transform.localScale = prefabCharacter.transform.localScale;
    }
    public void AddItemBlock(SubBlockCommonData i_subBlockData)
    {
        i_subBlockData.goBlock = (GameObject)GameObject.Instantiate(prefabItems[i_subBlockData.index]);
        KJ.Utilities.GameObjectUtility.AttachChild(i_subBlockData.mainBlockData.goBlock, i_subBlockData.goBlock.transform);

        i_subBlockData.goBlock.transform.localPosition = prefabItems[i_subBlockData.index].transform.localPosition;
        i_subBlockData.goBlock.transform.localScale = prefabItems[i_subBlockData.index].transform.localScale;
    }*/
    public void AddSkinBlock(SubBlockCommonData i_subBlockData)
    {
        i_subBlockData.goBlock = (GameObject)GameObject.Instantiate(prefabSkin);
        KJ.Utilities.GameObjectUtility.AttachChild(i_subBlockData.mainBlockData.goBlock, i_subBlockData.goBlock.transform);

        i_subBlockData.goBlock.transform.localPosition = prefabSkin.transform.localPosition;
        i_subBlockData.goBlock.transform.localScale = prefabSkin.transform.localScale;

        this.AddLog(string.Format("AddSkinBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }

    /*public void RemovePlayerBlock(SubBlockCommonData i_subBlockData)
    {
        GameObject.Destroy(i_subBlockData.goBlock);
    }
    public void RemoveItemBlock(SubBlockCommonData i_subBlockData)
    {
        GameObject.Destroy(i_subBlockData.goBlock);
    }*/
    public void RemoveSkinBlock(SubBlockCommonData i_subBlockData)
    {
        GameObject.Destroy(i_subBlockData.goBlock);

        this.AddLog(string.Format("RemoveSkinBlock: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }

    /*public void ChangeLedColor(LedBlockData i_blockData)
    {
        i_blockData.goBlock.GetComponent<Animator>().SetBool("OnLed", i_blockData.isOn);
    }*/

    public void ChangeTouchState(TouchBlockData i_blockData)
    {
        Color32 color = new Color32(255, 255, 255, 255);

        RgbBlockData top = BlockDataManager.shared[3, 4, 0] as RgbBlockData;
        if (null != top && top.isOn != i_blockData[KJBluetoothValues.TouchDirection.Up])
        {
            KJPluginMethods.methods.bluetooth.setRgb(3, 4, 0, color, i_blockData[KJBluetoothValues.TouchDirection.Up] ? (byte)5 : (byte)0);
        }

        RgbBlockData right = BlockDataManager.shared[4, 3, 0] as RgbBlockData;
        if (null != right && right.isOn != i_blockData[KJBluetoothValues.TouchDirection.Right])
        {
            KJPluginMethods.methods.bluetooth.setRgb(4, 3, 0, color, i_blockData[KJBluetoothValues.TouchDirection.Right] ? (byte)5 : (byte)0);
        }

        RgbBlockData down = BlockDataManager.shared[3, 2, 0] as RgbBlockData;
        if (null != down && down.isOn != i_blockData[KJBluetoothValues.TouchDirection.Down])
        {
            KJPluginMethods.methods.bluetooth.setRgb(3, 2, 0, color, i_blockData[KJBluetoothValues.TouchDirection.Down] ? (byte)5 : (byte)0);
        }

        RgbBlockData left = BlockDataManager.shared[2, 3, 0] as RgbBlockData;
        if (null != left && left.isOn != i_blockData[KJBluetoothValues.TouchDirection.Left])
        {
            KJPluginMethods.methods.bluetooth.setRgb(2, 3, 0, color, i_blockData[KJBluetoothValues.TouchDirection.Left] ? (byte)5 : (byte)0);
        }

        RgbBlockData center = BlockDataManager.shared[3, 3, 0] as RgbBlockData;
        if (null != center && center.isOn != i_blockData[KJBluetoothValues.TouchDirection.Center])
        {
            KJPluginMethods.methods.bluetooth.setRgb(3, 3, 0, color, i_blockData[KJBluetoothValues.TouchDirection.Center] ? (byte)5 : (byte)0);
        }

        this.AddLog(string.Format("ChangeTouchState: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    /*public void ChangeLcdContent(LcdBlockData i_blockData)
    {
    }*/
    public void ChangeMotorState(MotorBlockData i_blockData)
    {
        this.AddLog(string.Format("ChangeMotorState: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    public void ChangeRgbColor(RgbBlockData i_blockData)
    {
        i_blockData.goBlock.GetComponent<Animator>().SetBool("OnLed", i_blockData.isOn);
        this.AddLog(string.Format("ChangeRgbColor: latency = {0:0.000} sec(s)", Time.realtimeSinceStartup - startBlockSeconds), false);
    }
    #endregion Block Data Receive


    #region On Message
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.LoadPrevScene());
    }

    void OnMessageReceiveRawData(KJ.Message.Bluetooth.ReceiveRawData i_message)
    {
        string message = System.Text.Encoding.ASCII.GetString(i_message.raw).Replace('\0', '0');

        this.AddLog(message, false);

        ++memoryReadCount;

        startBlockSeconds = Time.realtimeSinceStartup;


        //KJPluginMethods.methods.bluetooth.testSetAsciiCommand(new byte[] { 79, 75, 50 });
    }
    void OnMessageSendRawData(KJ.Message.Bluetooth.SendRawData i_message)
    {
        string message = System.Text.Encoding.ASCII.GetString(i_message.raw).Replace('\0', '0');

        this.AddLog(message, true);
    }
    void OnMessageAckOk(KJ.Message.Bluetooth.AckOk i_message)
    {
        this.AddLog(string.Format("Memory read: block count = {0}, latency = {1:0.000} sec(s)", memoryReadCount, Time.realtimeSinceStartup - startReadAckSeconds), false);
    }


    void _TestRegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.AckOk>(this.OnMessageAckOk);
    }
    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);

        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveRawData>(this.OnMessageReceiveRawData);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.SendRawData>(this.OnMessageSendRawData);

        this._TestRegistMessage();
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message


    #region On UI
    public void OnClickButtonQuitPopupOK()
    {
        Application.Quit();
    }


    public void OnClickButtonLoop()
    {
        this.AddLog("Loop", true);

        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.Loop);
    }
    public void OnClickButtonMemoryRead()
    {
        this.AddLog("Memory read", true);

        startReadAckSeconds = Time.realtimeSinceStartup;
        memoryReadCount = 0;

        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.MemoryRead);
    }
    public void OnClickButtonMemoryReadAndLoop()
    {
        this.AddLog("Memory read and loop", true);

        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.MemoryReadAndLoop);
    }
    public void OnClickButtonWait()
    {
        this.AddLog("Wait", true);

        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.Wait);
    }
    public void OnClickButtonBlinkAll()
    {
        enableBlink = !enableBlink;
        this.AddLog("Blink all: " + enableBlink, true);

        if (true == enableBlink)
        {
            KJPluginMethods.methods.bluetooth.setBlink(0, 0, 0, KJBluetoothValues.BlinkState.Default, new Color32(255, 255, 255, 255), 5, KJBluetoothValues.BlinkTime.Seconds_Unlimited, KJBluetoothValues.BlinkInterval.MilliSeconds_500, KJBluetoothValues.BlinkTarget.All);
        }
        else
        {
            KJPluginMethods.methods.bluetooth.setBlink(0, 0, 0, KJBluetoothValues.BlinkState.Stop, new Color32(255, 255, 255, 255), 5, KJBluetoothValues.BlinkTime.Seconds_Unlimited, KJBluetoothValues.BlinkInterval.MilliSeconds_500, KJBluetoothValues.BlinkTarget.All);
        }
    }
    public void OnClickButtonLedAll()
    {
        setLedLightAllOn = !setLedLightAllOn;
        this.AddLog("Blink all: " + setLedLightAllOn, true);

        //BlockDataManager.shared.SetLedLightAll(setLedLightAllOn);
    }

    public void OnSubmitAsciiCommand()
    {
        string text = UIInput.current.value;

        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(text);

        //List<byte> listBytes = new List<byte>();
        
        //foreach (char c in text)
        //{
        //    if (false == System.Char.IsDigit(c))
        //    {
        //        return;
        //    }

        //    byte ascii = 0;
        //    if (false == byte.TryParse(c.ToString(), out ascii))
        //    {
        //        return;
        //    }

        //    listBytes.Add(ascii);
        //}

        KJPluginMethods.methods.bluetooth.testSetAsciiCommand(bytes);
    }
    //chi.
    public void OnClickButtonChangeDummyBlockPopupOk()
    {
        if (null == BlockDataManager.shared[0])
        {
            uiLabelChangeDummyBlockPopupMessage.text = "can you settings only [1, 1, 1] position";
            return;
        }
        if (KJBluetoothValues.BlockCategory.ThreeD != BlockDataManager.shared[0].category)
        {
            uiLabelChangeDummyBlockPopupMessage.text = "can you settings only dummy block";
            return;
        }

 //       uiLabelChangeDummyBlockPopupMessage.text = string.Format("{0} -> {1}", System.Convert.ToChar(BlockDataManager.shared[0].type), UIToggle.GetActiveToggle(uiToggleChangeDummyBlockPopupCommandRed.group).name);

//		Debug.Log (string.Format("{0} -> {1}", System.Convert.ToChar(BlockDataManager.shared[0].type), UIToggle.GetActiveToggle(uiToggleChangeDummyBlockPopupCommandRed.group).name));
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(string.Format("111{0}C{1}", System.Convert.ToChar(BlockDataManager.shared[0].type), UIToggle.GetActiveToggle(uiToggleChangeDummyBlockPopupCommandRed.group).name));
        KJPluginMethods.methods.bluetooth.testSetAsciiCommand(bytes);
    }

    public void OnClickButtonMotor()
    {
        List<MotorBlockData> list = BlockDataManager.shared.GetBlockData<MotorBlockData>();
        if (0 == list.Count)
        {
            return;
        }

        switch (list[0].direction)
        {
            case KJBluetoothValues.MotorDirection.Stop:
                //list[0].goBlock.animation.Play("animationMotorRight");

                KJPluginMethods.methods.bluetooth.setMotor((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.MotorDirection.CW, 9);
                list[0].direction = KJBluetoothValues.MotorDirection.CW;
                break;

            case KJBluetoothValues.MotorDirection.CW:
                //list[0].goBlock.animation.Play("animationMotorLeft");

                KJPluginMethods.methods.bluetooth.setMotor((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.MotorDirection.CCW, 9);
                list[0].direction = KJBluetoothValues.MotorDirection.CCW;
                break;

            case KJBluetoothValues.MotorDirection.CCW:
                //list[0].goBlock.animation.Stop();

                KJPluginMethods.methods.bluetooth.setMotor((byte)list[0].x, (byte)list[0].y, (byte)list[0].z, KJBluetoothValues.MotorDirection.Stop, 9);
                list[0].direction = KJBluetoothValues.MotorDirection.Stop;
                break;
        }
    }
    public void OnClickButtonLcdContent()
    {
        /*List<LcdBlockData> list = BlockDataManager.shared.GetBlockData<LcdBlockData>();
        if (0 == list.Count)
        {
            return;
        }

        if (true == this.IsInvoking("PlayLcdContent"))
        {
            this.CancelInvoke("PlayLcdContent");
        }

        lcdContent = UIButton.current.GetComponentInChildren<UILabel>().text;
        lcdContentIndex = 0;

        this.InvokeRepeating("PlayLcdContent", 0.5f, 0.5f);*/
    }
    #endregion On UI


	//block9
//    [System.Diagnostics.Conditional("UNITY_EDITOR")]
 //   [ContextMenu("Proceed positions")]
    private void InitBlockPosition()
    {
        blockPositionObject2 = new GameObject[BlockDataManager.s_boardRow * BlockDataManager.s_boardColumn * BlockDataManager.s_boardLayer];
        for (int z = 0; z < BlockDataManager.s_boardLayer; ++z)
        {
            for (int x = 0; x < BlockDataManager.s_boardRow; ++x)
            {
                for (int y = 0; y < BlockDataManager.s_boardColumn; ++y)
                {
                    int index = KJ.Utilities.Common.ThreeDimesionIndexToOneDimesionIndex(x, y, z, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn);
                    blockPositionObject2[index] = goDummyRoot.transform.Find(string.Format("layer_{2:00}/pos_{0}_{1}_{2}", x, y, z)).gameObject;

                }
            }
        }
    }
}
