﻿using UnityEngine;
using System.Collections;

public class TestLedView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    int x = 0, y = 0, z = 0;
    int bright = 4;
    Color32 color = new Color32(0, 255, 0, 255);

    string[] arr = new string[] { "1", "2", "3", "4", "5", "6", "7" };
    string[] arr2 = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    string[] arr3 = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

    [SerializeField]
    GUISkin skin;

    protected override void Start()
    {
        base.Start();
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    protected override void DoLoadData()
    {
        this._RegistMessage();
    }
    protected override void DoSaveData()
    {
        this._UnregistMessage();
    }

    protected override void DoOpen()
    {
        goRootView.SetActive(true);
    }
    protected override void DoClose()
    {
        goRootView.SetActive(false);
    }


    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.TestLog.Log(string.Format("connect lost")));

       // KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopView());
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }


    void OnGUI()
    {
        if (false == isOpened || true == TestLogView.fullScreen)
        {
            return;
        }

        float height = Screen.height / 20;

        Rect area = new Rect(0, Screen.height / 3, Screen.width, Screen.height - Screen.height / 3);

        GUILayout.BeginArea(area);
        {
            GUILayout.BeginVertical();
            {
                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("X : ", GUILayout.MaxWidth(50));
                    x = GUILayout.SelectionGrid(x, arr, 10, GUILayout.Height(height));
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Y : ", GUILayout.MaxWidth(50));
                    y = GUILayout.SelectionGrid(y, arr, 10, GUILayout.Height(height));
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Z : ", GUILayout.MaxWidth(50));
                    z = GUILayout.SelectionGrid(z, arr2, 10, GUILayout.Height(height));
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Bright : ", GUILayout.MaxWidth(50));
                    bright = GUILayout.SelectionGrid(bright, arr3, 10, GUILayout.Height(height));
                }
                GUILayout.EndHorizontal();


                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Space(Screen.width / 6);

                    GUILayout.BeginVertical();
                    {
                        GUI.skin = skin;

                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label(string.Format("R ({0}): ", color.r), GUILayout.MaxWidth(50));
                            color.r = (byte)GUILayout.HorizontalSlider(color.r, 0, 255, GUILayout.Width(Screen.width / 2));
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.Space(10);
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label(string.Format("G ({0}): ", color.g), GUILayout.MaxWidth(50));
                            color.g = (byte)GUILayout.HorizontalSlider(color.g, 0, 255, GUILayout.Width(Screen.width / 2));
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.Space(10);
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label(string.Format("B ({0}): ", color.b), GUILayout.MaxWidth(50));
                            color.b = (byte)GUILayout.HorizontalSlider(color.b, 0, 255, GUILayout.Width(Screen.width / 2));
                        }
                        GUILayout.EndHorizontal();

                        GUI.skin = null;
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();


                GUILayout.Space(10);
                if (true == GUILayout.Button("Send", GUILayout.Height(height)))
                {
                    KJPluginMethods.methods.bluetooth.setRgb((byte)x, (byte)y, (byte)z, color, (byte)bright);
                }
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }
}
