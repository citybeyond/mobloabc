﻿using UnityEngine;
using System.Collections.Generic;


namespace KJ.Message.TestLog
{
    public sealed class Log
    {
        public string message
        {
            get;
            private set;
        }

        public Log(string i_message)
        {
            message = i_message;
        }
    }
}

public class TestLogView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    public static bool fullScreen = false;

    Vector2 scroll = Vector2.zero;

    List<string> listRaw = new List<string>();

    Vector2 oldPosition = Vector3.zero;
    bool onDrag = false;


    protected override void Start()
    {
        base.Start();

        this._RegistMessage();
    }
    protected override void OnDestroy()
    {
        this._UnregistMessage();

        base.OnDestroy();
    }

    protected override void DoLoadData()
    {
    }
    protected override void DoSaveData()
    {
        
    }

    protected override void DoOpen()
    {
        
    }
    protected override void DoClose()
    {
        
    }


    void OnMessageLog(KJ.Message.TestLog.Log i_message)
    {
        listRaw.Add(i_message.message);

        scroll.y = float.MaxValue;
    }

    void OnMessageReceiveRawData(KJ.Message.Bluetooth.ReceiveRawData i_message)
    {
        string temp = System.Text.Encoding.ASCII.GetString(i_message.raw);

        this.OnMessageLog(new KJ.Message.TestLog.Log(temp));
    }

    void OnMessageReceiveDataGameBlock(KJ.Message.Bluetooth.ReceiveDataGameBlock i_message)
    {
        //BluetoothSerialize.GameBlockData data = LitJson.JsonMapper.ToObject<BluetoothSerialize.GameBlockData>(i_message.json);

        this.OnMessageLog(new KJ.Message.TestLog.Log(i_message.json));
    }
    void ReceiveDataThreeDBlock(KJ.Message.Bluetooth.ReceiveDataThreeDBlock i_message)
    {
        this.OnMessageLog(new KJ.Message.TestLog.Log(i_message.json));
    }
    void ReceiveDataArtBlock(KJ.Message.Bluetooth.ReceiveDataArtBlock i_message)
    {
        this.OnMessageLog(new KJ.Message.TestLog.Log(i_message.json));
    }
    void ReceiveDataRgbBlock(KJ.Message.Bluetooth.ReceiveDataRgbBlock i_message)
    {
        this.OnMessageLog(new KJ.Message.TestLog.Log(i_message.json));
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.TestLog.Log>(this.OnMessageLog);

        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveRawData>(this.OnMessageReceiveRawData);

        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataGameBlock>(this.OnMessageReceiveDataGameBlock);
        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataDummyBlock>(this.ReceiveDataDummyBlock);
        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataLedBlock>(this.ReceiveDataLedBlock);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }


    void Update()
    {
        Rect area = true == fullScreen ? new Rect(0, 0, Screen.width, Screen.height) : new Rect(0, 0, Screen.width, Screen.height / 3);

        if (true == Input.GetMouseButtonDown(0))
        {
            Vector2 pos = Input.mousePosition;
            pos.y = Screen.height - pos.y;

            if (true == area.Contains(pos))
            {
                oldPosition = pos;
                onDrag = true;
            }
        }
        else if (true == Input.GetMouseButtonUp(0))
        {
            onDrag = false;
        }

        if (true == onDrag)
        {
            Vector2 pos = Input.mousePosition;
            pos.y = Screen.height - pos.y;

            Vector2 delta = oldPosition - pos;
            scroll += delta;

            oldPosition = pos;
        }
    }

    void OnGUI()
    {
        if (false == isOpened)
        {
            return;
        }

        float height = Screen.height / 20;

        Rect area = new Rect(0, 0, Screen.width, Screen.height / 3);
        Rect fullArea = new Rect(0, 0, Screen.width, Screen.height);

        GUILayout.BeginArea((true == fullScreen) ? fullArea : area);
        {
            GUILayout.BeginVertical();
            {
                scroll = GUILayout.BeginScrollView(scroll);
                {
                    GUIStyle style = new GUIStyle();
                    style.fontSize = (int)height / 3;
                    style.normal.textColor = Color.white;

                    foreach (string str in listRaw)
                    {
                        GUILayout.Label(str, style);
                    }
                }
                GUILayout.EndScrollView();

                GUILayout.BeginHorizontal();
                {
                    if (true == GUILayout.Button("Clear", GUILayout.Height(height)))
                    {
                        listRaw.Clear();
                    }

                    if (true == GUILayout.Button(true == fullScreen ? "Window Screen" : "Full Screen", GUILayout.Height(height)))
                    {
                        fullScreen = !fullScreen;

                        scroll.y = float.MaxValue;
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }
}
