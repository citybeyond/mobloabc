﻿using UnityEngine;
using System.Collections.Generic;

public abstract class KJAPluginMethods
{
    public abstract class ABluetooth
    {
        public abstract void testSetAsciiCommand(byte[] i_arrBytes);

        public abstract void requestCommand(KJBluetoothValues.BluetoothCommands i_commandIndex);

        public abstract void ensureDiscoverable(int i_duration);

        public abstract void doDiscovery();

        public abstract void cancelDiscovery();

        public abstract void openDefaultBluetoothActivity();

        public abstract BluetoothSerialize.FoundDevice[] getBondedDevices();

        public abstract string connectDeviceByAddress(string i_address);

        public abstract bool isConnected();

        public abstract void disconnect();

		public abstract void requestBoardType();

        [System.Obsolete("Not used anymore")]
        public abstract bool hasBlock();

        public abstract void setBlink(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlinkState i_state, Color32 i_color, byte i_bright, KJBluetoothValues.BlinkTime i_blinkTime, KJBluetoothValues.BlinkInterval i_blinkInterval, KJBluetoothValues.BlinkTarget i_blinkTarget);
        public abstract void setMotor(byte i_x, byte i_y, byte i_z, KJBluetoothValues.MotorDirection i_direction, byte i_speed);

        //add
        public abstract void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led, int cardID);
        public abstract void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte BlockState, byte i_data0, int cardID);
        public abstract void setArt(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led);
        public abstract void setRgb(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright);

        public abstract void WriteDeviceName(string deviceName);
    }
    public abstract class ANfc
    {
        public abstract void writeText(string i_text);
    }

    private ABluetooth _bluetooth = null;
    public ABluetooth bluetooth
    {
        get
        {
            if (null == _bluetooth)
            {
                _bluetooth = this.CreateBluetooth();
            }

            return _bluetooth;
        }
    }

    private ANfc _nfc = null;
    public ANfc nfc
    {
        get
        {
            if (null == _nfc)
            {
                _nfc = this.CreateNfc();
            }

            return _nfc;
        }
    }

    protected abstract ABluetooth CreateBluetooth();
    protected abstract ANfc CreateNfc();

    public abstract void Initialize();
}
