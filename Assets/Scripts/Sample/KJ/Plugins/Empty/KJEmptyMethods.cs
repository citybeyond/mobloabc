﻿using UnityEngine;
using System.Collections;

public class KJEmptyMethods : KJAPluginMethods
{
    public class Bluetooth : ABluetooth
    {
        public override void testSetAsciiCommand(byte[] i_arrBytes)
        {
        }

        public override void requestCommand(KJBluetoothValues.BluetoothCommands i_commandIndex)
        {
        }

        public override void ensureDiscoverable(int i_duration)
        {
        }

        public override void doDiscovery()
        {
        }

        public override void cancelDiscovery()
        {
        }

        public override void openDefaultBluetoothActivity()
        {
        }

        public override BluetoothSerialize.FoundDevice[] getBondedDevices()
        {
            return new BluetoothSerialize.FoundDevice[0];
        }

        public override string connectDeviceByAddress(string i_address)
        {
            return string.Empty;
        }

        public override bool isConnected()
        {
            return false;
        }

        public override void disconnect()
        {
        }

		public override void requestBoardType()
		{

		}

        [System.Obsolete("Not used anymore")]
        public override bool hasBlock()
        {
            return false;
        }

        /*public override void setLed(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {
        }*/

        /*[System.Obsolete("This is no longer in use. Using setLed(...)")]
        public override void setLED(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {
        }*/

        public override void setBlink(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlinkState i_state, Color32 i_color, byte i_bright, KJBluetoothValues.BlinkTime i_blinkTime, KJBluetoothValues.BlinkInterval i_blinkInterval, KJBluetoothValues.BlinkTarget i_blinkTarget)
        {
        }

        /*[System.Obsolete("setBlink is obsolete. Use another setBlink")]
        public override void setBlink(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, KJBluetoothValues.BlinkState i_state, Color32 i_color, byte i_bright, KJBluetoothValues.BlinkTime i_blinkTime, KJBluetoothValues.BlinkInterval i_blinkInterval, KJBluetoothValues.BlinkTarget i_blinkTarget)
        {
        }*/

        /*public override void setLcd(byte i_x, byte i_y, byte i_z, KJBluetoothValues.LcdContentType i_contentType, char i_content)
        {
        }*/

        public override void setMotor(byte i_x, byte i_y, byte i_z, KJBluetoothValues.MotorDirection i_direction, byte i_speed)
        {
        }

        public override void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led, int cardID)
        {
        }
        public override void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte BlockState, byte i_data0, int cardID)
        {
        }
        public override void setArt(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led)
        {

        }
        public override void setRgb(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {

        }
        public override void WriteDeviceName(string deviceName)
        {

        }
    }
    public class Nfc : ANfc
    {
        public override void writeText(string i_text)
        {
        }
    }

    protected override ABluetooth CreateBluetooth()
    {
        return new Bluetooth();
    }
    protected override ANfc CreateNfc()
    {
        return new Nfc();
    }

    public override void Initialize()
    {
        
    }
}
