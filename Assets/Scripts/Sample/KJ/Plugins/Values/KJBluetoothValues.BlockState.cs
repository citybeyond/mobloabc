﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlockState : int
    {
        On = 1,
        Off,
        Hold,
    }
}
