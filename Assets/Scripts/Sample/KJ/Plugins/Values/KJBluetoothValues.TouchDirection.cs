﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum TouchDirection
    {
        None = 0,
        Up = 1,
        Right,
        Down,
        Left,
        Center
    }
}
