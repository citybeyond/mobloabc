﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlinkTarget : byte
    {
        Board = 0,
        /// <summary>
        /// Act on Block Type
        /// </summary>
        TargetBlock,
        /*NfcBlocks,
        DummyBlocks,
        LedBlocks,
        NfcAndDummyBlocks,
        NfcAndLedBlocks,
        DummyAndLedBlocks,
        NfcDummyAndLedBlocks,*/
        All,
    }
}
