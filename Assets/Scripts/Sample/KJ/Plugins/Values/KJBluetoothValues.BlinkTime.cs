﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlinkTime : byte
    {
        Seconds_1 = 1,
        Seconds_2,
        Seconds_3,
        Seconds_4,
        Seconds_5,
        Seconds_6,
        Seconds_7,
        Seconds_8,
        Seconds_Unlimited,
    }
}
