﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum MotorDirection
    {
        Stop = 1,

        // hack: caof: 2015-03-02: 하드웨어에서 반환값으로 잘못된 값이 옴(2 -> 5, 3 ->6 으로 변환)
        /// <summary>
        /// hard-codingn clock-wise
        /// </summary>
        CW2 = 2,
        /// <summary>
        /// hard-codingn clock-wise
        /// </summary>
        CCW2 = 3,
        //->

        /// <summary>
        /// clock-wise
        /// </summary>
        CW = 5,
        /// <summary>
        /// count-clock-wise
        /// </summary>
        CCW = 6,
    }
}
