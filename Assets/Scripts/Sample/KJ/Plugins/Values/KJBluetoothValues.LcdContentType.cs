﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum LcdContentType : byte
    {
        None = 1,
        UppserCase,
        LowerCase,
        Number,
    }
}
