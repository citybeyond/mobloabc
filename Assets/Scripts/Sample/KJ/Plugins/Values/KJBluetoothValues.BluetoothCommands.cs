﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BluetoothCommands : int
    {
        Loop = 1,
        MemoryRead,
        MemoryReadAndLoop,
        Wait,
    }
}
