﻿using UnityEngine;
using System.Collections;

public static class BluetoothSerialize
{
    public class FoundDevice
    {
        public string name = string.Empty;
        public string address = string.Empty;
    }

    public class ChangeState
    {
        public KJBluetoothReceiver.State state = KJBluetoothReceiver.State.NONE;
        public string deviceName = string.Empty;
    }

    public abstract class ABlockData
    {
        [LitJson.JsonMapper.Ignore]
        private int _x = 0;
        [LitJson.JsonMapper.Include]
        [LitJson.JsonMapper.Name("x")]
        public int x
        {
            get
            {
                return _x;
            }
            private set
            {
                _x = value - 1;
            }
        }

        [LitJson.JsonMapper.Ignore]
        private int _y = 0;
        [LitJson.JsonMapper.Include]
        [LitJson.JsonMapper.Name("y")]
        public int y
        {
            get
            {
                return _y;
            }
            private set
            {
                _y = value - 1;
            }
        }

        [LitJson.JsonMapper.Ignore]
        private int _z = 0;
        [LitJson.JsonMapper.Include]
        [LitJson.JsonMapper.Name("z")]
        public int z
        {
            get
            {
                return _z;
            }
            private set
            {
                _z = value - 1;
            }
        }

        [LitJson.JsonMapper.Ignore]
        private KJBluetoothValues.BlockCategory _category = KJBluetoothValues.BlockCategory.None;
        [LitJson.JsonMapper.Ignore]
        public KJBluetoothValues.BlockCategory category
        {
            get
            {
                return _category;
            }
        }

        [LitJson.JsonMapper.Ignore]
        private KJBluetoothValues.BlockType _type = KJBluetoothValues.BlockType.None;
        [LitJson.JsonMapper.Include]
        [LitJson.JsonMapper.Name("type")]
        public KJBluetoothValues.BlockType type
        {
            get
            {
                return _type;
            }
            set
            {
                if (true == System.Enum.IsDefined(typeof(KJBluetoothValues.BlockType), value))
                {
                    _type = value;
                }
                else
                {
                    _type = KJBluetoothValues.BlockType.None;
                }

                switch (_type)
                {
                    case KJBluetoothValues.BlockType.GameNfc:
                        _category = KJBluetoothValues.BlockCategory.GameNfc;
                        break;

                    /*case KJBluetoothValues.BlockType.DummyRed:
                    case KJBluetoothValues.BlockType.DummyGreen:
                    case KJBluetoothValues.BlockType.DummyBlue:
                    case KJBluetoothValues.BlockType.DummyYellow:
                    case KJBluetoothValues.BlockType.DummyViolet:
                    case KJBluetoothValues.BlockType.DummyLongRed:
                    case KJBluetoothValues.BlockType.DummyLongGreen:
                    case KJBluetoothValues.BlockType.DummyLongBlue:
                    case KJBluetoothValues.BlockType.DummyLongYellow:
                    case KJBluetoothValues.BlockType.DummyLongViolet:
                        _category = KJBluetoothValues.BlockCategory.Dummy;
                        break;*/
                    case KJBluetoothValues.BlockType.ThreeRed:
                    case KJBluetoothValues.BlockType.ThreeGreen:
                    case KJBluetoothValues.BlockType.ThreeBlue:
                    case KJBluetoothValues.BlockType.ThreeYellow:
                    case KJBluetoothValues.BlockType.ThreeViolet:
                    case KJBluetoothValues.BlockType.ThreeOrange:
                    case KJBluetoothValues.BlockType.ThreeIndigo:
                    case KJBluetoothValues.BlockType.TowerRed:
                    case KJBluetoothValues.BlockType.TowerGreen:
                    case KJBluetoothValues.BlockType.TowerBlue:
                    case KJBluetoothValues.BlockType.TowerYellow:
                    case KJBluetoothValues.BlockType.TowerViolet:
                    case KJBluetoothValues.BlockType.TowerOrange:
                    case KJBluetoothValues.BlockType.TowerIndigo:
                        _category = KJBluetoothValues.BlockCategory.ThreeD;
                        break;
                    case KJBluetoothValues.BlockType.ArtRed:
                    case KJBluetoothValues.BlockType.ArtGreen:
                    case KJBluetoothValues.BlockType.ArtBlue:
                    case KJBluetoothValues.BlockType.ArtYellow:
                    case KJBluetoothValues.BlockType.ArtViolet:
                    case KJBluetoothValues.BlockType.ArtOrange:
                    case KJBluetoothValues.BlockType.ArtIndigo:
                        _category = KJBluetoothValues.BlockCategory.Art;
                        break;

                    case KJBluetoothValues.BlockType.Rgb:
                        _category = KJBluetoothValues.BlockCategory.Rgb;
                        break;

                    case KJBluetoothValues.BlockType.Censor:
                    case KJBluetoothValues.BlockType.SpecialMotor:
                    case KJBluetoothValues.BlockType.SpecialTouch:
                        _category = KJBluetoothValues.BlockCategory.Special;
                        break;

                    default:
                        _category = KJBluetoothValues.BlockCategory.None;
                        break;
                }
            }
        }

        public KJBluetoothValues.BlockState blockState = KJBluetoothValues.BlockState.Off;

        [LitJson.JsonMapper.Ignore]
        public bool isAttached
        {
            get
            {
                if (KJBluetoothValues.BlockState.On == blockState)
                {
                    return true;
                }

                return false;
            }
        }

        [LitJson.JsonMapper.Ignore]
        public int blockID
        {
            get
            {
                return (int)type;
            }
            set
            {
                if (false == System.Enum.IsDefined(typeof(KJBluetoothValues.BlockType), value))
                {
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "Does not defined: {0}", value);
                    return;
                }

                type = (KJBluetoothValues.BlockType)value;
            }
        }

        public void SetPosition(int i_x, int i_y, int i_z)
        {
            _x = i_x;
            _y = i_y;
            _z = i_z;
        }
    }

    public sealed class GameBlockData : ABlockData
    {

        public int skinID=0;
        public KJBluetoothValues.BlockState skinState = KJBluetoothValues.BlockState.Off;
    }

    /*public sealed class DummyBlockData : ABlockData
    {
        [LitJson.JsonMapper.Ignore]
        private int[] _itemID = new int[0];
        [LitJson.JsonMapper.Include]
        [LitJson.JsonMapper.Name("itemID")]
        public int[] itemID
        {
            get
            {
                return _itemID;
            }
            private set
            {
                _itemID = new int[value.Length];
                for (int i = 0; i < value.Length; ++i)
                {
                    _itemID[i] = value[i] - 1;
                }
            }
        }
        public KJBluetoothValues.BlockState[] itemState = new KJBluetoothValues.BlockState[0];
    }*/


    public sealed class ThreeDBlockData : ABlockData
    {
        public byte ledState = 2;
        public int cardID;
    }
    public sealed class ArtBlockData : ABlockData
    {
        public byte ledState = 2;
    }
    public sealed class CensorBlockData : ABlockData
    {
        public byte centimeter = 0;
    }

    public sealed class RgbBlockData : ABlockData
    {
        /// <summary>
        /// index order RGB, 0~255
        /// </summary>
        public byte[] colors = new byte[0];
        /// <summary>
        /// 0~9
        /// </summary>
        public int bright = 0;

        [LitJson.JsonMapper.Ignore]
        public Color32 color
        {
            get
            {
                return new Color32(colors[0], colors[1], colors[2], 255);
            }
        }
    }

    public sealed class RemoveBlockData : ABlockData
    {
    }

    public sealed class TouchBlockData : ABlockData
    {
        /// <summary>
        /// 5 direction touch state
        /// </summary>
        public byte[] directionStates = new byte[0];
    }

    /*public sealed class LcdBlockData : ABlockData
    {
        public byte contentType = 0;
        /// <summary>
        /// range in 0 ~ 9
        /// </summary>
        public byte number = 0;
        /// <summary>
        /// range in A ~ Z
        /// </summary>
        public byte upperCase = 0;
        /// <summary>
        /// range in a ~ z
        /// </summary>
        public byte lowerCase = 0;
    }*/

    public sealed class MotorBlockData : ABlockData
    {
        public byte direction = 0;
        /// <summary>
        /// range in 0 ~ 9
        /// </summary>
        public byte speed = 0;
    }
}
