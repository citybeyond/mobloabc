﻿using UnityEngine;
using System.Collections;

namespace KJ.Message.Nfc
{
    public sealed class ResponseTagId
    {
        public string tagId
        {
            get;
            private set;
        }

        public ResponseTagId(string i_tagId)
        {
            tagId = i_tagId;
        }
    }
    public sealed class ResponseTagData
    {
        public string recordData
        {
            get;
            private set;
        }

        public ResponseTagData(string i_recordData)
        {
            recordData = i_recordData;
        }
    }

    public sealed class WriteTextResult
    {
        public bool result
        {
            get;
            private set;
        }
        public string text
        {
            get;
            private set;
        }

        public WriteTextResult(bool i_result, string i_text)
        {
            result = i_result;
            text = i_text;
        }
    }
} // namespace KJ.Message.Nfc
