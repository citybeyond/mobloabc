﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Tween the object's slider value
/// </summary>

[AddComponentMenu("KJ/Custom-NGUI/Tween/Tween Counter")]
public class KJCustomTweenCount : UITweener
{
    public UILabel uiText;

    public float from = 0.0f;
    public float to = 0.0f;

    public bool ignoreDigit = false;
    public bool isCurrency = false;

    public string Postfix = string.Empty;

    private float _value = 0.0f;
    public float value
    {
        set
        {
            _value = value;

            if (uiText != null)
            {
                uiText.text = (true == isCurrency) ? ((int)value).ToString("n0") : ((int)value).ToString();
                uiText.text += Postfix;
            }
        }
        get
        {
            //if (uiText != null)
            //{
            //    return (true == isCurrency) ? int.Parse(uiText.text, System.Globalization.NumberStyles.Currency) : int.Parse(uiText.text);
            //}

            return _value;
        }
    }

    /// <summary>
    /// 외부에서 보여질 점수
    /// </summary>
    public int score
    {
        get
        {
            return (int)to;
        }
    }


    /// <summary>
    /// 점수를 담아둔다.
    /// </summary>
    private Queue<int> scoreQueue = new Queue<int>();

    private bool isProcess = false;

    bool initialize = false;

    /// <summary>
    /// count 올라가는 연출이 끝났을 때 호출
    /// </summary>
    public delegate void OnCompleteHandler();
    public event OnCompleteHandler OnCompleteEvent;

    protected override void Start()
    {
        if (false == initialize)
        {
            this.Initialize();
        }

        base.Start();
    }

    void Initialize()
    {
        from = 0.0f;
        to = 0.0f;
        value = 0.0f;
        isProcess = false;

        scoreQueue.Clear();

        initialize = true;
    }

    void Reset()
    {
        from = value;

        if (scoreQueue.Count > 0)
        {
            to = scoreQueue.Dequeue();
            this.ResetToBeginning();
            enabled = true;
            isProcess = true;
        }
        else
        {
            to = value;
            isProcess = false;
        }
    }

    public void AddCount(int count)
    {
        if (false == initialize)
        {
            this.Initialize();
        }

        scoreQueue.Enqueue(count);
        if (false == isProcess)
        {
            this.Reset();
        }
    }

    protected override void OnUpdate(float factor, bool isFinished)
    {
        if (true == ignoreDigit)
        {
            if (true == isFinished)
            {
                value = to;
            }
            else
            {
                float min = Mathf.Pow(10, KJ.Utilities.KJMath.GetDigitCount((int)to) - 1);
                float max = min * 10;

                value = Mathf.Max(0, (int)Mathf.Abs(to - Random.Range(min, max)));
            }
        }
        else
        {
            value = from * (1f - factor) + to * factor;
        }

        if (isFinished)
        {
            this.Reset();

            OnCompleteHandler handler = OnCompleteEvent;
            if (handler != null)
            {
                handler();
            }
        }
    }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>
    static public KJCustomTweenCount Begin(GameObject go, float duration)
    {
        KJCustomTweenCount comp = UITweener.Begin<KJCustomTweenCount>(go, duration);

        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }
}
