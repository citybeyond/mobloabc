﻿#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY)
#define MOBILE
#endif

using UnityEngine;
using System.Collections;

[AddComponentMenu("KJ/Custom-NGUI/UI/Input Korean")]
public class KJCustomInputKorean : UIInput
{
    protected override void OnSelect(bool isSelected)
    {
#if !MOBILE
        if (false == isSelected)
        {
            value = string.Format("{0}{1}", value, mLastIME);
        }
#endif
        base.OnSelect(isSelected);
    }
}
