﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("KJ/Custom-NGUI/Interaction/Play Sound")]
public class KJCustomPlaySound : UIPlaySound
{
    void OnHover(bool isOver)
    {
        if (enabled && ((isOver && trigger == Trigger.OnMouseOver) || (!isOver && trigger == Trigger.OnMouseOut)))
        {
            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Audio.PlaySound(audioClip, volume, pitch));
        }
    }

    void OnPress(bool isPressed)
    {
        if (enabled && ((isPressed && trigger == Trigger.OnPress) || (!isPressed && trigger == Trigger.OnRelease)))
        {
            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Audio.PlaySound(audioClip, volume, pitch));
        }
    }

    void OnClick()
    {
        if (enabled && trigger == Trigger.OnClick)
        {
            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Audio.PlaySound(audioClip, volume, pitch));
        }
    }
}
