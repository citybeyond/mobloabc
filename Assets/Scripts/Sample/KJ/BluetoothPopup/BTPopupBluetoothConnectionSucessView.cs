﻿using UnityEngine;
using System.Collections;




public class BTPopupBluetoothConnectionSucessView : BTPopup, KJ.Handler.KJMessageHandler.IMessageReceiver
{

    public GameObject m_panelChangeDeviceName;
    public UIInput m_input;
    public UILabel m_deviceName;
	public GameObject goReceivingBoardType;
	public GameObject goFrame;
	public GameObject goBtnOK;
	public GameObject goErrorConnect;
	public UILabel  labelRename;

	[SerializeField]
	private GameObject	goFirmwareInfo;

    string m_name = "";
	int boardType = 0;
	int tempBoardType = 0;
    public GameObject KJBluetoothReceiver; 
	private bool firmwareInfo = false;
	private readonly int OLD_BOARD_TYPE = 9;

    string address = string.Empty;

    void Start()
    {
        m_panelChangeDeviceName.GetComponent<UIPanel>().depth = BluetoothManager.Instance.NextDepth();

        address = PlayerPrefs.GetString("deviceName");//KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
        m_deviceName.text = address;
        
        string test = "MOO-123456789";
        if (test.StartsWith("MOBLO-"))
        {
            m_name = test.Substring("MOBLO-".Length);
        }
        else
        {
            m_name = test;
        }
        Debug.Log("m_name :" + m_name);
        DoOpen();
		StartCoroutine("WaitResponse");
    }


    void DoOpen()
    {
    //    Debug.Log("BTPopupBluetoothConnectionSucessView : DoOpen");
        _RegistMessage();
    }
    void DoClose()
    {
     //   Debug.Log("BTPopupBluetoothConnectionSucessView : DoClose");
        _UnregistMessage();
    }
    

    #region On Message

    void OnMessageConnectionFail(KJ.Message.Bluetooth.ConnectionFail i_message)
    {
        _UnregistMessage();
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothView");
    }
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {
        _UnregistMessage();
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothView");
    }
	

	void OnMessageBoardType(KJ.Message.Bluetooth.ReceiveBoardType type)
	{
		boardType = PlayerPrefs.GetInt ("BoardType");
		StopCoroutine("WaitResponse");

		if(type.boardType != boardType)
		{
			goReceivingBoardType.SetActive(false);
			ErrorConnect();

			return;
		}


		boardType = type.boardType;
		tempBoardType = type.boardType;
		goReceivingBoardType.SetActive(false);
		goFrame.SetActive(true);
		goBtnOK.SetActive(true);
		
		m_deviceName.text = address;
		string moblo = m_deviceName.text;
		
		if(moblo.StartsWith("MOBLO" + boardType.ToString()) == false)
		{
			
			moblo = moblo.Insert(5,boardType.ToString());
			m_deviceName.text = moblo;
		}
	}

	public void ErrorConnect()
	{
		goErrorConnect.SetActive(true);
	}

	public void OnQuit()
	{
		Application.Quit();
	}

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionFail>(this.OnMessageConnectionFail);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
		KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveBoardType>(this.OnMessageBoardType);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    #region On UI
    public void OnClickButtonOk()
    {
		StopCoroutine("WaitResponse");
        DoClose();
        CloseBTPopup();
    }

    public void OnClickButtonChangeDeviceView()
    {
        m_panelChangeDeviceName.SetActive(true);
		labelRename.text = "MOBLO" + boardType.ToString() + " -";

        if (m_deviceName.text != null)
        {
			if (m_deviceName.text.StartsWith("MOBLO" + boardType.ToString() + "-"))
			{
				m_input.value = m_deviceName.text.Substring(("MOBLO" + boardType.ToString() +"-").Length);
			}
			else
			{
				m_input.value = m_deviceName.text;
			}
        }
        //m_input.value = m_deviceName.text.Substring("MOBLO-".Length);
    }

    public void OnClickButtonDeviceNameConfirm()
    {
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.LoadNextScene());
        m_panelChangeDeviceName.SetActive(false);
    }


    public void OnSubmitDeviceName()
    {
		string text = "MOBLO" + boardType.ToString() + "-" + UIInput.current.value;
        m_deviceName.text = text;
   //     Debug.Log("OnSubmitDeviceName : " + text);
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(text);
        PlayerPrefs.SetString("deviceName", text);
        KJPluginMethods.methods.bluetooth.WriteDeviceName(text);

        DisableSystemUI.DisableNavUI();
    }

    #endregion On UI

	public void OnSetOldBoardType()
	{
		boardType = OLD_BOARD_TYPE;
		goReceivingBoardType.SetActive(false);
		goFrame.SetActive(true);
		goBtnOK.SetActive(true);
		PlayerPrefs.SetInt("BoardType",boardType);
		m_deviceName.text = KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
		
		StopCoroutine("WaitResponse");
	}


	IEnumerator WaitResponse()
	{
		while(tempBoardType <= 0)
		{
			yield return new WaitForSeconds(4.0f);
	//		Debug.Log ("WaitResponse");
			KJPluginMethods.methods.bluetooth.requestBoardType();

			if(firmwareInfo == false)
			{
				goReceivingBoardType.SetActive(false);
				goFirmwareInfo.SetActive(true);
				firmwareInfo = true;
			}
		}
	}
}
