﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void DelegatePO(object param);

public class BluetoothManager : MonoBehaviour {

    bool m_isConnected = false;
    bool isIngame = false;
    int m_count = 0;
    int m_depth = 9000;

    GameObject m_gameObjectBluetoothIcon = null;

    public List<GameObject> m_goPoolList = new List<GameObject>();
    private static BluetoothManager instance;
    public static BluetoothManager Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.Log("BluetoothManager instance = null");
                instance = new BluetoothManager();
            }
            return instance;
        }
    }

    /*
     * 블루투스 접속 상태 true/false
     * */
    public bool isConnected()
    {
        return m_isConnected;
    }

    /*
     *  블루투스 접속 상태가 끊어진 상태면 재접속 유도 팝업. 
     * */
    public void CheckBluetooth()
    {
        Debug.Log("CheckBluetooth : " + isIngame + "," + m_isConnected + ",count :" + m_count);
        if (m_isConnected == true)
        {
            return;
        }
//        if (isIngame == false)
//        {
//            return;
//        }
        if (m_count == 0)
        {
            CreateBTPopup("BTPopupBluetoothView", true);
        }
    }

    /*
     * 블루투스 접속 성공시 .KJBluetoothReceiver 함수와 연동하도록 함.
     * */
    public void BluetoothConnect()
    {
        if (isIngame == false) isIngame = true;
        m_isConnected = true;

        Debug.Log("BluetoothConnect : " + isIngame + "," + m_isConnected);
    }
    /*
     * 블루투스 접속이 끊어졌을때 .KJBluetoothReceiver 함수와 연동하도록 함.
     * */
    public void BluetoothDisConnected()
    {
        m_isConnected = false;
        if (isIngame == false)
        {
            return;
        }
        if (m_count == 0)
        {
            CreateBTPopup("BTPopupBluetoothView", true);
        }
    }


	// Use this for initialization
	void Start () {
        
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if (instance == null)
        {
            Debug.Log("Start BluetoothManager instance = null");
            instance = this;
            DontDestroyOnLoad(this);

            //GameObject o = CreateBTPopup("BTPopupBluetoothView", true);
            //o.GetComponent<BTPopupBluetoothView>().Init(Test);
        }
        else
        {
            Debug.Log("Start BluetoothManager instance");
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != instance)
                Destroy(this.gameObject);
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SetIcon(bool isShow, float x, float y)
    {
        if (isShow == true)
        {
            if (m_gameObjectBluetoothIcon == null)
            {
                GameObject root = GameObject.Find("UI Root");
                m_gameObjectBluetoothIcon = NGUITools.AddChild(root, Resources.Load("KJ/Prefabs/BluetoothPopup/BTPopupIcon") as GameObject);
                m_gameObjectBluetoothIcon.GetComponent<UIPanel>().depth = 9000;
            }
            m_gameObjectBluetoothIcon.SetActive(true);
            m_gameObjectBluetoothIcon.GetComponent<BTPopupIcon>().SetPosition(x, y);
        }
        else
        {
            if (m_gameObjectBluetoothIcon != null)
            {
                m_gameObjectBluetoothIcon.SetActive(false);
            }
        }
    }
    public void IconDestroyed()
    {
        m_gameObjectBluetoothIcon = null;
    }

    public GameObject CreateBTPopup(string a_prefabName, bool isBGAlpha = false)
    {
        GameObject root = GameObject.Find("UI Root");
        GameObject go;
        m_depth++;
        go = NGUITools.AddChild(root, Resources.Load("KJ/Prefabs/BluetoothPopup/" + a_prefabName) as GameObject);
        go.GetComponent<UIPanel>().depth = m_depth;
        BoxCollider box = go.GetComponent<BoxCollider>();
        if (box == null)
        {
            Debug.Log("CreatePopup Box collider is not found~ create box collider!");
            box = go.AddComponent<BoxCollider>();

            //box = NGUITools.AddWidget<BoxCollider>(go) BoxCollider:;

            box.size = new Vector3(1280f, 800f, 1f);
        }

        

        m_goPoolList.Add(go);
        m_count++;

        //Debug.Log("CreatePopup : " + a_prefabName + ",depth :" + m_depth);
        return go;
    }

    public void DeleteBTPopup(GameObject a_popup)
    {
        m_count--;
        if (m_count <= 0)
        {

        }

        m_goPoolList.Remove(a_popup);
        GameObject.Destroy(a_popup);

        //Debug.Log("DeletePopup : " + m_count + ",depth :" + m_depth);
    }
    public void RemoveAllBTPopup()
    {
        foreach (GameObject go in m_goPoolList)
        {
            GameObject.Destroy(go);
        }
        m_goPoolList.Clear();
        m_count = 0;
        if (m_count <= 0)
        {

        }
    }

    public int NextDepth()
    {
        m_depth++;
        return m_depth;
    }

    public int GetBTPopupCount()
    {
        return m_count;
    }
    public void SetDepth(int depth=9000)
    {
        if (depth < 9000) depth = 9000;
        m_depth = depth;
    }

}

public class BTPopup : MonoBehaviour
{
    DelegatePO m_closeCB;
    public virtual void Init(DelegatePO cb)
    {
        m_closeCB = cb;

        m_closeCB("init");
    }
    public virtual void CloseBTPopup(object arg=null)
    {
        BluetoothManager.Instance.DeleteBTPopup(gameObject);
        if (m_closeCB != null) m_closeCB(arg);
    }


}
