﻿using UnityEngine;
using System.Collections;

public class BTPopupIcon : MonoBehaviour {
    public UIButton m_iconButton;

    public UISprite m_bluetoothOn;
	public UISprite m_bluetoothOff;
	private bool CheckPopup = false;

	// Use this for initialization
	void Start () {
		//SetPosition(Screen.width*0.1f, Screen.height*0.1f)
		CheckPopup = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (BluetoothManager.Instance.isConnected() == true)
        {
            if (m_bluetoothOn.gameObject.activeSelf == false)
            {
                m_bluetoothOn.gameObject.SetActive(true);
            }
            if (m_bluetoothOff.gameObject.activeSelf == true)
            {
                m_bluetoothOff.gameObject.SetActive(false);
            }
        }
        else 
        {
            if (m_bluetoothOn.gameObject.activeSelf == true)
            {
                m_bluetoothOn.gameObject.SetActive(false);
            }
            if (m_bluetoothOff.gameObject.activeSelf == false)
            {
                m_bluetoothOff.gameObject.SetActive(true);
			}
			if( !CheckPopup )
			{
				BluetoothManager.Instance.CheckBluetooth();
				CheckPopup = true;
			}
        }
	}

    void Destroy()
    {
        Debug.Log("BTPopupIcon : Destroy");
        //BluetoothManager.Instance.Icon();
    }

    void OnDestroy()
    {
        Debug.Log("BTPopupIcon : OnDestroy");
        BluetoothManager.Instance.IconDestroyed();
    }
    public void SetPosition(float x, float y)
    {
        Vector3 pos = new Vector3(x, y, 0);
        m_iconButton.transform.position = NGUITools.GetRoot(gameObject).transform.FindChild("Camera").GetComponent<Camera>().ScreenToWorldPoint(pos);
    }

    public void OnClickButton()
    {
        BluetoothManager.Instance.CheckBluetooth();
    }

}
