﻿using UnityEngine;
using System.Collections;

public class BTPopupBluetoothConnectingView : BTPopup, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    string address = string.Empty;
    float m_timerForConnect = 0f;
    const float CONNECTTIME = 15f;
    void Start()
    {
        DoOpen();
    }

    void Update()
    {
        if (m_timerForConnect > 0f)
        {
            m_timerForConnect -= Time.deltaTime;
            if (m_timerForConnect <= 0f)
            {
                this._UnregistMessage();
                CloseBTPopup();
                GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothConnectionFailView");
            }
        }
    }


    /*protected override void DoLoadData()
    {
        Debug.Log("MainBluetoothConnectingView : " + "DoLoadData");
    }
    protected override void DoSaveData()
    {
        KJPluginMethods.methods.bluetooth.disconnect();
    }*/

    void DoOpen()
    {
        Debug.Log("MainBluetoothConnectingView : " + "DoOpen," + PlayerPrefs.GetString("address") + "," + PlayerPrefs.GetString("device"));
        this._RegistMessage();
        address = PlayerPrefs.GetString("address");

        KJPluginMethods.methods.bluetooth.connectDeviceByAddress(address);

        this.CheckTestAddress();

        m_timerForConnect = CONNECTTIME;
    }

    void DoClose()
    {
        m_timerForConnect = 0f;
        Debug.Log("MainBluetoothConnectingView : " + "DoClose");
        //NGUITools.SetActiveSelf(goRootView, false);

        this._UnregistMessage();
    }



    #region On Message
    void OnMessageConnectionSuccess(KJ.Message.Bluetooth.ConnectionSuccess i_message)
    {
        Debug.Log("OnMessageConnectionSuccess : " + i_message.deviceName);
        if (m_timerForConnect <= 0f)
        {
            return;
        }
        this._UnregistMessage();
        m_timerForConnect = 0f;
		KJPluginMethods.methods.bluetooth.requestBoardType(); //#boardmodelid
        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.Loop);
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothConnectionSucessView");
    }

    void OnMessageConnectionFail(KJ.Message.Bluetooth.ConnectionFail i_message)
    {
        if (m_timerForConnect <= 0f)
        {
            return;
        }
        this._UnregistMessage();
        //time out
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothConnectionFailView");
        
    }
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {

        // todo: 연결하자마자 끊긴다?
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionSuccess>(this.OnMessageConnectionSuccess);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionFail>(this.OnMessageConnectionFail);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    #region On UI
    public void OnClickButtonCancel()
    {
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopView());
    }
    #endregion On UI


    #region Condition Debug
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void SendMessageConnectionSuccess()
    {
        KJ.Message.Bluetooth.ConnectionSuccess message = new KJ.Message.Bluetooth.ConnectionSuccess("TEST");
        KJ.Handler.KJMessageHandler.SendMessage(message);
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void CheckTestAddress()
    {
        if (false == address.Equals("TEST_ADDRESS"))
        {
            return;
        }

        this.Invoke("SendMessageConnectionSuccess", 1f);
    }
    #endregion Condition Debug
}
