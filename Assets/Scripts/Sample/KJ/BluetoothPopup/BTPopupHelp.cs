﻿using UnityEngine;
using System.Collections;

public class BTPopupHelp : BTPopup
{
    WebViewObject webViewObject = null;

	// Use this for initialization
	void Start () {
        StartCoroutine(WebViewOpen("moblo1.html"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onButtonExit()
    {
        if (webViewObject != null)
        {
            webViewObject.SetVisibility(false);
        }
        CloseBTPopup();
    }


#if !UNITY_WEBPLAYER
    IEnumerator WebViewOpen(string Url)
#else
	void WebViewOpen(string Url)
#endif

    {

        webViewObject = GetComponent<WebViewObject>();
        if (webViewObject == null)
        {
            webViewObject =
                (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
            webViewObject.Init((msg) =>
            {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
            });
            int left = (int)(Screen.width * 0.03f);
            int top = (int)(Screen.height * 0.19f);
            int right = (int)(Screen.width * 0.03f);
            int bottom = (int)(Screen.height * 0.04f);
            webViewObject.SetMargins(left, top, right, bottom);
        }
        webViewObject.SetVisibility(true);

        switch (Application.platform)
        {
#if !UNITY_WEBPLAYER
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.IPhonePlayer:

                break;
            case RuntimePlatform.Android:
                var src = System.IO.Path.Combine(Application.streamingAssetsPath, Url);
                var dst = System.IO.Path.Combine(Application.persistentDataPath, Url);
                var result = "";
                if (src.Contains("://"))
                {
                    var www = new WWW(src);
                    yield return www;
                    result = www.text;
                }
                else
                {
                    result = System.IO.File.ReadAllText(src);
                }
                System.IO.File.WriteAllText(dst, result);
                //webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
                webViewObject.LoadURL("file:///android_asset/moblohelp.html");
                if (Application.platform != RuntimePlatform.Android)
                {
                    webViewObject.EvaluateJS(
                        "window.addEventListener('load', function() {" +
                        "	window.Unity = {" +
                        "		call:function(msg) {" +
                        "			var iframe = document.createElement('IFRAME');" +
                        "			iframe.setAttribute('src', 'unity:' + msg);" +
                        "			document.documentElement.appendChild(iframe);" +
                        "			iframe.parentNode.removeChild(iframe);" +
                        "			iframe = null;" +
                        "		}" +
                        "	}" +
                        "}, false);");
                }
                break;
#else
	case RuntimePlatform.OSXWebPlayer:
	case RuntimePlatform.WindowsWebPlayer:
		webViewObject.LoadURL(Url.Replace(" ", "%20"));
		webViewObject.EvaluateJS(
			"parent.$(function() {" +
			"	window.Unity = {" +
			"		call:function(msg) {" +
			"			parent.unityWebView.sendMessage('WebViewObject', msg)" +
			"		}" +
			"	};" +
			"});");
		break;
#endif
        }
    }


}
