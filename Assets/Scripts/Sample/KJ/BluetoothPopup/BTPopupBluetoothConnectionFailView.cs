﻿using UnityEngine;
using System.Collections;

public class BTPopupBluetoothConnectionFailView : BTPopup
{
    void Start()
    {

    }




    #region On UI
    public void OnClickButtonRetry()
    {
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothConnectingView");
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopView());
    }
    public void OnClickButtonCancel()
    {
        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothView");
        //KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopViewForType(typeof(MainBluetoothView))); 
    }
    #endregion On UI
}
