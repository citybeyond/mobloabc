﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[KJ.Model.KJSingletonMonoBehaviour(StartOnLoad = true)]
public sealed class BlockDataManager : KJ.Model.KJASingleton<BlockDataManager>, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    #region Const Values
	public const int s_boardRow = 9;//7; //block9
	public const int s_boardColumn = 9;//7; //block9
    public const int s_boardLayer = 10;

    public const int s_invalidID = -1;
    public const int s_invalidRow = -1;
    public const int s_invalidColumn = -1;
    public const int s_invalidLayer = -1;

    public const int s_maximumItemBlockCount = 4;

    public const int s_maximumBright = 5;
    public const int s_minimumBright = 0;

    const int s_processSetLedAllCount = 5;

    static readonly Color32 s_defaultLedColor = new Color32(255, 255, 255, 255);

    public static bool s_isHold = false;
    #endregion Const Values

    private BlockCommonData[, ,] _blockData = null;

    private IBlockDataReceiver _blockDataReceiver = null;
    public static IBlockDataReceiver blockDataReceiver
    {
        get
        {
            if (false == isConstructed)
            {
                return null;
            }

            return shared._blockDataReceiver;
        }
        set
        {
            if (false == isConstructed)
            {
                return;
            }

            shared._blockDataReceiver = value;
        }
    }

    public BlockCommonData this[int i_row, int i_column, int i_layer]
    {
        get
        {
            if (0 > i_row || 0 > i_column || 0 > i_layer || s_boardRow <= i_row || s_boardColumn <= i_column || s_boardLayer <= i_layer)
            {
                return null;
            }

            return _blockData[i_row, i_column, i_layer];
        }
    }
    public BlockCommonData this[int i_id]
    {
        get
        {
            int x, y, z;
            KJ.Utilities.Common.OneDimesionIndexToThreeDimesionIndex(i_id, BlockDataManager.s_boardRow, BlockDataManager.s_boardColumn, out x, out y, out z);

            return this[x, y, z];
        }
    }

    IEnumerator enumeratorLedLightAll = null;


    public override void Initialize()
    {
        base.Initialize();

        _blockData = new BlockCommonData[s_boardRow, s_boardColumn, s_boardLayer];

        this._RegistMessage();
    }
    public override void Release()
    {
        this._UnregistMessage();

        base.Release();
    }


    #region Clear Block Data
    private void ClearBlockData(BlockCommonData i_blockData)
    {
        _blockData[i_blockData.x, i_blockData.y, i_blockData.z] = null;

        i_blockData.Clear();
        KJ.Handler.KJObjectPoolHandler.shared.LoseObject(i_blockData);
    }

    private void InnerClearBlockDataAll(bool i_noRemoveCallback)
    {
        foreach (BlockCommonData data in _blockData)
        {
            if (null == data || false == data.isAttached)
            {
                continue;
            }

            this.RemoveBlockData(data, i_noRemoveCallback);
        }
    }
    public static void ClearBlockDataAll(bool i_noRemoveCallback)
    {
        if (false == isConstructed)
        {
            return;
        }

        BlockDataManager.shared.InnerClearBlockDataAll(i_noRemoveCallback);
    }
    #endregion Clear Block Data


    #region Get Block Data
    public BlockCommonData GetBlockData(int i_row, int i_column, int i_layer)
    {
        return this[i_row, i_column, i_layer];
    }

    public List<BlockCommonData> GetBlockData(KJBluetoothValues.BlockType i_blockType)
    {
        List<BlockCommonData> listBlockData = new List<BlockCommonData>();

        foreach (BlockCommonData blockCommonData in _blockData)
        {
            if (null == blockCommonData || i_blockType != blockCommonData.type)
            {
                continue;
            }

            listBlockData.Add(blockCommonData);
        }

        return listBlockData;
    }

    public List<T> GetBlockData<T>() where T : BlockCommonData, new()
    {
        List<T> listBlockData = new List<T>();

        foreach (BlockCommonData blockCommonData in _blockData)
        {
            if (null == blockCommonData || false == blockCommonData is T)
            {
                continue;
            }

            listBlockData.Add(blockCommonData as T);
        }

        return listBlockData;
    }

    /// <summary>
    /// get sub block data
    /// </summary>
    /// <param name="i_type">sub block type</param>
    /// <param name="i_row">x</param>
    /// <param name="i_column">y</param>
    /// <param name="i_layer">z</param>
    /// <param name="i_at">only using item block(4 direction)</param>
    /// <returns>sub block common data</returns>
    private SubBlockCommonData GetSubBlockData(KJBluetoothValues.SubBlockType i_type, int i_row, int i_column, int i_layer, int i_at)
    {
        BlockCommonData data = this.GetBlockData(i_row, i_column, i_layer);
        return data.FindSubBlockData(i_type, i_at);
    }


    public List<BlockCommonData> GetAllEnterBlock()
    {
        List<BlockCommonData> list = new List<BlockCommonData>();

        foreach (BlockCommonData data in _blockData)
        {
            if (null != data && true == data.isAttached)
            {
                list.Add(data);
            }
        }

        return list;
    }

    public GameObject[] GetAllBlockObject()
    {
        List<GameObject> list = new List<GameObject>();

        foreach (BlockCommonData data in _blockData)
        {
            if (null != data && true == data.isAttached)
            {
                list.Add(data.goBlock);
            }
        }

        return list.ToArray();
    }

    public BlockCommonData GetUnderEnterBlock(int i_row, int i_column, int i_layer)
    {
        BlockCommonData data = null;

        if (i_layer != 0)
        {
            data = BlockDataManager.shared[i_row, i_column, i_layer - 1];
        }

        return data;
    }

    public List<GameObject> GetBlockObjectOfLayer(KJBluetoothValues.BlockType i_blockType, int i_layer , bool isUpBlock)
    {
        List<GameObject> list = new List<GameObject>();

        for (int row = 0; row < s_boardRow; ++row)
        {
            for (int column = 0; column < s_boardColumn; ++column)
            {
                BlockCommonData data = BlockDataManager.shared[row, column, i_layer];
                if (data != null && data.type == i_blockType)
                {
                    if (isUpBlock == true)
                    {
                        if (BlockDataManager.shared[row, column, i_layer + 1] == null)
                        {
                            list.Add(data.goBlock);
                        }
                    }
                    else
                    {
                        list.Add(data.goBlock);
                    }
                }
            }
        }

        return list;
    }

    public int GetTopLayerBlockData(int i_row, int i_column)
    {
        for (int z = s_boardLayer - 1; z >= 0; --z)
        {
            if (_blockData[i_row, i_column, z].isAttached == true)
            {
                return z;
            }
        }

        return s_invalidLayer;
    }
    public int GetEmptyTopLayer(int i_row, int i_column)
    {
        for (int z = 0; z < s_boardLayer; ++z)
        {
            if (_blockData[i_row, i_column, z].isAttached == false)
            {
                return z;
            }
        }

        return s_invalidLayer;
    }

    [System.Obsolete("This is no longer use. Using GetBlockData(KJBluetoothValues.BlockType)")]
    public List<BlockCommonData> GetBlockDataWithType(KJBluetoothValues.BlockType i_blockType)
    {
        return this.GetBlockData(i_blockType);
    }
    [System.Obsolete("This is no longer use. Using GetBlockData<T>()")]
    public List<T> GetBlockDataForClass<T>() where T : BlockCommonData, new()
    {
        return this.GetBlockData<T>();
    }
    #endregion Get Block Data


    public bool IsUseSubBlock()
    {
        foreach (BlockCommonData blockData in _blockData)
        {
            if (blockData != null && blockData.HasAnySubBlockData() == true)
            {
                return true;
            }
        }

        return false;
    }


    #region Handle Main Block
    private void AddBlockData(BluetoothSerialize.ABlockData i_blockData, int i_id, int i_row, int i_column, int i_layer, KJBluetoothValues.BlockType i_type, KJBluetoothValues.BlockCategory i_category)
    {
        BlockCommonData data = null;

        switch (i_type)
        {
            case KJBluetoothValues.BlockType.GameNfc:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<GameBlockData>();
                break;

            case KJBluetoothValues.BlockType.ThreeRed:
            case KJBluetoothValues.BlockType.ThreeGreen:
            case KJBluetoothValues.BlockType.ThreeBlue:
            case KJBluetoothValues.BlockType.ThreeYellow:
            case KJBluetoothValues.BlockType.ThreeViolet:
            case KJBluetoothValues.BlockType.ThreeOrange:
            case KJBluetoothValues.BlockType.ThreeIndigo:

            case KJBluetoothValues.BlockType.TowerRed:
            case KJBluetoothValues.BlockType.TowerGreen:
            case KJBluetoothValues.BlockType.TowerBlue:
            case KJBluetoothValues.BlockType.TowerYellow:
            case KJBluetoothValues.BlockType.TowerViolet:
            case KJBluetoothValues.BlockType.TowerOrange:
            case KJBluetoothValues.BlockType.TowerIndigo:

                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<ThreeDBlockData>();
                break;
            case KJBluetoothValues.BlockType.ArtRed:
            case KJBluetoothValues.BlockType.ArtGreen:
            case KJBluetoothValues.BlockType.ArtBlue:
            case KJBluetoothValues.BlockType.ArtYellow:
            case KJBluetoothValues.BlockType.ArtViolet:
            case KJBluetoothValues.BlockType.ArtOrange:
            case KJBluetoothValues.BlockType.ArtIndigo:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<ArtBlockData>();
                break;
            case KJBluetoothValues.BlockType.Rgb:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<RgbBlockData>();
                break;
            case KJBluetoothValues.BlockType.Censor:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<CensorBlockData>();
                break;

            case KJBluetoothValues.BlockType.SpecialTouch:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<TouchBlockData>();
                break;

            case KJBluetoothValues.BlockType.SpecialMotor:
                data = KJ.Handler.KJObjectPoolHandler.shared.ObtainObject<MotorBlockData>();
                break;

            default:
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "invalid category");
                return;
        }

        data.id = i_id;
        data.type = i_type;
        data.category = i_category;
        data.x = i_row;
        data.y = i_column;
        data.z = i_layer;
        data.isAttached = true;

        _blockData[i_row, i_column, i_layer] = data;

        if (null != _blockDataReceiver)
        {
            switch (i_type)
            {
                case KJBluetoothValues.BlockType.GameNfc:
                    _blockDataReceiver.AddGameBlock((GameBlockData)data);
                    break;

                case KJBluetoothValues.BlockType.ThreeRed:
                case KJBluetoothValues.BlockType.ThreeGreen:
                case KJBluetoothValues.BlockType.ThreeBlue:
                case KJBluetoothValues.BlockType.ThreeYellow:
                case KJBluetoothValues.BlockType.ThreeViolet:
                case KJBluetoothValues.BlockType.ThreeOrange:
                case KJBluetoothValues.BlockType.ThreeIndigo:

                case KJBluetoothValues.BlockType.TowerRed:
                case KJBluetoothValues.BlockType.TowerGreen:
                case KJBluetoothValues.BlockType.TowerBlue:
                case KJBluetoothValues.BlockType.TowerYellow:
                case KJBluetoothValues.BlockType.TowerViolet:
                case KJBluetoothValues.BlockType.TowerOrange:
                case KJBluetoothValues.BlockType.TowerIndigo:
                    {
                        ThreeDBlockData three = (ThreeDBlockData)data;
                        BluetoothSerialize.ThreeDBlockData threeorg = (BluetoothSerialize.ThreeDBlockData)i_blockData;
                        three.cardID = threeorg.cardID;
                        three.ledState = threeorg.ledState;
                        if (three.ledState == 0) three.ledState = 2;

                        Debug.Log("AddBlockData final cardID : " + three.cardID);
                        Debug.Log("AddBlockData final ledState : " + three.ledState);
                    }
                    _blockDataReceiver.AddThreeDBlock((ThreeDBlockData)data);
                    break;
                case KJBluetoothValues.BlockType.ArtRed:
                case KJBluetoothValues.BlockType.ArtGreen:
                case KJBluetoothValues.BlockType.ArtBlue:
                case KJBluetoothValues.BlockType.ArtYellow:
                case KJBluetoothValues.BlockType.ArtViolet:
                case KJBluetoothValues.BlockType.ArtOrange:
                case KJBluetoothValues.BlockType.ArtIndigo:
                    
                    {
                        ArtBlockData atr = (ArtBlockData)data;
                        BluetoothSerialize.ArtBlockData artorg = (BluetoothSerialize.ArtBlockData)i_blockData;
                        atr.ledState = artorg.ledState;
                        if (atr.ledState == 0) atr.ledState = 2;
                    }
		    _blockDataReceiver.AddArtBlock((ArtBlockData)data);
                    break;
                case KJBluetoothValues.BlockType.Rgb:
                    _blockDataReceiver.AddRgbBlock((RgbBlockData)data);
                    break;
                case KJBluetoothValues.BlockType.Censor:
                    {
                        CensorBlockData censor = (CensorBlockData)data;
                        BluetoothSerialize.CensorBlockData censororg = (BluetoothSerialize.CensorBlockData)i_blockData;
                        censor.centimeter = censororg.centimeter;
                    }
                    _blockDataReceiver.AddCensorBlock((CensorBlockData)data);
                    break;
                
                case KJBluetoothValues.BlockType.SpecialTouch:
                    _blockDataReceiver.AddTouchBlock((TouchBlockData)data);
                    break;
                
                case KJBluetoothValues.BlockType.SpecialMotor:
                    _blockDataReceiver.AddMotorBlock((MotorBlockData)data);
                    break;

                default:
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "invalid category");
                    return;
            }
        }
    }
    private void RemoveBlockData(BlockCommonData i_blockCommonData, bool i_noRemoveCallback = false)
    {
        //CHI HOLD
        if (s_isHold == true)
        {
            return;
        }

        i_blockCommonData.isAttached = false;

        if (null != _blockDataReceiver)
        {
            switch (i_blockCommonData.type)
            {
                case KJBluetoothValues.BlockType.GameNfc:
                    //this.RemoveSubBlockData(i_blockCommonData, KJBluetoothValues.SubBlockType.Player, 0);
                    this.RemoveSubBlockData(i_blockCommonData, KJBluetoothValues.SubBlockType.Skin, 0);


                    if (true == i_noRemoveCallback)
                    {
                        GameObject.Destroy(i_blockCommonData.goBlock);
                    }
                    else
                    {
                        _blockDataReceiver.RemoveGameBlock((GameBlockData)i_blockCommonData);
                    }
                    break;

                case KJBluetoothValues.BlockType.ThreeRed:
                case KJBluetoothValues.BlockType.ThreeGreen:
                case KJBluetoothValues.BlockType.ThreeBlue:
                case KJBluetoothValues.BlockType.ThreeYellow:
                case KJBluetoothValues.BlockType.ThreeViolet:
                case KJBluetoothValues.BlockType.ThreeOrange:
                case KJBluetoothValues.BlockType.ThreeIndigo:

                case KJBluetoothValues.BlockType.TowerRed:
                case KJBluetoothValues.BlockType.TowerGreen:
                case KJBluetoothValues.BlockType.TowerBlue:
                case KJBluetoothValues.BlockType.TowerYellow:
                case KJBluetoothValues.BlockType.TowerViolet:
                case KJBluetoothValues.BlockType.TowerOrange:
                case KJBluetoothValues.BlockType.TowerIndigo:    
                    if (true == i_noRemoveCallback)
                    {
                        GameObject.Destroy(i_blockCommonData.goBlock);
                    }
                    else
                    {
                        _blockDataReceiver.RemoveThreeDBlock((ThreeDBlockData)i_blockCommonData);
                    }
                    break;
                case KJBluetoothValues.BlockType.ArtRed:
                case KJBluetoothValues.BlockType.ArtGreen:
                case KJBluetoothValues.BlockType.ArtBlue:
                case KJBluetoothValues.BlockType.ArtYellow:
                case KJBluetoothValues.BlockType.ArtViolet:
                case KJBluetoothValues.BlockType.ArtOrange:
                case KJBluetoothValues.BlockType.ArtIndigo:

                    if (true == i_noRemoveCallback)
                    {
                        GameObject.Destroy(i_blockCommonData.goBlock);
                    }
                    else
                    {
                        _blockDataReceiver.RemoveArtBlock((ArtBlockData)i_blockCommonData);
                    }
                    break;
                case KJBluetoothValues.BlockType.Rgb:
                    {
                        if (true == i_noRemoveCallback)
                        {
                            GameObject.Destroy(i_blockCommonData.goBlock);
                        }
                        else
                        {
                            _blockDataReceiver.RemoveRgbBlock((RgbBlockData)i_blockCommonData);
                        }
                    }
                    break;
                case KJBluetoothValues.BlockType.Censor:
                    {
                        if (true == i_noRemoveCallback)
                        {
                            GameObject.Destroy(i_blockCommonData.goBlock);
                        }
                        else
                        {
                            _blockDataReceiver.RemoveCensorBlock((CensorBlockData)i_blockCommonData);
                        }
                    }
                    break;


                case KJBluetoothValues.BlockType.SpecialTouch:
                    {
                        if (true == i_noRemoveCallback)
                        {
                            GameObject.Destroy(i_blockCommonData.goBlock);
                        }
                        else
                        {
                            _blockDataReceiver.RemoveTouchBlock(i_blockCommonData);
                        }
                    }
                    break;
                
                case KJBluetoothValues.BlockType.SpecialMotor:
                    {
                        if (true == i_noRemoveCallback)
                        {
                            GameObject.Destroy(i_blockCommonData.goBlock);
                        }
                        else
                        {
                            _blockDataReceiver.RemoveMotorBlock(i_blockCommonData);
                        }
                    }
                    break;

                default:
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "invalid category");
                    return;
            }
        }

        this.ClearBlockData(i_blockCommonData);
    }
    //BlockData로 proc.
    private void HandleReceiveBlockData(BluetoothSerialize.ABlockData i_blockData)
    {
        if (KJBluetoothValues.BlockState.Hold == i_blockData.blockState)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.LOG, "hold block");
            return;
        }

        BlockCommonData data = this[i_blockData.x, i_blockData.y, i_blockData.z];
        if (null == data)
        {
            if (KJBluetoothValues.BlockState.On == i_blockData.blockState)
            {
                this.AddBlockData(i_blockData, i_blockData.blockID, i_blockData.x, i_blockData.y, i_blockData.z, i_blockData.type, i_blockData.category);
            }
            else if(KJBluetoothValues.BlockState.Hold != i_blockData.blockState)
            {
                //Debug.Log("invalid block data");
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.LOG, "invalid block data");
                return;
            }
        }
        else if (i_blockData.type == data.type)
        {
            if (true == data.isAttached && KJBluetoothValues.BlockState.On == i_blockData.blockState)
            {
                return;
            }

            this.RemoveBlockData(data);
        }
        else
        {
            if (KJBluetoothValues.BlockState.On == i_blockData.blockState)
            {
                if (true == data.isAttached)
                {
                    this.RemoveBlockData(data);
                }

                this.AddBlockData(i_blockData, i_blockData.blockID, i_blockData.x, i_blockData.y, i_blockData.z, i_blockData.type, i_blockData.category);
            }
            else
            {
                this.RemoveBlockData(data);
            }
        }
    }

    //Temp Debug File Log
    //todo : 차후 삭제
    string fileLogName = string.Empty;

    private void MakeLogFile()
    {
        string path = string.Format("{0}/Log", Application.persistentDataPath);
        System.IO.Directory.CreateDirectory(path);

        fileLogName = string.Format("{0}/{1}_log.txt", path, DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss"));
    }

    private void ProcessFilePrinting(string i_strMessage)
    {
        if (true == string.IsNullOrEmpty(fileLogName))
        {
            this.MakeLogFile();
        }

        string text = string.Empty;
        text = string.Format("[{0}] - {1}", "ERROR", i_strMessage);

        System.IO.File.AppendAllText(fileLogName, string.Format("[{0}] {1}{2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), text, System.Environment.NewLine));
    }
    #endregion Handle Main Block


    #region Handle Sub Block

    private void AddSubBlockData(int i_id, int i_row, int i_column, int i_layer, int i_at, KJBluetoothValues.SubBlockType i_subBlockType)
    {
        KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.LOG, "AddSubBlockData - ID : {0} , row : {1} , column : {2} , layer : {3} , at : {4} , subBlockType : {5}", i_id , i_row , i_column , i_layer , i_at , i_subBlockType.ToString());
        //this.ProcessFilePrinting(string.Format("AddSubBlockData - ID : {0} , row : {1} , column : {2} , layer : {3} , at : {4} , subBlockType : {5}", i_id, i_row, i_column, i_layer, i_at, i_subBlockType.ToString()));

        BlockCommonData data = this[i_row, i_column, i_layer];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not find block data, failed add sub: {0}/{1}, {2}, {3}", i_id, i_row, i_column, i_layer);
            return;
        }

        SubBlockCommonData find = data.FindSubBlockData(i_subBlockType, i_at);

        find.id = i_id;
        find.type = i_subBlockType;
        find.isAttached = true;
        find.mainBlockData = data;

        if (null != _blockDataReceiver)
        {
            switch (find.type)
            {
                

                case KJBluetoothValues.SubBlockType.Skin:
                    _blockDataReceiver.AddSkinBlock(find);
                    break;

                default:
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "invalid sub type");
                    return;
            }
        }
    }
    private void RemoveSubBlockData(BlockCommonData i_blockData, KJBluetoothValues.SubBlockType i_subBlockType, int i_at, bool i_noRemoveCallback = false)
    {
        SubBlockCommonData subBlockData = i_blockData.FindSubBlockData(i_subBlockType, i_at);
        if (false == subBlockData.isAttached)
        {
            return;
        }

        subBlockData.isAttached = false;

        if (true == i_noRemoveCallback)
        {
            GameObject.Destroy(subBlockData.goBlock);
        }
        else
        {
            if (null != _blockDataReceiver)
            {
                switch (subBlockData.type)
                {
                    

                    case KJBluetoothValues.SubBlockType.Skin:
                        _blockDataReceiver.RemoveSkinBlock(subBlockData);
                        break;

                    default:
                        KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "invalid sub type");
                        return;
                }
            }
        }

        subBlockData.Clear();
    }

    private void HandleReceiveSubBlockData(KJ.Message.Bluetooth.AReceiveDataSub i_subBlockData)
    {
        if (KJBluetoothValues.BlockState.Hold == i_subBlockData.state)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.LOG, "hold block");
            return;
        }

        BlockCommonData data = this[i_subBlockData.x, i_subBlockData.y, i_subBlockData.z];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.LOG, "invalid block data with sub");
            return;
        }

        SubBlockCommonData subBlockData = data.FindSubBlockData(i_subBlockData.type, i_subBlockData.at);

        if (i_subBlockData.id == subBlockData.id)
        {
            if (true == subBlockData.isAttached && KJBluetoothValues.BlockState.On == i_subBlockData.state)
            {
                return;
            }

            this.RemoveSubBlockData(data, i_subBlockData.type, i_subBlockData.at);
        }
        else
        {
            if (KJBluetoothValues.BlockState.On == i_subBlockData.state)
            {
                if (true == data.isAttached)
                {
                    this.RemoveSubBlockData(data, i_subBlockData.type, i_subBlockData.at);
                }

                this.AddSubBlockData(i_subBlockData.id, i_subBlockData.x, i_subBlockData.y, i_subBlockData.z, i_subBlockData.at, i_subBlockData.type);
            }
            else
            {
                this.RemoveSubBlockData(data, i_subBlockData.type, i_subBlockData.at);
            }
        }
    }
    #endregion Handle Sub Block



    #region Led Block Control
    IEnumerator SetLedLightAll(List<RgbBlockData> i_listLedBlockData, bool i_on)
    {
        int processCount = 0;

        foreach (RgbBlockData ledBlockData in i_listLedBlockData)
        {
            if (i_on == ledBlockData.isOn)
            {
                continue;
            }

            KJPluginMethods.methods.bluetooth.setRgb((byte)ledBlockData.x, (byte)ledBlockData.y, (byte)ledBlockData.z, s_defaultLedColor, (byte)s_maximumBright);

            ++processCount;
            if (s_processSetLedAllCount == processCount)
            {
                yield return null;
            }
        }
    }

    public void SetLedLightAll(bool i_on)
    {
        if (null != enumeratorLedLightAll)
        {
            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Coroutine.StopCoroutine(enumeratorLedLightAll));
        }

        List<RgbBlockData> list = BlockDataManager.shared.GetBlockData<RgbBlockData>();

        enumeratorLedLightAll = this.SetLedLightAll(list, i_on);

        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Coroutine.StartCoroutine(enumeratorLedLightAll));
    }
    #endregion Led Block Control
    

    #region On Message
    private void OnMessageReceiveDataGameBlock(KJ.Message.Bluetooth.ReceiveDataGameBlock i_message)
    {
        //Debug.Log("OnMessageReceiveDataArtBlock : " + i_message.receiveData.ledState);
        this.HandleReceiveBlockData(i_message.receiveData);
    }
    private void OnMessageReceiveDataThreeDBlock(KJ.Message.Bluetooth.ReceiveDataThreeDBlock i_message)
    {
        //Debug.Log("OnMessageReceiveDataThreeDBlock : " + i_message.receiveData.ledState + "cardID : " + i_message.receiveData.cardID);
        this.HandleReceiveBlockData(i_message.receiveData);
    }
    private void OnMessageReceiveDataArtBlock(KJ.Message.Bluetooth.ReceiveDataArtBlock i_message)
    {
        //Debug.Log("OnMessageReceiveDataArtBlock : " + i_message.receiveData.ledState);
        this.HandleReceiveBlockData(i_message.receiveData);
    }
    private void OnMessageReceiveDataRgbBlock(KJ.Message.Bluetooth.ReceiveDataRgbBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }
    private void OnMessageReceiveDataCensorBlock(KJ.Message.Bluetooth.ReceiveDataCensorBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }

    private void OnMessageReceiveDataRemoveBlock(KJ.Message.Bluetooth.ReceiveDataRemoveBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }

    private void OnMessageReceiveDataTouchBlock(KJ.Message.Bluetooth.ReceiveDataTouchBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }
    /*private void OnMessageReceiveDataLcdBlock(KJ.Message.Bluetooth.ReceiveDataLcdBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }*/
    private void OnMessageReceiveDataMotorBlock(KJ.Message.Bluetooth.ReceiveDataMotorBlock i_message)
    {
        this.HandleReceiveBlockData(i_message.receiveData);
    }


    private void OnMessageReceiveDataSkin(KJ.Message.Bluetooth.ReceiveDataSkin i_message)
    {
        this.HandleReceiveSubBlockData(i_message);
    }
    
    private void OnMessageReceiveDataRgbColor(KJ.Message.Bluetooth.ReceiveDataRgbColor i_message)
    {
        BlockCommonData data = this[i_message.x, i_message.y, i_message.z];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not found led: {0}, {1}, {2}", i_message.x, i_message.y, i_message.z);
            return;
        }

        if (data is RgbBlockData)
        {
            RgbBlockData led = data as RgbBlockData;
            led.bright = i_message.bright;
            led.color = i_message.color;

            if (null != _blockDataReceiver)
            {
                _blockDataReceiver.ChangeRgbColor(led);
            }
        }
        else
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "is not led: {0}, {1}, {2}", i_message.x, i_message.y, i_message.z);
        }
    }

    private void OnMessageReceiveDataTouchState(KJ.Message.Bluetooth.ReceiveDataTouchState i_message)
    {
        BlockCommonData data = this[i_message.receiveTouchBlock.x, i_message.receiveTouchBlock.y, i_message.receiveTouchBlock.z];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not found touch: {0}, {1}, {2}", i_message.receiveTouchBlock.x, i_message.receiveTouchBlock.y, i_message.receiveTouchBlock.z);
            return;
        }

        if (data is TouchBlockData)
        {
            TouchBlockData blockData = data as TouchBlockData;
            if (0 == blockData.prevDirectionStates.Length)
            {
                blockData.prevDirectionStates = new KJBluetoothValues.TouchState[i_message.receiveTouchBlock.directionStates.Length];
            }
            else
            {
                blockData.prevDirectionStates = blockData.directionStates;
            }

            blockData.directionStates = new KJBluetoothValues.TouchState[i_message.receiveTouchBlock.directionStates.Length];
            for (int i = 0; i < i_message.receiveTouchBlock.directionStates.Length; ++i)
            {
                blockData.directionStates[i] = (KJBluetoothValues.TouchState)i_message.receiveTouchBlock.directionStates[i];
            }

            if (null != _blockDataReceiver)
            {
                _blockDataReceiver.ChangeTouchState(blockData);
            }
        }
        else
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "is not touch: {0}, {1}, {2}", i_message.receiveTouchBlock.x, i_message.receiveTouchBlock.y, i_message.receiveTouchBlock.z);
        }
    }



    private void OnMessageReceiveDataMotorState(KJ.Message.Bluetooth.ReceiveDataMotorState i_message)
    {
        BlockCommonData data = this[i_message.receiveMotorBlock.x, i_message.receiveMotorBlock.y, i_message.receiveMotorBlock.z];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not found motor: {0}, {1}, {2}", i_message.receiveMotorBlock.x, i_message.receiveMotorBlock.y, i_message.receiveMotorBlock.z);
            return;
        }

        if (data is MotorBlockData)
        {
            MotorBlockData blockData = data as MotorBlockData;
            blockData.direction = (KJBluetoothValues.MotorDirection)i_message.receiveMotorBlock.direction;
            // hack: caof: 2015-03-02: 하드웨어에서 반환값으로 잘못된 값이 옴(2 -> 5, 3 ->6 으로 변환)
            switch (blockData.direction)
            {
                case KJBluetoothValues.MotorDirection.CW2:
                    blockData.direction = KJBluetoothValues.MotorDirection.CW;
                    break;

                case KJBluetoothValues.MotorDirection.CCW2:
                    blockData.direction = KJBluetoothValues.MotorDirection.CCW;
                    break;
            }
            //->

            blockData.speed = i_message.receiveMotorBlock.speed;

            if (null != _blockDataReceiver)
            {
                _blockDataReceiver.ChangeMotorState(blockData);
            }
        }
        else
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "is not motor: {0}, {1}, {2}", i_message.receiveMotorBlock.x, i_message.receiveMotorBlock.y, i_message.receiveMotorBlock.z);
        }
    }

    private void OnMessageReceiveDataCensorValue(KJ.Message.Bluetooth.ReceiveDataCensorValue i_message)
    {
        BlockCommonData data = this[i_message.receiveCensorBlock.x, i_message.receiveCensorBlock.y, i_message.receiveCensorBlock.z];
        if (null == data)
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not found touch: {0}, {1}, {2}", i_message.receiveCensorBlock.x, i_message.receiveCensorBlock.y, i_message.receiveCensorBlock.z);
            return;
        }

        if (data is CensorBlockData)
        {
            CensorBlockData blockData = data as CensorBlockData;

            blockData.centimeter = i_message.receiveCensorBlock.centimeter;
            if (null != _blockDataReceiver)
            {
                _blockDataReceiver.ChangeCensorValue(blockData);
            }
        }
        else
        {
            KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "is not touch: {0}, {1}, {2}", i_message.receiveCensorBlock.x, i_message.receiveCensorBlock.y, i_message.receiveCensorBlock.z);
        }
    }



    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataGameBlock>(this.OnMessageReceiveDataGameBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataThreeDBlock>(this.OnMessageReceiveDataThreeDBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataArtBlock>(this.OnMessageReceiveDataArtBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataRgbBlock>(this.OnMessageReceiveDataRgbBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataRemoveBlock>(this.OnMessageReceiveDataRemoveBlock);

        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataTouchBlock>(this.OnMessageReceiveDataTouchBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataMotorBlock>(this.OnMessageReceiveDataMotorBlock);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataCensorBlock>(this.OnMessageReceiveDataCensorBlock);

        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataPlayer>(this.OnMessageReceiveDataPlayer);
        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataItem>(this.OnMessageReceiveDataItem);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataSkin>(this.OnMessageReceiveDataSkin);

        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataLedColor>(this.OnMessageReceiveDataLedColor);

        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataTouchState>(this.OnMessageReceiveDataTouchState);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataMotorState>(this.OnMessageReceiveDataMotorState);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveDataCensorValue>(this.OnMessageReceiveDataCensorValue);

    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message
}
