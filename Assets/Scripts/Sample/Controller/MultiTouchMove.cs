﻿using UnityEngine;
using System.Collections;

public class MultiTouchMove : MonoBehaviour
{
	public GameObject CameraRotateObj;
	public GameObject CameraMoveObj;
	public Camera MainCamera;

	Vector2 MouseDownPos;
	Vector2 MousePos;
	bool MouseDownBool;
	Vector2 FirstPos;
	Vector2 DragDistance;
	Vector2 MoveDistance;
	
	bool FirstTwoTouch = false;
	bool FirstThreeTouch = false;
	Vector2 m_FirstTouchPos0;
	Vector2 m_FirstTouchPos1;
	Vector3 m_FirstTouchPos2;
	Vector2 PrevDistance;

	Vector2 m_PrevVector;
	float m_Angle;
	float m_PrevAngle;
	float m_MoveAngle;

	Vector2 m_CenterPos;

	bool m_TouchEnd = false;
	float m_TouchEndTimer = 0.1f;
    //Vector2 m_PrevTouchPos0;
    //Vector2 m_PrevTouchPos1;

	//bool m_RotationMode = false;
	//bool m_ZoomMode = false;

    //bool m_SignePlus = false;

	Vector2 m_TouchPos0 = Vector2.zero;
	Vector2 m_TouchPos1 = Vector2.zero;
	
	// Use this for initialization
	void Start () {
		ObjectDataClear();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}

		if(m_TouchEnd == true && Input.touchCount == 0){
			m_TouchEndTimer = m_TouchEndTimer-Time.deltaTime;
			if(m_TouchEndTimer < 0.0f){
				m_TouchEndTimer = 0.1f;
				m_TouchEnd = false;
				//m_RotationMode = false;
				//m_ZoomMode = false;
			}
		}

		foreach(Touch touch in Input.touches)
		{
            Debug.Log("touch :" + touch.phase.ToString());

			switch(touch.phase)
			{
			case TouchPhase.Began:
				break;
			case TouchPhase.Moved:
				break;
			case TouchPhase.Ended:
				if(m_TouchEnd == false){
					m_TouchEnd = true;
				}
				break;
			}

			if (touch.tapCount == 2) {        
				// double-tap   
				ObjectDataClear();
			}
		}
        //Debug.Log("tc Count :" + Input.touchCount);
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("tc GetMouseButtonDown");
        }

		if(m_TouchEnd == false){
            if (Input.touchCount == 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    MouseDownPos = Input.mousePosition;
                    MouseDownBool = true;
                    Debug.Log("MouseDown 0");
                }
                if (MouseDownBool == true)
                {
                    if (Input.GetMouseButton(0) && null != CameraMoveObj)
                    {
                        Debug.Log("MouseDown 0-a");
                        MousePos = Input.mousePosition;
                        DragDistance.x = (MouseDownPos.x - MousePos.x) / 5;
                        DragDistance.y = (MouseDownPos.y - MousePos.y) / 5;
                        MouseDownPos = Input.mousePosition;

                        Vector3 moveCamera = new Vector3(DragDistance.x,
                                                                            0,
                                                                            DragDistance.y);

                        Vector3 v = CameraMoveObj.transform.TransformDirection(moveCamera);


                        CameraMoveObj.transform.localPosition += v;
                        Debug.Log("MouseDown 0-b");
                    }
                }
            }


			if(Input.touchCount == 1){
				if(Input.GetMouseButtonDown(0)){
					MouseDownPos = Input.mousePosition;
					MouseDownBool = true;
					Debug.Log("MouseDown");
				}
				if(MouseDownBool == true){
					if(Input.GetMouseButton(0) && null != CameraMoveObj){
						MousePos = Input.mousePosition;
						DragDistance.x = (MouseDownPos.x - MousePos.x)/5;
						DragDistance.y = (MouseDownPos.y - MousePos.y)/5;
						MouseDownPos = Input.mousePosition;
						
						Vector3 moveCamera = new Vector3(DragDistance.x,
																			0,
																			DragDistance.y);

						Vector3 v = CameraMoveObj.transform.TransformDirection(moveCamera);

						
						CameraMoveObj.transform.localPosition += v;
					}
				}
			}

			if(Input.touchCount == 2){
				if(FirstTwoTouch == false){
					FirstTwoTouch = true;
					m_FirstTouchPos0 = Input.GetTouch(0).position;
					m_FirstTouchPos1 = Input.GetTouch(1).position;
                    //m_PrevTouchPos0 = Input.GetTouch(0).position;
                    //m_PrevTouchPos1 = Input.GetTouch(1).position;

					PrevDistance.x = Mathf.Abs(m_FirstTouchPos0.x - m_FirstTouchPos1.x);
					PrevDistance.y = Mathf.Abs(m_FirstTouchPos0.y - m_FirstTouchPos1.y);

					m_PrevVector = m_FirstTouchPos0 - m_FirstTouchPos1;

					m_CenterPos.x = Mathf.Abs(m_FirstTouchPos0.x + m_FirstTouchPos1.x)/2;
					m_CenterPos.y = Mathf.Abs(m_FirstTouchPos0.y + m_FirstTouchPos1.y)/2;
					m_PrevAngle = 0;
				}else if(FirstTwoTouch == true){
					m_TouchPos0 = Input.GetTouch(0).position;
					m_TouchPos1 = Input.GetTouch(1).position;
					DragDistance.x = (PrevDistance.x - Mathf.Abs(m_TouchPos0.x - m_TouchPos1.x))/5;
					DragDistance.y = (PrevDistance.y - Mathf.Abs(m_TouchPos0.y - m_TouchPos1.y))/5;
					m_Angle = Vector2.Angle(m_PrevVector , (m_TouchPos0 - m_TouchPos1));
					Vector3 cross = Vector3.Cross(m_PrevVector, (m_TouchPos0 - m_TouchPos1));

					if (cross.z < 0){    
						m_Angle = 360 - m_Angle;
					}
					/*
					if(m_TouchPos0.x <= m_TouchPos1.x){
						if(m_TouchPos0.y <= m_PrevTouchPos0.y || m_TouchPos1.y >= m_PrevTouchPos1.y){
							m_SignePlus = true;
						}else{
							m_SignePlus = false;

						}
					}else{
						if(m_TouchPos1.y <= m_CenterPos.y || m_TouchPos0.y >= m_PrevTouchPos0.y){

							m_SignePlus = true;
						}else{
							m_SignePlus = false;

						}
						}

					if(m_SignePlus == false){
						m_Angle = -m_Angle;
					}
					//*/

					m_MoveAngle = m_Angle - m_PrevAngle;
					m_PrevAngle = m_Angle;

					/*
					if(m_RotationMode == false && m_ZoomMode == false){
						if((Mathf.Abs(Touch0Pos.x - m_CenterPos.x) >= Mathf.Abs(Input.GetTouch(0).position.x - m_CenterPos.x) &&
						   Mathf.Abs(Touch0Pos.y - m_CenterPos.y) <= Mathf.Abs(Input.GetTouch(0).position.y - m_CenterPos.y)) &&
						   (Mathf.Abs(Touch1Pos.x - m_CenterPos.x) >= Mathf.Abs(Input.GetTouch(1).position.x - m_CenterPos.x) &&
						 	Mathf.Abs(Touch1Pos.y - m_CenterPos.y) <= Mathf.Abs(Input.GetTouch(1).position.y - m_CenterPos.y))
						   ){
							m_RotationMode = true;
						}else{
							m_ZoomMode = true;
						}
					}
					//*/
					/*
					if(!((((Mathf.Abs(Touch0Pos.x - m_CenterPos.x) < Mathf.Abs(Input.GetTouch(0).position.x - m_CenterPos.x) &&
					    Mathf.Abs(Touch0Pos.y - m_CenterPos.y) <= Mathf.Abs(Input.GetTouch(0).position.y - m_CenterPos.y)) ||
					   (Mathf.Abs(Touch1Pos.x - m_CenterPos.x) < Mathf.Abs(Input.GetTouch(1).position.x - m_CenterPos.x) &&
					 Mathf.Abs(Touch1Pos.y - m_CenterPos.y) <= Mathf.Abs(Input.GetTouch(1).position.y - m_CenterPos.y))) ||
					   ((Mathf.Abs(Touch0Pos.x - m_CenterPos.x) > Mathf.Abs(Input.GetTouch(0).position.x - m_CenterPos.x) &&
					  Mathf.Abs(Touch0Pos.y - m_CenterPos.y) >= Mathf.Abs(Input.GetTouch(0).position.y - m_CenterPos.y)) ||
					 (Mathf.Abs(Touch1Pos.x - m_CenterPos.x) > Mathf.Abs(Input.GetTouch(1).position.x - m_CenterPos.x) &&
					 Mathf.Abs(Touch1Pos.y - m_CenterPos.y) >= Mathf.Abs(Input.GetTouch(1).position.y - m_CenterPos.y))))||
					   (((Mathf.Abs(Touch0Pos.x - m_CenterPos.x) <= Mathf.Abs(Input.GetTouch(0).position.x - m_CenterPos.x) &&
					   Mathf.Abs(Touch0Pos.y - m_CenterPos.y) < Mathf.Abs(Input.GetTouch(0).position.y - m_CenterPos.y)) ||
					  (Mathf.Abs(Touch1Pos.x - m_CenterPos.x) <= Mathf.Abs(Input.GetTouch(1).position.x - m_CenterPos.x) &&
					 Mathf.Abs(Touch1Pos.y - m_CenterPos.y) < Mathf.Abs(Input.GetTouch(1).position.y - m_CenterPos.y))) ||
					 ((Mathf.Abs(Touch0Pos.x - m_CenterPos.x) >= Mathf.Abs(Input.GetTouch(0).position.x - m_CenterPos.x) &&
					  Mathf.Abs(Touch0Pos.y - m_CenterPos.y) > Mathf.Abs(Input.GetTouch(0).position.y - m_CenterPos.y)) ||
					 (Mathf.Abs(Touch1Pos.x - m_CenterPos.x) >= Mathf.Abs(Input.GetTouch(1).position.x - m_CenterPos.x) &&
					 Mathf.Abs(Touch1Pos.y - m_CenterPos.y) > Mathf.Abs(Input.GetTouch(1).position.y - m_CenterPos.y)))))
					   ){
						m_RotationMode = true;
					}
					*/

					CameraRotateObj.transform.eulerAngles -= new Vector3(0 , m_MoveAngle , 0);

					float ZoomValue = 0.0f;
					if(Mathf.Abs(DragDistance.x) > Mathf.Abs(DragDistance.y)){
						ZoomValue = DragDistance.x/2;
					}else{
						ZoomValue = DragDistance.y/2;
					}
					if(m_MoveAngle > 0.5f || m_MoveAngle < - 0.5f){
						ZoomValue = ZoomValue/10;
					}
					MainCamera.fieldOfView = MainCamera.fieldOfView + ZoomValue;
					if(MainCamera.fieldOfView < 10){
						MainCamera.fieldOfView = 10;	
					}
					if(MainCamera.fieldOfView > 120){
						MainCamera.fieldOfView = 120;
					}


					PrevDistance.x = Mathf.Abs(Input.GetTouch(0).position.x - Input.GetTouch(1).position.x);
					PrevDistance.y = Mathf.Abs(Input.GetTouch(0).position.y - Input.GetTouch(1).position.y);
					//m_PrevTouchPos0 = Input.GetTouch(0).position;
					//m_PrevTouchPos1 = Input.GetTouch(1).position;
					//m_PrevVector = Input.GetTouch(0).position - Input.GetTouch(1).position;
				}
			}else{
				if(FirstTwoTouch == true){
					FirstTwoTouch = false;
				}
			}

			if(Input.touchCount == 3){

				if(FirstThreeTouch == false){
					FirstThreeTouch = true;
					m_FirstTouchPos0 = Input.GetTouch(0).position;
					m_FirstTouchPos2 = Input.GetTouch(2).position;
					PrevDistance.x = Mathf.Abs(m_FirstTouchPos0.x - m_FirstTouchPos2.x);
					PrevDistance.y = Mathf.Abs(m_FirstTouchPos0.y - m_FirstTouchPos2.y);
				}else if(FirstThreeTouch == true){
					DragDistance.x = (PrevDistance.x - Mathf.Abs(Input.GetTouch(0).position.x - Input.GetTouch(2).position.x))/5;
					DragDistance.y = (PrevDistance.y - Mathf.Abs(Input.GetTouch(0).position.y - Input.GetTouch(2).position.y))/5;

					CameraRotateObj.transform.eulerAngles += new Vector3(0/* + DragDistance.y*/ , DragDistance.x , 0);


					PrevDistance.x = Mathf.Abs(Input.GetTouch(0).position.x - Input.GetTouch(2).position.x);
					PrevDistance.y = Mathf.Abs(Input.GetTouch(0).position.y - Input.GetTouch(2).position.y);
				}
			}else{
				if(FirstThreeTouch == true){
					FirstThreeTouch = false;
				}
			}
		}
	}

	void OnDrawGizmos(){
//		Vector3 pos0 = new Vector3(m_TouchPos0.x, m_TouchPos0.y, 0);
//		Vector3 pos1 = new Vector3(m_TouchPos1.x, m_TouchPos1.y, 0);
//		float radius = 1;
//		Gizmos.DrawSphere(pos0, radius);
//		Gizmos.DrawSphere(pos1, radius);
	}

	void ObjectDataClear(){
		if(null != CameraMoveObj)
		{
			CameraMoveObj.transform.localPosition = Vector3.zero;
		}

		CameraRotateObj.transform.localRotation = Quaternion.identity;
		MainCamera.fieldOfView = 60;
	}

	void OnGUI(){
		/*
		GUI.Label(new Rect(Screen.width/2 - 50 , 10 , 100 , 30) , "TouchCount : " + Input.touchCount);
		GUI.Label(new Rect(Screen.width/2 - 100 , 30 , 200 , 30) , "DragDistance : " + DragDistance.x + " : " + DragDistance.y);
		GUI.Label(new Rect(Screen.width/2 - 100 , 60 , 200 , 30) , "Angle : " + m_Angle + " : " + m_MoveAngle);
		GUI.Label(new Rect(Screen.width/2 - 100 , 90 , 200 , 30) , "CenterPos : " + m_CenterPos.x + " : " + m_CenterPos.y);
		GUI.Label(new Rect(Screen.width/2 - 200 , 120 , 400 , 30) , "CenterPos : " + m_TouchPos0 + " : " + m_TouchPos1);
		//GUI.Label(new Rect(Screen.width/2 - 100 , 70 , 200 , 30) , "Touch0Pos : " + Input.GetTouch(0).position);
		//GUI.Label(new Rect(Screen.width/2 - 100 , 100 , 200 , 30) , "Touch1Pos : " + Input.GetTouch(1).position);
		*/
	}
}
