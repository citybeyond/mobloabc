﻿using UnityEngine;
using System.Collections;

public class MgrGameScene : MonoBehaviour {

	public CtrReady ctrReady;
	public CtrGameScene ctrGame;
	
	public GameObject objPause;

	public bool bReady;
	public bool bGame;
	
	public bool bTouchBlockMode = false;

	public int indexBoardType = 0;
	public int verticalNumber = 9;
	public int horizontalNumber = 9;
	public GameObject[] objBoards;
	public GameObject[] objBlockItems;
	public BlockListItem[] blocks;
	public static MgrGameScene Instance;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	void Start()
	{
		SoundManager.Instance.PlayBGM("S_SLP01_Tr12_01 A Sunny Day",0.25f);
		int _indexBoard = PlayerPrefs.GetInt("BoardType",0);

		if( _indexBoard == 9 )
			indexBoardType = 0;
		else if( _indexBoard == 7 )
			indexBoardType = 1;
		else if( _indexBoard == 5 )
			indexBoardType = 2;
		else
			indexBoardType = 0;

		SetBlockData();
	}
	public void ClickPause()
	{
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		Time.timeScale = 0f;
		objPause.SetActive(true);
		if( bReady )
			ctrReady.ctrCharacter.StopTalk();
		if( bGame )
			ctrGame.ctrCharacter.StopTalk();
	}

	public void ClickHome()
	{
		Time.timeScale = 1f;
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.LoadLevel("3.MainScene");
	}
	public void ClickResume()
	{
		Time.timeScale = 1f;
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		objPause.SetActive(false);
	}
	public void ClickExit()
	{
		Application.Quit();
	}


	void SetActiveBoard( int _index)
	{
		if( objBoards.Length != 0 )
		{
			for( int i = 0; i < objBoards.Length; ++i )
			{
				if( _index == i )
					objBoards[i].SetActive(true);
				else 
					objBoards[i].SetActive(false);
			}
			objBoards[_index].transform.parent.gameObject.SetActive(true);
		}

		if( _index == 0 ) //99
		{
			verticalNumber = 9;
			horizontalNumber = 9;
		}
		else if( _index == 1 ) //77
		{
			verticalNumber = 7;
			horizontalNumber = 7;
		}
		else if( _index == 2 ) //55
		{
			verticalNumber = 5;
			horizontalNumber = 5;
		}
	}
	
	public void SetBlockData()
	{
		SetActiveBoard(indexBoardType);

		blocks = new BlockListItem[horizontalNumber];
		
		Vector3 _pos;
		for( int i = 0; i < horizontalNumber; ++i )
		{
			blocks[i] = new BlockListItem();
			blocks[i].block = new BlockItem[verticalNumber];
			for( int j = 0; j < verticalNumber; ++j )
			{
				
				GameObject _block = Instantiate(objBlockItems[indexBoardType]) as GameObject;
				_block.transform.localPosition = Vector3.zero;
				BlockItem _blockItem = _block.GetComponent<BlockItem>();
				
				
				_blockItem.transform.name = "Block_"+i.ToString()+"_"+j.ToString();
				_blockItem.transform.parent = objBoards[indexBoardType].transform;
				_blockItem.transform.localScale = Vector3.one;
				_blockItem.x = i;
				_blockItem.y = j;
				_blockItem.imgBlock.gameObject.SetActive(false);
				_pos = _blockItem.transform.localPosition;
				_pos.z = 0;
				_blockItem.transform.localPosition = _pos;
				
				blocks[i].block[j] = _blockItem;
			}
		}
	}
}
