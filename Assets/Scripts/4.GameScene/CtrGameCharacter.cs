﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CtrGameCharacter : MonoBehaviour {
	
	public Animator animatorCharacter;
	public Animator animatorFace;
	public GameObject objBalloon;
	public Text textBalloon;
	public string strAction;
	public string[] strBalloonText;
	public string[] strSoundFileName;
	
	public bool bDisappearBalloon = false;
	private bool bPlayAnim = false;
	public float lengthSound;
	// Use this for initialization
	void Start () {
	
		//PlayTalk(0);
	}

	IEnumerator PlayAnimCo()
	{
		bPlayAnim = true;
		yield return new WaitForSeconds(0.1f);
		bPlayAnim = false;
	}
	public void PlayAnim( string _name )
	{
		if( bPlayAnim ) return;
		StartCoroutine( PlayAnimCo());
		 
		if( _name == "Talk" )
		{
			animatorCharacter.SetBool(_name,true);
			animatorFace.SetBool(_name,true);
		}
		else
		{
			animatorCharacter.SetTrigger(_name);
			animatorFace.SetTrigger(_name);

		}
	}

	public void PlayTalk( int _index )
	{
		objBalloon.SetActive(true);
		//Debug.Log ("PlayTalk "+ _index);
		textBalloon.text = strBalloonText[_index];
		
		//Debug.Log ("strSoundFileName "+ strSoundFileName[_index]);
		SoundManager.Instance.PlayEffect(strSoundFileName[_index],1f);
		lengthSound = SoundManager.Instance.lengthSound;
		if( _index >= 0 && _index < 8 )
		{
			if( Random.Range(0,2) == 0 )
				PlayAnim("Correct01");
			else
				PlayAnim("Correct02");
		}
		else if( _index >= 8 && _index < 12  )
		{
			if( Random.Range(0,2) == 0 )
				PlayAnim("Wrong01");
			else
				PlayAnim("Wrong02");
		}
		else if( _index >= 12 )
		{
			PlayAnim("Talk");
			StartCoroutine(TalkingCo());
		}

		if( bDisappearBalloon ) 
			StartCoroutine(SpeakCo());
	}
	
	IEnumerator SpeakCo()
	{
		yield return new WaitForSeconds(lengthSound );
		objBalloon.SetActive(false);
	}
	IEnumerator TalkingCo()
	{
		yield return new WaitForSeconds(lengthSound );
		StopTalk();
	}

	public void StopTalk()
	{
		animatorCharacter.SetBool("Talk",false);
		animatorFace.SetBool("Talk",false);
	}



	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
		if( Input.GetButton("Jump") )
			PlayAnim(strAction);
#endif
	}
}
