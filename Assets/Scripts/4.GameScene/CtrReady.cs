﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CtrReady : MonoBehaviour {

	public CtrGameCharacter ctrCharacter;
	public GameObject objGamePanel;
	public GameObject objReadyPanel;
	public GameObject[] objCardSets;
	public Button btnOK;
	public GameObject objEnableTouch;

	public GameObject objBoard;
//	public int verticalNumber = 9;
//	public int horizontalNumber = 9;
//	public GameObject objBoard;
//	public GameObject objBlockItem;
//	public BlockListItem[] blocks;

	public int countWord = 0;
	public Text textInfo;

	private bool bNFCBlock = false;
	private bool bTouchBlock = false;
	// Use this for initialization
	IEnumerator Start () {
		objCardSets[World.Instance.indexWordSet].SetActive(true);

		yield return new WaitForSeconds(0.3f);
		SetWordInfo();
		ctrCharacter.PlayTalk(15);

		//SetBlockData();
		btnOK.interactable = false;
		objEnableTouch.SetActive(false);

		MgrGameScene.Instance.bReady = true;
		MgrGameScene.Instance.bGame = false;
		MgrGameScene.Instance.bTouchBlockMode = false;

		//Request Moblo MemoryRead
		KJPluginMethods.methods.bluetooth.requestCommand(
			KJBluetoothValues.BluetoothCommands.MemoryRead);
	}

	public void ClickReadyOK()
	{
		ctrCharacter.StopTalk();
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		objGamePanel.GetComponent<CanvasGroup>().DOFade(1f,0.1f);
		objReadyPanel.SetActive(false);
		objBoard.SetActive(false);
		ctrCharacter.PlayTalk(17);

		MgrGameScene.Instance.bReady = false;
		MgrGameScene.Instance.bGame = true;
	}
	
	public void ClickTouchBlock()
	{
		ctrCharacter.StopTalk();
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		objEnableTouch.SetActive(true);
		bTouchBlock = true;
		MgrGameScene.Instance.bTouchBlockMode = true;
		if( bNFCBlock && bTouchBlock )
		{
			StartCoroutine(CoChangeGameScene());
//			objGamePanel.GetComponent<CanvasGroup>().DOFade(1f,0.1f);
//			objReadyPanel.SetActive(false);
//			objBoard.SetActive(false);
//			ctrCharacter.PlayTalk(17);
//			
//			MgrGameScene.Instance.bReady = false;
//			MgrGameScene.Instance.bGame = true;
		}
	}
	IEnumerator CoChangeGameScene()
	{
		yield return new WaitForSeconds(1f);
		objGamePanel.GetComponent<CanvasGroup>().DOFade(1f,0.1f);
		objReadyPanel.SetActive(false);
		objBoard.SetActive(false);
		ctrCharacter.PlayTalk(17);
		
		MgrGameScene.Instance.bReady = false;
		MgrGameScene.Instance.bGame = true;
	}

//	public void SetBlockData()
//	{
//		blocks = new BlockListItem[horizontalNumber];
//
//		Vector3 _pos;
//		for( int i = 0; i < horizontalNumber; ++i )
//		{
//			blocks[i] = new BlockListItem();
//			blocks[i].block = new BlockItem[verticalNumber];
//			for( int j = 0; j < verticalNumber; ++j )
//			{
//				
//				GameObject _block = Instantiate(objBlockItem) as GameObject;
//				_block.transform.localPosition = Vector3.zero;
//				BlockItem _blockItem = _block.GetComponent<BlockItem>();
//
//				
//				_blockItem.transform.name = "Block_"+i.ToString()+"_"+j.ToString();
//				_blockItem.transform.parent = objBoard.transform;
//				_blockItem.transform.localScale = Vector3.one;
//				_blockItem.x = i;
//				_blockItem.y = j;
//				_blockItem.imgBlock.gameObject.SetActive(false);
//				_pos = _blockItem.transform.localPosition;
//				_pos.z = 0;
//				_blockItem.transform.localPosition = _pos;
//				
//				blocks[i].block[j] = _blockItem;
//			}
//		}
//	}
	
	public void ReceiveData( string _rawdata)
	{
		Debug.Log ("ReceiveData "+_rawdata);
		char[] _data = new char[17];
		_data = _rawdata.ToCharArray();
		
		int _x = int.Parse(_data[2].ToString())-1;
		int _y = int.Parse(_data[3].ToString())-1;
		
		int _state = int.Parse(_data[6].ToString());	



		if( _data[5].ToString() == "N" )//NFC Block Check
		{
			if( _state == 1 || _state == 3)//Add, Hold
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetNFCBlockImage();
				bNFCBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
			}
		}
		else if( _data[5].ToString() == "T")//Touch Block Check
		{
			if( _state == 1 || _state == 3)//Add, Hold
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetTouchBlockImage();
				bTouchBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
			}
		}
		else
		{
			if(_state == 2) // Remove
			{
				MgrGameScene.Instance.blocks[_y].block[_x].DisableBlockImage();
				if( MgrGameScene.Instance.blocks[_y].block[_x].bNFCMode )
					bNFCBlock = false;
				else
					bTouchBlock = false;
			}
			else
			{
				//Error
				SoundManager.Instance.PlayEffect("eff_block_miss");
			}
		}

		if( bNFCBlock && bTouchBlock )
		{
			btnOK.interactable = true;
		}
		else
		{
			btnOK.interactable = false;
		}
	}

	void SetWordInfo()
	{
		countWord = 0;
		if( World.Instance.indexWordSet == 0 ) //ABCD
		{
			countWord += World.Instance.wordItems_Success[0].listWords.Count;
			countWord += World.Instance.wordItems_Success[1].listWords.Count;
			countWord += World.Instance.wordItems_Success[2].listWords.Count;
			countWord += World.Instance.wordItems_Success[3].listWords.Count;
		}
		else if( World.Instance.indexWordSet == 1 )	//EFGH
		{
			countWord += World.Instance.wordItems_Success[4].listWords.Count;
			countWord += World.Instance.wordItems_Success[5].listWords.Count;
			countWord += World.Instance.wordItems_Success[6].listWords.Count;
			countWord += World.Instance.wordItems_Success[7].listWords.Count;
		}
		else if( World.Instance.indexWordSet == 2 )	//IJKL
		{
			countWord += World.Instance.wordItems_Success[8].listWords.Count;
			countWord += World.Instance.wordItems_Success[9].listWords.Count;
			countWord += World.Instance.wordItems_Success[10].listWords.Count;
			countWord += World.Instance.wordItems_Success[11].listWords.Count;
		}
		else if( World.Instance.indexWordSet == 3 )	//MNOP
		{
			countWord += World.Instance.wordItems_Success[12].listWords.Count;
			countWord += World.Instance.wordItems_Success[13].listWords.Count;
			countWord += World.Instance.wordItems_Success[14].listWords.Count;
			countWord += World.Instance.wordItems_Success[15].listWords.Count;
		}
		else if( World.Instance.indexWordSet == 4 )	//QRSTU
		{
			countWord += World.Instance.wordItems_Success[16].listWords.Count;
			countWord += World.Instance.wordItems_Success[17].listWords.Count;
			countWord += World.Instance.wordItems_Success[18].listWords.Count;
			countWord += World.Instance.wordItems_Success[19].listWords.Count;
			countWord += World.Instance.wordItems_Success[20].listWords.Count;
		}
		else if( World.Instance.indexWordSet == 5 )	//VWXYZ
		{
			countWord += World.Instance.wordItems_Success[21].listWords.Count;
			countWord += World.Instance.wordItems_Success[22].listWords.Count;
			countWord += World.Instance.wordItems_Success[23].listWords.Count;
			countWord += World.Instance.wordItems_Success[24].listWords.Count;
			countWord += World.Instance.wordItems_Success[25].listWords.Count;
		}
		textInfo.text = "(현재 <color=red>"+countWord.ToString("n0")+"</color>개의 단어를 학습하였어요.)";

	}
	
	void Update()
	{
		#if UNITY_EDITOR
		
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			if (hit) 
			{
				BlockItem _blockItem = hit.transform.GetComponent<BlockItem>();
				if( _blockItem != null )
				{
					_blockItem.SetBlockImage();
					//btnOK.interactable = true;
					bNFCBlock = true;

					if( bNFCBlock && bTouchBlock )
						btnOK.interactable = true;
				}
			}
		}
		#endif
	}
}
