﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class CtrGameScene : MonoBehaviour {
	
	public CtrGameCharacter ctrCharacter;

	public CtrAlphabet ctrAlphabet;
	public GameObject objStart;
	public Image imgPic;
	public Text textAlphabet01;
	public Text textAlphabet02;
	public GameObject objButtonSound;
	public GameObject objTouchGuide;
	public Button btnTouch;
	public Button btnNFC;

	public GameObject[] objNotices;

	public List<string> strTestWords  = new List<string>();
	public List<string> strTestWordTypes  = new List<string>();

	public int indexWordSet = 0;

	private int totalTestCount = 10;
	private int indexTest = 0;

	private List<string> _strWord01 = new List<string>();
	private List<string> _strWord02 = new List<string>();
	private List<string> _strWord03 = new List<string>();
	private List<string> _strWord04 = new List<string>();
	private List<string> _strWord05 = new List<string>();

	private bool bTouchBlock = true;
	private bool bProcessNFCTag = false;
	
	private bool bFirstWord = true;

	private int indexLastWordType;
	private List<int> listTypeNumber = new List<int>();
	// Use this for initialization
	void Start () {
		//indexWordSet = Random.Range(0,6);
		indexWordSet = World.Instance.indexWordSet;

		if( indexWordSet >= 4 )
			indexLastWordType =Random.Range(0,5);
		else 
			indexLastWordType =Random.Range(0,4);
		
		btnTouch.interactable = false;
		bFirstWord = true;
//		GenerateWordList(indexWordSet);
//		SetWord(indexTest);
//		SetGameMode();
	}

	public void SetGameMode()
	{
		SoundManager.Instance.StopEffectSound();
		ctrCharacter.PlayTalk(17);
	}
	public void ClickStart()
	{
		ctrCharacter.StopTalk();
		ctrCharacter.bDisappearBalloon = true;
		ctrCharacter.objBalloon.SetActive(false);

		
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		
		StartCoroutine(StartGameCo());
	}
	
	IEnumerator StartGameCo()
	{
		yield return new WaitForSeconds(0.5f);
		objStart.GetComponent<CanvasGroup>().DOFade(0f,0.5f).OnComplete(()=>{objStart.SetActive(false);});

		GenerateWordList(indexWordSet);
		SetWord(indexTest);
	}
	public void ClickSound()
	{
		string _sound = "nar_english_"+ctrAlphabet.strTypeWord.ToLower()
			+"_"+ctrAlphabet.strWord.ToLower();
		Debug.Log ("ClickSound "+_sound);
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect(_sound,1f);
	}
	
	
	public void ClickHome()
	{
		SoundManager.Instance.StopEffectSound();
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.LoadLevel("3.MainScene");
	}
	
	public void ClickTouch()
	{
//#if UNITY_EDITOR
		if( bTouchBlock ) return;
		//if( !btnTouch.interactable ) return;

		if( MgrGameScene.Instance.bTouchBlockMode )
		{
			SoundManager.Instance.StopEffectSound();
			SoundManager.Instance.PlayEffect("button_hit");
			objTouchGuide.SetActive(false);
		}

		bTouchBlock = true;
		btnTouch.interactable = false;

		StartCoroutine(NextWordCo());
//#endif
	}
	
	public void ClickCorrectWord()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		CorrectWord();
	}
	public void ClickWrongWord()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		WrongWord();
	}

	public void CorrectWord()
	{
		if( !bProcessNFCTag ) return;
		bProcessNFCTag = false;

		SoundManager.Instance.PlayEffect("eff_correct");
		ctrCharacter.PlayTalk(Random.Range(0,8));
		ctrAlphabet.PlayAnimCorrectWord();
		StartCoroutine(CorrectWordCo());

		World.Instance.SaveCorrectWord(
			ctrAlphabet.strTypeWord.ToUpper() ,ctrAlphabet.strWord.ToUpper());

	}
	IEnumerator CorrectWordCo()
	{
		yield return new WaitForSeconds(SoundManager.Instance.lengthSound);
		string _type = ctrAlphabet.strTypeWord.ToLower();
		string _sound = "nar_english_"+_type+_type+_type
			+"_"+ctrAlphabet.strWord.ToLower();
		//Debug.Log ("CorrectWord "+_sound);
		SoundManager.Instance.PlayEffect(_sound,1f);
		objButtonSound.SetActive(true);
		yield return new WaitForSeconds(SoundManager.Instance.lengthSound+1f);

		if( bFirstWord )
		{
			bFirstWord = false;
			ctrCharacter.PlayTalk(22);
		}
		else
			ctrCharacter.PlayTalk(23);


		//if( first ) ctrCharacter.PlayTalk(22);
		//else
		//ctrCharacter.PlayTalk(23);
		btnTouch.interactable = true;
		if( MgrGameScene.Instance.bTouchBlockMode )
			objTouchGuide.SetActive(true);
		bTouchBlock = false;
	}
	public void WrongWord()
	{
		if( !bProcessNFCTag ) return;
		bProcessNFCTag = false;

		SoundManager.Instance.PlayEffect("eff_wrong");
		ctrCharacter.PlayTalk(Random.Range(8,12));
		ctrAlphabet.PlayAnimWrongWord();
		StartCoroutine(WrongWordCo());
	}
	
	IEnumerator WrongWordCo()
	{
		yield return new WaitForSeconds(ctrCharacter.lengthSound+1.5f);
		ctrAlphabet.StopAnimWrongWord();
//		yield return new WaitForSeconds(0.3f);
//		ctrAlphabet.StopAnimWrongWord();
		//StartCoroutine(NextWordCo());	
		if( bFirstWord )
		{
			bFirstWord = false;
			ctrCharacter.PlayTalk(22);
		}
		else
			ctrCharacter.PlayTalk(23);

		btnTouch.interactable = true;
		if( MgrGameScene.Instance.bTouchBlockMode )
			objTouchGuide.SetActive(true);
		bTouchBlock = false;
	}

	IEnumerator NextWordCo()
	{
		//yield return new WaitForSeconds(1.5f);

		ctrAlphabet.PlayAnimClose();
		imgPic.GetComponent<CanvasGroup>().DOFade(0f,1f).SetDelay(0.5f);
		yield return new WaitForSeconds(1.5f);
		++indexTest;
		if( indexTest != totalTestCount )
		{
			SetWord(indexTest);
		}
		else
		{
			//Finish
			GenerateWordList(indexWordSet);
			SetWord(indexTest);
		}

		//bTouchBlock = false;

	}

	void SetWord( int _indexTest )
	{
		//ctrAlphabet.GetAlphabet("DINOSAUR","D");
		ctrAlphabet.GetAlphabet(strTestWords[_indexTest],strTestWordTypes[_indexTest] );
		string _name = "Images/ill_english_"+ctrAlphabet.strTypeWord.ToLower()
			+"_"+ctrAlphabet.strWord.ToLower();
		//Debug.Log ("SetWord "+_name);
		imgPic.sprite = Resources.Load<Sprite>(_name);// as Sprite;

		imgPic.GetComponent<CanvasGroup>().DOFade(1f,1f).SetDelay(1f);
		textAlphabet01.text = ctrAlphabet.strTypeWord.ToUpper();
		textAlphabet02.text = ctrAlphabet.strTypeWord.ToLower();
		
		objButtonSound.SetActive(false);
		
		StartCoroutine(ShowWordCo());
	}
	IEnumerator ShowWordCo()
	{
		yield return new WaitForSeconds(1.5f);
		
		bProcessNFCTag = true;
		SoundManager.Instance.PlayEffect("eff_alphabet_show");
	}
	
	public void ReceiveData( string _rawdata)
	{
		Debug.Log ("ReceiveData "+_rawdata);
		char[] _data = new char[17];
		_data = _rawdata.ToCharArray();
		
		int _x = int.Parse(_data[2].ToString())-1;
		int _y = int.Parse(_data[3].ToString())-1;
		
		int _state = int.Parse(_data[6].ToString());	

		int _idTag = int.Parse(_data[13].ToString())*100 
			+ int.Parse(_data[14].ToString())*10 +  int.Parse(_data[15].ToString());
		int _tagstate = int.Parse(_data[16].ToString());

		
		if( _data[5].ToString() == "N" )//NFC Block Check
		{
			if( _state == 1 )//Add
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetNFCBlockImage();
				//bNFCBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
				btnNFC.interactable = true;
				objNotices[0].SetActive(false);
			}
			else if(_state == 3)// Hold
			{
				if( _tagstate == 1 )
				{
					string _strTag = World.Instance.GetAlphabetFromTag(_idTag);

					if( strTestWordTypes[indexTest].ToUpper() == _strTag.ToUpper())
					{
						CorrectWord();
					}
					else
					{
						WrongWord();
					}
				}


			}
		}
		else if( _data[5].ToString() == "T")//Touch Block Check
		{
			if( _state == 1 )//Add
			{
				MgrGameScene.Instance.blocks[_y].block[_x].SetTouchBlockImage();
				//bTouchBlock = true;
				SoundManager.Instance.PlayEffect("eff_block_put");
				btnTouch.interactable = true;
				objNotices[1].SetActive(false);
			}
			
			if(  _state == 3)//Add, Hold
			{
				//Touch Input
				ClickTouch();
			}
		}
		else
		{
			if(_state == 2) // Remove
			{
				MgrGameScene.Instance.blocks[_y].block[_x].DisableBlockImage();
				if( MgrGameScene.Instance.blocks[_y].block[_x].bNFCMode )
				{
					//bNFCBlock = false;
					btnNFC.interactable = false;
					SoundManager.Instance.StopEffectSound();
					ctrCharacter.PlayTalk(19);
					objNotices[0].SetActive(true);
				}
				else
				{
					//bTouchBlock = false;
					btnTouch.interactable = false;
					SoundManager.Instance.StopEffectSound();
					ctrCharacter.PlayTalk(20);
					objNotices[1].SetActive(true);
				}
			}
			else
			{
				//Error
				//SoundManager.Instance.PlayEffect("eff_block_miss");
			}
		}

		
	}

	void GenerateWordList( int _indexWordSet)
	{
		Debug.Log ("GenerateWordList "+_indexWordSet);
		strTestWords.Clear();
		strTestWordTypes.Clear();
		_strWord01.Clear();
		_strWord02.Clear();
		_strWord03.Clear();
		_strWord04.Clear();
		_strWord05.Clear();

		List<List<string>> array_list = new List<List<string>>();
		array_list.Add (_strWord01);
		array_list.Add (_strWord02);
		array_list.Add (_strWord03);
		array_list.Add (_strWord04);
		array_list.Add (_strWord05);

		indexTest = 0;

		int _index = 0;
		int _countWordSet = 4;
		//int _indexArrayList = 0;
		int _indexStart = 0;
		
		if( _indexWordSet == 0 )
		{
			_countWordSet = 4;
			_indexStart = 0;
		}
		else if( _indexWordSet == 1 )
		{
			_countWordSet = 4;
			_indexStart = 4;
		}
		else if( _indexWordSet == 2 )
		{
			_countWordSet = 4;
			_indexStart = 8;
		}
		else if( _indexWordSet == 3 )
		{
			_countWordSet = 4;
			_indexStart = 12;
		}
		else if( _indexWordSet == 4 )
		{
			_countWordSet = 5;
			_indexStart = 16;
		}
		else if( _indexWordSet == 5 )
		{
			_countWordSet = 5;
			_indexStart = 21;
		}

		for( int _indexArrayList = 0; _indexArrayList < _countWordSet; ++_indexArrayList)
		{
			for( _index = 0; _index < World.Instance.wordItems[_indexArrayList+_indexStart].listWords.Count; ++_index)
			{
				array_list[_indexArrayList].Add(World.Instance.wordItems[_indexArrayList+_indexStart].listWords[_index]);
			}
		}

		ResetIndexTypeNumberList(indexLastWordType,_countWordSet);

		int _indexWordType = 0;
		for( int i = 0; i < totalTestCount; ++i )
		{
			_indexWordType = listTypeNumber[Random.Range(0,listTypeNumber.Count)];
			indexLastWordType = _indexWordType;
			AddTestWord( _indexWordType);
			ResetIndexTypeNumberList(_indexWordType,_countWordSet);
		}
	}

	void ResetIndexTypeNumberList( int _preIndex, int _totalCount)
	{
		listTypeNumber.Clear();
		for( int _index = 0; _index < _totalCount; ++_index)
		{
			if( _preIndex != _index )
				listTypeNumber.Add(_index);
		}
	}

	void AddTestWord( int _wordType )
	{
		int _index =0;
		if( _wordType == 0 )
		{
			_index = Random.Range(0,_strWord01.Count);
			strTestWords.Add ( _strWord01[_index] );
			_strWord01.Remove(_strWord01[_index]);
			if( indexWordSet == 0 )
				strTestWordTypes.Add("A");
			else if( indexWordSet == 1 )
				strTestWordTypes.Add("E");
			else if( indexWordSet == 2 )
				strTestWordTypes.Add("I");
			else if( indexWordSet == 3 )
				strTestWordTypes.Add("M");
			else if( indexWordSet == 4 )
				strTestWordTypes.Add("Q");
			else if( indexWordSet == 5 )
				strTestWordTypes.Add("V");
		}
		else if( _wordType == 1 )
		{
			_index = Random.Range(0,_strWord02.Count);
			strTestWords.Add ( _strWord02[_index] );
			_strWord02.Remove(_strWord02[_index]);
			if( indexWordSet == 0 )
				strTestWordTypes.Add("B");
			else if( indexWordSet == 1 )
				strTestWordTypes.Add("F");
			else if( indexWordSet == 2 )
				strTestWordTypes.Add("J");
			else if( indexWordSet == 3 )
				strTestWordTypes.Add("N");
			else if( indexWordSet == 4 )
				strTestWordTypes.Add("R");
			else if( indexWordSet == 5 )
				strTestWordTypes.Add("W");
		}
		else if( _wordType == 2 )
		{
			_index = Random.Range(0,_strWord03.Count);
			strTestWords.Add ( _strWord03[_index] );
			_strWord03.Remove(_strWord03[_index]);
			if( indexWordSet == 0 )
				strTestWordTypes.Add("C");
			else if( indexWordSet == 1 )
				strTestWordTypes.Add("G");
			else if( indexWordSet == 2 )
				strTestWordTypes.Add("K");
			else if( indexWordSet == 3 )
				strTestWordTypes.Add("O");
			else if( indexWordSet == 4 )
				strTestWordTypes.Add("S");
			else if( indexWordSet == 5 )
				strTestWordTypes.Add("X");
		}
		else if( _wordType == 3 )
		{
			_index = Random.Range(0,_strWord04.Count);
			strTestWords.Add ( _strWord04[_index] );
			_strWord04.Remove(_strWord04[_index]);
			if( indexWordSet == 0 )
				strTestWordTypes.Add("D");
			else if( indexWordSet == 1 )
				strTestWordTypes.Add("H");
			else if( indexWordSet == 2 )
				strTestWordTypes.Add("L");
			else if( indexWordSet == 3 )
				strTestWordTypes.Add("P");
			else if( indexWordSet == 4 )
				strTestWordTypes.Add("T");
			else if( indexWordSet == 5 )
				strTestWordTypes.Add("Y");
		}
		else if( _wordType == 4 )
		{
			_index = Random.Range(0,_strWord05.Count);
			Debug.Log ("Word Type U ,Word "+_strWord05[_index]);
			strTestWords.Add ( _strWord05[_index] );
			_strWord05.Remove(_strWord05[_index]);
		
			if( indexWordSet == 4 )
				strTestWordTypes.Add("U");
			else if( indexWordSet == 5 )
				strTestWordTypes.Add("Z");
		}


	}

}
