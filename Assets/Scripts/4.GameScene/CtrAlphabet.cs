﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CtrAlphabet : MonoBehaviour {

	public string strTypeWord = "A";
	public string strWord = "ELEPHANT";
	public AlphabetItem[] itemAlphabets;
	public Sprite[] spriteBlueAlphabats;
	public Sprite[] spriteRedAlphabats;
	public string[] characters;
	public Image imgCorrectWord;

	private Vector3 posCorrect;
	private int indexStart = 0;
	private int indexRedAlphabet = 0;
	// Use this for initialization
	void Start () {

		//yield return new WaitForSeconds(0.5f);
		//GetAlphabet(strWord);
	}
	
//	// Update is called once per frame
//	void Update () {
//	
//	}


	public void GetAlphabet( string _text,  string _typeWord )
	{
		strTypeWord = _typeWord;
		strWord = _text;
		bool _bTypeWord = false;
		Vector3 _pos = Vector3.zero;
		SetAlphabetItemPos(_text.Length);

		string[] _characters = new string[_text.Length];


		//Debug.Log ("GetAlphabet _text.Length "+_text.Length);
		//Debug.Log ("GetAlphabet indexStart "+indexStart);
		for (int i = 0; i < _text.Length; i++)
		{ 
			_characters[i] = _text[i].ToString();
			if( _bTypeWord )
			{
				itemAlphabets[indexStart+i].imgAlphabet.sprite = 
					spriteBlueAlphabats[GetLetter(_characters[i])];
			}
			else
			{
				if( strTypeWord == _characters[i] || strTypeWord.ToLower() == _characters[i] )
				{
					_bTypeWord = true;
					indexRedAlphabet = indexStart+i;
					itemAlphabets[indexStart+i].imgAlphabet.sprite = 
						spriteRedAlphabats[GetLetter(_characters[i])];
					imgCorrectWord.sprite =
						spriteBlueAlphabats[GetLetter(_characters[i])];
					posCorrect = itemAlphabets[indexStart+i].transform.position;
					_pos = posCorrect;
					_pos.y = -100f;
					imgCorrectWord.transform.position = _pos;
				}
				else
				{
					itemAlphabets[indexStart+i].imgAlphabet.sprite = 
						spriteBlueAlphabats[GetLetter(_characters[i])];
				}
			}
			itemAlphabets[indexStart+i].delayTurn = 0.5f+(0.1f*(float)i);
			itemAlphabets[indexStart+i].AnimTurn();
		}
		
		for( int i = 0; i < itemAlphabets.Length; ++i )
		{
			if( i < indexStart || i >= (indexStart+_text.Length) )
			{
				itemAlphabets[i].SetDefaultCard();
				Debug.Log ("CloseCard Index "+i);
			}
		}
		
		imgCorrectWord.DOFade(1f,0f);
		imgCorrectWord.rectTransform.DOLocalRotate(Vector3.zero, 0f);

		characters = _characters;
		StartCoroutine(CheckCloseCard());
		//imgCorrectWord.transform.DOMove(posCorrect, 1f).SetDelay(2f);
	}
	IEnumerator CheckCloseCard()
	{
		yield return new WaitForSeconds(1.5f);
		for( int i = 0; i < itemAlphabets.Length; ++i )
		{
			if( i < indexStart || i >= (indexStart+characters.Length) )
			{
				itemAlphabets[i].SetDefaultCard();
				Debug.Log ("CloseCard Index "+i);
			}
		}
		Debug.Log ("CheckCloseCard() ");
	}
	
	public void PlayAnimWrongWord()
	{
		itemAlphabets[indexRedAlphabet].WrongWord();
	}
	public void StopAnimWrongWord()
	{
		itemAlphabets[indexRedAlphabet].StopFadeInOut();
	}

	public void PlayAnimCorrectWord()
	{
		imgCorrectWord.transform.DOMove(posCorrect, 0.5f).SetEase(Ease.Linear);
	}

	public void PlayAnimClose()
	{
		for( int i = 0; i < itemAlphabets.Length; ++i )
		{
			if(itemAlphabets[i].transform.eulerAngles.y != 0f)
				itemAlphabets[i].AnimClose();
		}
		
		imgCorrectWord.rectTransform.DOLocalRotate(new Vector3(0f,180f,0f), 1f).SetDelay(0.5f);
		imgCorrectWord.DOFade(0f,0.5f).SetDelay(0.5f);
	}

	public void SetAlphabetItemPos( int _wordCount )
	{
		if( _wordCount == 3 )
		{
			indexStart = 2;
		}
		else if(_wordCount == 4)
		{
			indexStart = 2;
		}
		else if(_wordCount == 5)
		{
			indexStart = 1;
		}
		else if(_wordCount == 6)
		{
			indexStart = 1;
		}
		else if(_wordCount == 7)
		{
			indexStart = 0;
		}
		else if(_wordCount == 8)
		{
			indexStart = 0;
		}

	}

	public int GetLetter( string _str )
	{ 
		int _index = 0;
		if(_str == "A" || _str == "a" ) _index = 0;
		else if(_str == "B"|| _str == "b" ) _index = 1;
		else if(_str == "C"|| _str == "c" ) _index = 2;
		else if(_str == "D"|| _str == "d" ) _index = 3;
		else if(_str == "E"|| _str == "e" ) _index = 4;
		else if(_str == "F"|| _str == "f" ) _index = 5;
		else if(_str == "G"|| _str == "g" ) _index = 6;
		else if(_str == "H"|| _str == "h" ) _index = 7;
		else if(_str == "I"|| _str == "i" ) _index = 8;
		else if(_str == "J"|| _str == "j" ) _index = 9;
		else if(_str == "K"|| _str == "k" ) _index = 10;
		else if(_str == "L"|| _str == "l" ) _index = 11;
		else if(_str == "M"|| _str == "m" ) _index = 12;
		else if(_str == "N"|| _str == "n" ) _index = 13;
		else if(_str == "O"|| _str == "o" ) _index = 14;
		else if(_str == "P"|| _str == "p" ) _index = 15;
		else if(_str == "Q"|| _str == "q" ) _index = 16;
		else if(_str == "R"|| _str == "r" ) _index = 17;
		else if(_str == "S"|| _str == "s" ) _index = 18;
		else if(_str == "T"|| _str == "t" ) _index = 19;
		else if(_str == "U"|| _str == "u" ) _index = 20;
		else if(_str == "V"|| _str == "v" ) _index = 21;
		else if(_str == "W"|| _str == "w" ) _index = 22;
		else if(_str == "X"|| _str == "x" ) _index = 23;
		else if(_str == "Y"|| _str == "y" ) _index = 24;
		else if(_str == "Z"|| _str == "z" ) _index = 25;

		return _index;
	}

}
