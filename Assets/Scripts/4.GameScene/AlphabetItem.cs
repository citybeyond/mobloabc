﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class AlphabetItem : MonoBehaviour {
	
	public Image imgDefalut;
	public Image imgAlphabet;
	public float delayTurn = 0.5f;

	private bool bFadeInOut = false;
	private Transform myTransform;
	// Use this for initialization
	void Awake () {
		myTransform = this.transform;
	}

	public void WrongWord()
	{
		bFadeInOut = true;
		FadeInWord();
	}
	void FadeInWord()
	{
		if( bFadeInOut )
			imgAlphabet.DOFade(1f,0.3f).OnComplete(FadeOutWord);
	}
	void FadeOutWord()
	{
		if( bFadeInOut )
			imgAlphabet.DOFade(0f,0.3f).OnComplete(FadeInWord);
	}
	public void StopFadeInOut()
	{
		bFadeInOut = false;
		imgAlphabet.DOFade(1f,0.1f).SetDelay(0.3f);
//		Color _color = imgAlphabet.color;
//		_color.a = 1f;
//		imgAlphabet.color = _color;
	}

	public void AnimTurn()
	{
		bFadeInOut = false;
		myTransform.DOLocalRotate( new Vector3(0f,180f,0f), 0.5f).SetDelay(delayTurn);
		imgDefalut.DOFade(0f,1f).SetDelay(delayTurn);
		imgAlphabet.DOFade(1f,1f).SetDelay(delayTurn);
		StartCoroutine(TurnSoundCo());

	}
	IEnumerator TurnSoundCo()
	{
		yield return new WaitForSeconds(delayTurn);
		SoundManager.Instance.PlayEffect("eff_card_open");
	}

	public void AnimClose()
	{
		bFadeInOut = false;
		myTransform.DOLocalRotate( Vector3.zero, 1f).SetDelay(0.5f);
		imgAlphabet.DOFade(0f,0.5f).SetDelay(0.5f);
		imgDefalut.DOFade(1f,0f);
	}
	public void SetDefaultCard()
	{
		Color _color = imgDefalut.color;
		_color.a = 1f;
		imgDefalut.color = _color;

		_color = imgAlphabet.color;
		_color.a = 0f;
		imgAlphabet.color = _color;
		
//		myTransform.DOLocalRotate( Vector3.zero, 0.1f);
		myTransform.eulerAngles = Vector3.zero;
		imgDefalut.transform.eulerAngles = Vector3.zero;
		imgAlphabet.transform.eulerAngles = new Vector3( 0f, 180f,0f);
		bFadeInOut = false;
	}
//	// Update is called once per frame
//	void Update () {
//	
//	}
}
