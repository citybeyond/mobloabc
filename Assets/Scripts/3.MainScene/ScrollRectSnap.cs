﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ScrollRectSnap : UIBehaviour, IDragHandler, IEndDragHandler//, IBeginDragHandler
{
	public ScrollRect scrollRect;
	public RectTransform m_rectTransformContent;
	#region Interfaces
//	public void OnBeginDrag(PointerEventData eventData)
//	{
////		Debug.Log("OnBeginDrag");
//		scrollRect.inertia = true;
//		StopCoroutine(EndDragCo());
//	}
	
	public void OnEndDrag(PointerEventData eventData)
	{
//		Debug.Log("OnEndDrag");
		StartCoroutine(EndDragCo());
	}

	public void OnDrag(PointerEventData eventData)
	{
		scrollRect.inertia = true;
		//Debug.Log("OnDrag");
		StopCoroutine(EndDragCo());
		
	}
	#endregion

	IEnumerator EndDragCo()
	{
		yield return new WaitForSeconds(0.1f);
		scrollRect.inertia = false;
		//Debug.Log("OnEndDrag1 "+m_rectTransformContent.anchoredPosition);
		int _val01 = (int)(m_rectTransformContent.anchoredPosition.x/230f);
		float _val02 = m_rectTransformContent.anchoredPosition.x%230f;
//		Debug.Log("_val01 "+_val01);
//		Debug.Log("_val02 "+_val02);
//		if( Mathf.Abs(_val02) > 100f )
//			++_val01;
		float _val = 230f*_val01;

		float _distance = _val - m_rectTransformContent.anchoredPosition.x;
		if( _distance < 100f  &&  _distance > -100f )
		{
			m_rectTransformContent.anchoredPosition = new Vector2 (230f*_val01, 0);
		}
		else if(_distance <= -100f) 
		{
			m_rectTransformContent.anchoredPosition = new Vector2 (230f*(_val01+1), 0);
		}
		else if(_distance >= 100f)
		{
			m_rectTransformContent.anchoredPosition = new Vector2 (230f*(_val01-1), 0);
		}

		
//		Debug.Log("OnEndDrag2 "+m_rectTransformContent.anchoredPosition);
	}
}

