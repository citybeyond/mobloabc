﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimHandSilder : MonoBehaviour {

	private Transform myTransform;
	// Use this for initialization
	void Start () {
		myTransform = this.transform;

		AnimLeft();
	}

	void AnimLeft()
	{
		myTransform.DOLocalMoveX(-150f,3f).OnComplete(AnimRight);
	}
	void AnimRight()
	{
		myTransform.DOLocalMoveX(150f,3f).OnComplete( AnimLeft);
	}
	
//	// Update is called once per frame
//	void Update () {
//	
//	}
}
