﻿using UnityEngine;
using System.Collections;

public class CtrMain : MonoBehaviour {
	
	public CtrGameCharacter ctrCharacter;

	public GameObject objGuide;
	public GameObject objExit;
	// Use this for initialization
	void Start () {
		
		int _index = World.Instance.indexWordSet;
		SoundManager.Instance.PlayBGM("S_SLP01_Tr12_01 A Sunny Day",0.5f);

		if( !World.Instance.bDetailCardTestMode )
			StartCoroutine(SoundCharacterCo());
	}

	IEnumerator SoundCharacterCo()
	{
		yield return new WaitForSeconds(1f);
		//원하는 알파벳을 선택해 주세요.
		SoundManager.Instance.StopEffectSound();
		ctrCharacter.PlayTalk(14);
		//SoundManager.Instance.PlayEffect("nar_english_moble_direct3_choice",1f);
	}
	
	public void ClickClose()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objExit.SetActive(true);

	}
	
	public void ClickTest()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.bDetailCardTestMode = false;
		World.Instance.LoadLevel("5.TestScene");
	}

	public void ClickGuide()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objGuide.SetActive(true);
	}
	public void ClickCloseGuide()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objGuide.SetActive(false);
	}
	public void ClickExitCancel()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objExit.SetActive(false);
		
	}
	public void ClickExitOK()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		Application.Quit();
		
	}

	public void ChangeItem()
	{
		Debug.Log ("ChangeItem");
	}
	// Update is called once per frame
//	void Update () {
//		if(Application.platform == RuntimePlatform.Android)
//		{
//			if(Input.GetKey(KeyCode.Escape))
//			{
//				//Application.Quit();
//				ClickClose();
//			}
//		}
//	}
}
