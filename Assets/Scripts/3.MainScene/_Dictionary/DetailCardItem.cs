﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DetailCardItem : MonoBehaviour {
	
	public string strType;
	public string strWord;
	public GameObject objDetailCard;
	public Text textWord;
	public Image imgPic;
//	// Use this for initialization
//	void Start () {
//	
//	}

	public void EnableCard()
	{
		textWord.text = strWord;
		objDetailCard.SetActive(true);
		
		string _name = "Images/ill_english_"+strType.ToLower()+"_"+strWord.ToLower();
		//Debug.Log ("DetailCardItem "+_name);
		imgPic.sprite = Resources.Load<Sprite>(_name);// as Sprite;
	}
	public void DisableCard()
	{
		objDetailCard.SetActive(false);
	}

	public void ClickSound()
	{
		SoundManager.Instance.StopEffectSound();
		string _sound = "nar_english_"+strType.ToLower()+"_"+strWord.ToLower();
		SoundManager.Instance.PlayEffect(_sound,1f);
	}

	public void ClickTest()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.bDetailCardTestMode = true;
		World.Instance.strType_DetailCard = strType;
		World.Instance.strWord_DetailCard = strWord;
		World.Instance.LoadLevel("5.TestScene");
	}
}
