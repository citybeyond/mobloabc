﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CtrDictionary : MonoBehaviour {

	public GameObject objDictionary;
	public GameObject objDetailCard;
	public Text textDicButton;
	public DictionaryItem[] dicItems;
	public DetailCardItem[] cardItems;

	public bool bTestMode = false;
	private int indexAlphabet;
	// Use this for initialization
	IEnumerator Start () {
		
		for( int i = 0 ; i < dicItems.Length; ++i )
		{
			dicItems[i].indexItem = i;
			dicItems[i].ctrDic = this;
		}
		
		if( World.Instance.bDetailCardTestMode )
		{
			objDictionary.SetActive(true);
			objDetailCard.SetActive(true);
			indexAlphabet = World.Instance.GetLetter(World.Instance.strType_DetailCard);
			SetDetailCard();
		}

		yield return new WaitForSeconds(0.5f);
		textDicButton.text = World.Instance.GetTotalCountDictionaryWord().ToString()
			+"/"+World.Instance.GetTotalCountWord().ToString();

		SetDictionaryItem();
	}

	
	public void ClickOpenDic()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objDictionary.SetActive (true);
		//SetDictionaryItem();

	}
	public void ClickCloseDic()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objDictionary.SetActive (false);
		World.Instance.bDetailCardTestMode = false;
	}
	public void ClickCloseCard()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objDetailCard.SetActive (false);
		World.Instance.bDetailCardTestMode = false;
	}

	public void ClickDicItem( int _index)
	{
		Debug.Log ("ClickDicItem "+_index);
		indexAlphabet = _index;
		objDetailCard.SetActive(true);
		SetDetailCard();
	}

	public void SetDictionaryItem()
	{
		int _totalCount = 0;
		int _countEnableCard = 0;
		if( bTestMode )
		{
			for( int i = 0 ; i < dicItems.Length; ++i )
			{			
				_totalCount = World.Instance.wordItems[i].listWords.Count;
				_countEnableCard = _totalCount;
				if( _countEnableCard == _totalCount )
					dicItems[i].objMedal.SetActive(true);
				else 
					dicItems[i].objMedal.SetActive(false);
				
				dicItems[i].textCount.text = _countEnableCard.ToString()+"/"+_totalCount.ToString();
			}
			return;
		}
		for( int i = 0 ; i < dicItems.Length; ++i )
		{			
			_totalCount = World.Instance.wordItems[i].listWords.Count;
			_countEnableCard = 0;
			for( int j = 0; j < _totalCount; ++j )
			{
				if( PlayerPrefs.GetInt( "Test_"+i.ToString()+"_"+j.ToString(), 0 ) == 1 )
				{	
					++_countEnableCard;
				}
			}
			
			//_countEnableCard = _totalCount;
			Debug.Log("dicItems[i] "+i);
			Debug.Log("_countEnableCard "+_countEnableCard);
			Debug.Log("_totalCount "+_totalCount);
			if( _countEnableCard == _totalCount )
				dicItems[i].objMedal.SetActive(true);
			else 
				dicItems[i].objMedal.SetActive(false);
				
			dicItems[i].textCount.text = _countEnableCard.ToString()+"/"+_totalCount.ToString();
		}
	}

	public void SetDetailCard()
	{
		//World.Instance.wordItems[indexAlphabet].listWords
		int _count = World.Instance.wordItems[indexAlphabet].listWords.Count;
		int _countEnableCard = 0;
		
		if( bTestMode )
		{
			if( _count == 7 )
			{
				for( int i = 0; i < _count; ++i )
				{
					cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
					cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[i];
					cardItems[i].EnableCard();
				}
			}
			else if( _count == 5 )
			{
				int _index = 0;
				for( int i = 0; i < cardItems.Length; ++i )
				{
					if( i != 3 && i != 6 )
					{
						cardItems[i].gameObject.SetActive(true);
						
						cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
						cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[_index];
						cardItems[i].EnableCard();
						++_index;
					}
					else
						cardItems[i].gameObject.SetActive(false);
				}
			}
			return;
		}
		if( _count == 7 )
		{
			for( int i = 0; i < _count; ++i )
			{
				cardItems[i].gameObject.SetActive(true);
				
//				cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
//				cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[i];
//				cardItems[i].EnableCard();
				if( PlayerPrefs.GetInt( "Test_"+indexAlphabet.ToString()+"_"+i.ToString(), 0 ) == 1 )
				{
					cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
					cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[i];
					cardItems[i].EnableCard();
					++_countEnableCard;
				}
				else
					cardItems[i].DisableCard();
			}
		}
		else if( _count == 5 )
		{
			int _index = 0;
			for( int i = 0; i < cardItems.Length; ++i )
			{
				if( i != 3 && i != 6 )
				{
					cardItems[i].gameObject.SetActive(true);
//					
//					cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
//					cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[_index];
//					cardItems[i].EnableCard();
					if( PlayerPrefs.GetInt( "Test_"+indexAlphabet.ToString()+"_"+_index.ToString(), 0 ) == 1 )
					{

						cardItems[i].strType = World.Instance.GetAlphabet(indexAlphabet);
						cardItems[i].strWord = World.Instance.wordItems[indexAlphabet].listWords[_index];
						cardItems[i].EnableCard();
						++_countEnableCard;
					}
					else
						cardItems[i].DisableCard();
					++_index;
				}
				else
				{
					cardItems[i].gameObject.SetActive(false);
				}
			}
		}
		Debug.Log("_countEnableCard "+_countEnableCard);
		Debug.Log("_totalCount "+_count);

	}










}
