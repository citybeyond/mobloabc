﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DictionaryItem : MonoBehaviour {

	public CtrDictionary ctrDic;
	public int indexItem;
	public Text textCount;
	public GameObject objMedal;
	public Image imgDim;

	// Use this for initialization
	void Start () {
		imgDim.sprite = this.GetComponent<Image>().sprite;
		//btnItem.onClick.AddListener(delegate{this.ClickDicItem();});
	}
	
	public void ClickDicItem()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		imgDim.gameObject.SetActive(true);
		StartCoroutine(ClickItemCo());
	}
	IEnumerator ClickItemCo()
	{
		yield return new WaitForSeconds(0.1f);
		imgDim.gameObject.SetActive(false);
		ctrDic.ClickDicItem(indexItem);
	}

}
