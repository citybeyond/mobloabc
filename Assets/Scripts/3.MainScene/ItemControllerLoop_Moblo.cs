﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof( InfiniteScroll))]
public class ItemControllerLoop_Moblo : UIBehaviour, IInfiniteScrollSetup
{
	private bool isSetuped = false;

	public void OnPostSetupItems ()
	{
		GetComponentInParent<ScrollRect> ().movementType = ScrollRect.MovementType.Unrestricted;
		isSetuped = true;
	}

	public void OnUpdateItem (int itemCount, GameObject obj)
	{
		if( isSetuped == true ) 
			return;

		var item = obj.GetComponentInChildren<Item_Moblo> ();
		item.UpdateItem (itemCount);
	}
	public void OnEndDrag()
	{
		Debug.Log ("OnEndDrag ");
		
//		float _val = Mathf.Abs( centerScreen - myTransform.position.x );
//		if( _val < 30f )
//		{
//			myTransform.position = new Vector3( centerScreen, myTransform.position.y, myTransform.position.z);
//		}
	}
}
