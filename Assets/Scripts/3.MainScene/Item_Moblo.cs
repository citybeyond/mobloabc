﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class Item_Moblo : UIBehaviour {

	[SerializeField]
	Image uiIcon;
	//Image uiBackground;
	public GameObject objClickArea;
	public Transform transformObject;
	public Transform transformContent;
	public Transform transformCenter;
	public Transform transformEnd;
	public Sprite[] spriteIcons;
	private Transform myTransform;
	public int indexItem;
	private float centerScreen;
	private float endScale;

	private float width;
	void Start()
	{
		myTransform = this.transform;
		centerScreen = transformCenter.position.x;
		endScale = transformEnd.position.x;

		width = Mathf.Abs(centerScreen-endScale);

	}

	public void ClickPlayGame()
	{
		if( transformObject.localScale.x < 0.9f) return;

		uiIcon.transform.DOScale(myTransform.localScale*1.1f,0.1f).SetEase(Ease.OutExpo);
		uiIcon.transform.DOScale(myTransform.localScale*1f,0.1f).SetEase(Ease.OutCubic).SetDelay(0.1f);
		if( indexItem != 0 )
			World.Instance.indexWordSet = indexItem-1;
		else
			World.Instance.indexWordSet = 5;

		SoundManager.Instance.PlayEffect("button_hit");
		World.Instance.LoadLevel("4.GameScene");
	}
	public void Drag()
	{
		Update();
	}

	public void UpdateItem (int count) 
	{
		indexItem = count;
		uiIcon.sprite = spriteIcons[count];
	}

	void Update()
	{
//		if( indexItem == 1 )
//		{
//			Debug.Log ("indexItem "+indexItem);
//			//Debug.Log ("Pos "+(myTransform.localPosition-transformContent.position));
//			Debug.Log ("Pos "+myTransform.position);
//			Debug.Log ("centerScreen "+centerScreen);
//		}

		float _val = Mathf.Abs( centerScreen - myTransform.position.x );
		//float _rate = 1f - _val*0.004f;
		float _rate = 1f - 0.6f*(_val/width);
		if( _rate < 0.4f )
			_rate = 0.4f;

		transformObject.localScale = Vector3.one*_rate;

		if( transformObject.localScale.x < 0.9f)
		{
			if( objClickArea.activeSelf )
				objClickArea.SetActive(false);
		}	
		else
		{
			if( !objClickArea.activeSelf )
				objClickArea.SetActive(true);
		}
	
	}

}
