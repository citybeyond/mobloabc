﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CtrLogo : MonoBehaviour {

	public Slider sliderLoading;
	// Use this for initialization
	IEnumerator Start () {
		
		SoundManager.Instance.PlayBGM("S_SLP01_Tr12_01 A Sunny Day",0.5f);
		SoundManager.Instance.PlayEffect("nar_english_moble_direct1",1f);

		sliderLoading.DOValue(1f,2.0f);
		World.Instance.Start();
		yield return new WaitForSeconds(SoundManager.Instance.lengthSound);
		GoToMainScene();
	}

	void GoToMainScene()
	{
		World.Instance.LoadLevel("3.MainScene");
	}

}
