﻿using UnityEngine;
using System.Collections;

public class Controller_Guide : MonoBehaviour {

	public GameObject[] objGuideDetail;

	// Use this for initialization
	void Start () {
	
	}

	public void ClickCloseGuide()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		gameObject.SetActive(false);
	}
	
	public void ClickGuide( int _index)
	{
		SoundManager.Instance.PlayEffect("button_hit");
		objGuideDetail[_index].SetActive(true);
	}


	public void ClickGuideDetailClose()
	{
		SoundManager.Instance.PlayEffect("button_hit");
		for( int i = 0; i < objGuideDetail.Length; ++i )
		{
			objGuideDetail[i].SetActive(false);
		}
	}
}
