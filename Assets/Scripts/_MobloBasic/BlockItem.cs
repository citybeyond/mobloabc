﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlockItem : MonoBehaviour {
	
	public int x;
	public int y;
	
	public Image imgBlock;
	
	public Sprite[] spriteBlocks;
	public bool bNFCMode;

	void Start()
	{
		//imgBlock = this.transform.GetChild(0).GetComponent<Image>();
	}
	public void SetNFCBlockImage()
	{
		imgBlock.sprite = spriteBlocks[0];
		imgBlock.gameObject.SetActive(true);
		bNFCMode = true;
		
	}
	public void SetTouchBlockImage()
	{
		imgBlock.sprite = spriteBlocks[1];
		imgBlock.gameObject.SetActive(true);
		bNFCMode = false;
	}
	public void DisableBlockImage()
	{
		imgBlock.gameObject.SetActive(false);
	}
	
	public void SetBlockImage()
	{
		if( !imgBlock.gameObject.activeSelf )
		{
			imgBlock.gameObject.SetActive(true);
		}
		else
			imgBlock.gameObject.SetActive(false);
		
	}
	
}
