﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonScale : MonoBehaviour {

	private Transform myTransform;
	private Vector3 vectorOrignScale;
	// Use this for initialization
	void Start () {
		myTransform = this.transform;
		vectorOrignScale = myTransform.localScale;
		//yield return new WaitForSeconds(0.5f);
		this.GetComponent<Button>().onClick.AddListener(delegate {this.CallScale();});
	}

	void CallScale()
	{
		myTransform.DOScale(vectorOrignScale*1.1f,0.1f).SetEase(Ease.OutExpo);
		myTransform.DOScale(vectorOrignScale*1f,0.1f).SetEase(Ease.OutCubic).SetDelay(0.1f);
	}

}
