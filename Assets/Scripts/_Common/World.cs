﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WordItem
{
	public string strWordType;
	public List<string> listWords = new List<string>();
}

[System.Serializable]
public class BlockListItem  
{  
	public BlockItem[] block;    
}  

public class World : Singleton<World> 
{
	public int iTestScore;
	public int indexWordSet;

	public bool bDetailCardTestMode = false;
	public string strType_DetailCard;
	public string strWord_DetailCard;
	
	public WordItem[] wordItems;
	public WordItem[] wordItems_Success;

	// Use this for initialization
	public void Start () {
		
		SetWordItemData();
		SetSuccessWordItemData();
	}
	
	public void LoadLevel(string _name)
	{
		StartCoroutine(LoadLevelCo(_name));
	}
	
	IEnumerator LoadLevelCo(string _name)
	{
		yield return new WaitForSeconds(0.5f);
		Application.LoadLevel(_name);
		
	}

	
	public void SaveCorrectWord( string _wordType, string _word )
	{
		int _indexWordType = GetLetter(_wordType);
		int _indexWord = GetWordIndex(_indexWordType, _word );
		if( PlayerPrefs.GetInt( _indexWordType.ToString()+"_"+_indexWord.ToString(), 0 ) == 0 )
		{
			PlayerPrefs.SetInt( _indexWordType.ToString()+"_"+_indexWord.ToString(), 1 );
			wordItems_Success[_indexWordType].strWordType = _wordType;
			wordItems_Success[_indexWordType].listWords.Add(_word);
		}
	}

	public void SaveCorrectWord_Test( string _wordType, string _word )
	{
		int _indexWordType = GetLetter(_wordType);
		int _indexWord = GetWordIndex(_indexWordType, _word );
		if( PlayerPrefs.GetInt( "Test_"+_indexWordType.ToString()+"_"+_indexWord.ToString(), 0 ) == 0 )
		{
			PlayerPrefs.SetInt( "Test_"+_indexWordType.ToString()+"_"+_indexWord.ToString(), 1 );
//			wordItems_Success[_indexWordType].strWordType = _wordType;
//			wordItems_Success[_indexWordType].listWords.Add(_word);
		}
	}
	void SetSuccessWordItemData()
	{
		wordItems_Success = new WordItem[26];
		for( int i = 0; i < wordItems_Success.Length;++i )
		{
			wordItems_Success[i] = new WordItem();
			wordItems_Success[i].strWordType = GetAlphabet(i);

			for( int j = 0; j < wordItems[i].listWords.Count; ++j )
			{
				if( PlayerPrefs.GetInt( i.ToString()+"_"+j.ToString(), 0 ) == 1 )
				{
					wordItems_Success[i].listWords.Add(wordItems[i].listWords[j]);
				}
			}
		
		}

	}
	public int GetTotalCountWord()
	{
		int _count = 0;
		for( int i = 0; i < wordItems.Length;++i )
		{
			_count += wordItems[i].listWords.Count;
		}
		return _count;
	}
	public int GetTotalCountDictionaryWord()
	{
		int _count = 0;
		for( int i = 0; i < wordItems.Length;++i )
		{
			for( int j = 0; j < wordItems[i].listWords.Count; ++j )
			{
				if( PlayerPrefs.GetInt( "Test_"+i.ToString()+"_"+j.ToString(), 0 ) == 1 )
					++_count;
			}
		}
		return _count;
	}
	public int GetTotalCountSuccessWord()
	{
		int _count = 0;
		for( int i = 0; i < wordItems_Success.Length;++i )
		{
			_count += wordItems_Success[i].listWords.Count;
		}
		return _count;
	}
	public int GetWordIndex( int _indexWordType, string _word)
	{
		List<string> _listWords = wordItems[_indexWordType].listWords;
		//for( int i = 0; i < wordItems[_indexWordType].listWords.Count; ++i )
		for( int i = 0; i < _listWords.Count; ++i )
		{
			if( _listWords[i] == _word )
			{
				return i;
			}
		}
		return -1;
	}
	
	public string GetAlphabet( int _index )
	{ 
		if( _index == 0 ) return "A";
		else if( _index == 1 ) return "B";
		else if( _index == 2 ) return "C";
		else if( _index == 3 ) return "D";
		else if( _index == 4 ) return "E";
		else if( _index == 5 ) return "F";
		else if( _index == 6 ) return "G";
		else if( _index == 7 ) return "H";
		else if( _index == 8 ) return "I";
		else if( _index == 9 ) return "J";
		else if( _index == 10 ) return "K";
		else if( _index == 11 ) return "L";
		else if( _index == 12 ) return "M";
		else if( _index == 13 ) return "N";
		else if( _index == 14 ) return "O";
		else if( _index == 15 ) return "P";
		else if( _index == 16 ) return "Q";
		else if( _index == 17 ) return "R";
		else if( _index == 18 ) return "S";
		else if( _index == 19 ) return "T";
		else if( _index == 20 ) return "U";
		else if( _index == 21 ) return "V";
		else if( _index == 22 ) return "W";
		else if( _index == 23 ) return "X";
		else if( _index == 24 ) return "Y";
		else if( _index == 25 ) return "Z";

		return "A";

	}

	public int GetLetter( string _str )
	{ 
		int _index = 0;
		if(_str == "A" || _str == "a" ) _index = 0;
		else if(_str == "B"|| _str == "b" ) _index = 1;
		else if(_str == "C"|| _str == "c" ) _index = 2;
		else if(_str == "D"|| _str == "d" ) _index = 3;
		else if(_str == "E"|| _str == "e" ) _index = 4;
		else if(_str == "F"|| _str == "f" ) _index = 5;
		else if(_str == "G"|| _str == "g" ) _index = 6;
		else if(_str == "H"|| _str == "h" ) _index = 7;
		else if(_str == "I"|| _str == "i" ) _index = 8;
		else if(_str == "J"|| _str == "j" ) _index = 9;
		else if(_str == "K"|| _str == "k" ) _index = 10;
		else if(_str == "L"|| _str == "l" ) _index = 11;
		else if(_str == "M"|| _str == "m" ) _index = 12;
		else if(_str == "N"|| _str == "n" ) _index = 13;
		else if(_str == "O"|| _str == "o" ) _index = 14;
		else if(_str == "P"|| _str == "p" ) _index = 15;
		else if(_str == "Q"|| _str == "q" ) _index = 16;
		else if(_str == "R"|| _str == "r" ) _index = 17;
		else if(_str == "S"|| _str == "s" ) _index = 18;
		else if(_str == "T"|| _str == "t" ) _index = 19;
		else if(_str == "U"|| _str == "u" ) _index = 20;
		else if(_str == "V"|| _str == "v" ) _index = 21;
		else if(_str == "W"|| _str == "w" ) _index = 22;
		else if(_str == "X"|| _str == "x" ) _index = 23;
		else if(_str == "Y"|| _str == "y" ) _index = 24;
		else if(_str == "Z"|| _str == "z" ) _index = 25;
		
		return _index;
	}

	void SetWordItemData()
	{
		wordItems = new WordItem[26];
		for( int i = 0; i < wordItems.Length;++i )
			wordItems[i] = new WordItem();

		wordItems[0].listWords.Add("APPLE");
		wordItems[0].listWords.Add("ANT");
		wordItems[0].listWords.Add("ALBUM");
		wordItems[0].listWords.Add("ALPHABET");
		wordItems[0].listWords.Add("ACORN");
		wordItems[0].listWords.Add("ANGEL");
		wordItems[0].listWords.Add("AVOCADO");
		
		wordItems[1].listWords.Add("BAG");
		wordItems[1].listWords.Add("BUS");
		wordItems[1].listWords.Add("BED");
		wordItems[1].listWords.Add("BAT");
		wordItems[1].listWords.Add("BOOK");
		wordItems[1].listWords.Add("BLUE");
		wordItems[1].listWords.Add("BIRD");
		
		wordItems[2].listWords.Add("CAT");
		wordItems[2].listWords.Add("CUP");
		wordItems[2].listWords.Add("CAP");
		wordItems[2].listWords.Add("CAR");
		wordItems[2].listWords.Add("COW");
		wordItems[2].listWords.Add("CRAB");
		wordItems[2].listWords.Add("CAMERA");
		
		wordItems[3].listWords.Add("DOG");
		wordItems[3].listWords.Add("DUCK");
		wordItems[3].listWords.Add("DESK");
		wordItems[3].listWords.Add("DOOR");
		wordItems[3].listWords.Add("DISH");
		wordItems[3].listWords.Add("DINOSAUR");
		wordItems[3].listWords.Add("DOLPHIN");
		
		wordItems[4].listWords.Add("ELEPHANT");
		wordItems[4].listWords.Add("EGG");
		wordItems[4].listWords.Add("ELBOW");
		wordItems[4].listWords.Add("EYE");
		wordItems[4].listWords.Add("EIGHT");
		wordItems[4].listWords.Add("EAR");
		wordItems[4].listWords.Add("EAGLE");
		
		wordItems[5].listWords.Add("FROG");
		wordItems[5].listWords.Add("FISH");
		wordItems[5].listWords.Add("FAN");
		wordItems[5].listWords.Add("FLOWER");
		wordItems[5].listWords.Add("FOOT");
		wordItems[5].listWords.Add("FIVE");
		wordItems[5].listWords.Add("FOUR");
		
		wordItems[6].listWords.Add("GORILLA");
		wordItems[6].listWords.Add("GRAPE");
		wordItems[6].listWords.Add("GOAT");
		wordItems[6].listWords.Add("GLASSES");
		wordItems[6].listWords.Add("GREEN");
		wordItems[6].listWords.Add("GIRAFFE");
		wordItems[6].listWords.Add("GUITAR");
		
		wordItems[7].listWords.Add("HIPPO");
		wordItems[7].listWords.Add("HAT");
		wordItems[7].listWords.Add("HEN");
		wordItems[7].listWords.Add("HAND");
		wordItems[7].listWords.Add("HORSE");
		wordItems[7].listWords.Add("HOUSE");
		wordItems[7].listWords.Add("HAIR");
		
		wordItems[8].listWords.Add("IGLOO");
		wordItems[8].listWords.Add("INSECT");
		wordItems[8].listWords.Add("IGUANA");
		wordItems[8].listWords.Add("INK");
		wordItems[8].listWords.Add("CHAIR");
		wordItems[8].listWords.Add("RAIN");
		wordItems[8].listWords.Add("SHIRT");
		
		wordItems[9].listWords.Add("JET");
		wordItems[9].listWords.Add("JAM");
		wordItems[9].listWords.Add("JACKET");
		wordItems[9].listWords.Add("JEANS");
		wordItems[9].listWords.Add("JAGUAR");
		wordItems[9].listWords.Add("JUICE");
		wordItems[9].listWords.Add("JEWELRY");

		
		wordItems[10].listWords.Add("KING");
		wordItems[10].listWords.Add("KEY");
		wordItems[10].listWords.Add("KITE");
		wordItems[10].listWords.Add("KIWI");
		wordItems[10].listWords.Add("KETTLE");
		wordItems[10].listWords.Add("KNIFE");
		wordItems[10].listWords.Add("CAKE");
		
		wordItems[11].listWords.Add("LION");
		wordItems[11].listWords.Add("LEG");
		wordItems[11].listWords.Add("LEMON");
		wordItems[11].listWords.Add("LAMP");
		wordItems[11].listWords.Add("LARK");
		wordItems[11].listWords.Add("BLOCK");
		wordItems[11].listWords.Add("CAMEL");
		
		wordItems[12].listWords.Add("MAP");
		wordItems[12].listWords.Add("MAN");
		wordItems[12].listWords.Add("MOP");
		wordItems[12].listWords.Add("MELON");
		wordItems[12].listWords.Add("MOUSE");
		wordItems[12].listWords.Add("MOUTH");
		wordItems[12].listWords.Add("MILK");
		
		wordItems[13].listWords.Add("NUT");
		wordItems[13].listWords.Add("NET");
		wordItems[13].listWords.Add("NOSE");
		wordItems[13].listWords.Add("NEST");
		wordItems[13].listWords.Add("NAVY");
		wordItems[13].listWords.Add("NURSE");
		wordItems[13].listWords.Add("NINE");
		
		wordItems[14].listWords.Add("OCTOPUS");
		wordItems[14].listWords.Add("OSTRICH");
		wordItems[14].listWords.Add("TOMATO");
		wordItems[14].listWords.Add("ORANGE");
		wordItems[14].listWords.Add("ONE");
		wordItems[14].listWords.Add("MOON");
		wordItems[14].listWords.Add("OIL");
		
		wordItems[15].listWords.Add("PANDA");
		wordItems[15].listWords.Add("PEN");
		wordItems[15].listWords.Add("PIANO");
		wordItems[15].listWords.Add("PRINCE");
		wordItems[15].listWords.Add("PIG");
		wordItems[15].listWords.Add("PENCIL");
		wordItems[15].listWords.Add("POTATO");
		
		wordItems[16].listWords.Add("QUEEN");
		wordItems[16].listWords.Add("QUESTION");
		wordItems[16].listWords.Add("SQUIRREL");
		wordItems[16].listWords.Add("AQUA");
		wordItems[16].listWords.Add("MOSQUITO");
		
		wordItems[17].listWords.Add("RABBIT");
		wordItems[17].listWords.Add("RING");
		wordItems[17].listWords.Add("ROBOT");
		wordItems[17].listWords.Add("RED");
		wordItems[17].listWords.Add("ROSE");
		wordItems[17].listWords.Add("RADIO");
		wordItems[17].listWords.Add("RACCOON");
		
		wordItems[18].listWords.Add("SWAN");
		wordItems[18].listWords.Add("SOCKS");
		wordItems[18].listWords.Add("SEA");
		wordItems[18].listWords.Add("SNAKE");
		wordItems[18].listWords.Add("SHOES");
		wordItems[18].listWords.Add("SAND");
		wordItems[18].listWords.Add("SEVEN");
		
		wordItems[19].listWords.Add("TEN");
		wordItems[19].listWords.Add("TIGER");
		wordItems[19].listWords.Add("TRAIN");
		wordItems[19].listWords.Add("TREE");
		wordItems[19].listWords.Add("TWO");
		wordItems[19].listWords.Add("THREE");
		wordItems[19].listWords.Add("STAR");

		wordItems[20].listWords.Add("UMBRELLA");
		wordItems[20].listWords.Add("UNIFORM");
		wordItems[20].listWords.Add("UFO");
		wordItems[20].listWords.Add("COMPUTER");
		wordItems[20].listWords.Add("UNCLE");
		wordItems[20].listWords.Add("DRUM");
		wordItems[20].listWords.Add("SUN");

		
		wordItems[21].listWords.Add("VASE");
		wordItems[21].listWords.Add("VEST");
		wordItems[21].listWords.Add("VIOLIN");
		wordItems[21].listWords.Add("VIOLET");
		wordItems[21].listWords.Add("TV");
		wordItems[21].listWords.Add("VEHICLE");
		wordItems[21].listWords.Add("VENUS");
		
		wordItems[22].listWords.Add("WATCH");
		wordItems[22].listWords.Add("WORLD");
		wordItems[22].listWords.Add("WINDOW");
		wordItems[22].listWords.Add("WINTER");
		wordItems[22].listWords.Add("WALLET");
		wordItems[22].listWords.Add("WHALE");
		wordItems[22].listWords.Add("WHEEL");
		
		wordItems[23].listWords.Add("BOX");
		wordItems[23].listWords.Add("FOX");
		wordItems[23].listWords.Add("SIX");
		wordItems[23].listWords.Add("XRAY");
		wordItems[23].listWords.Add("OX");
		
		wordItems[24].listWords.Add("YELLOW");
		wordItems[24].listWords.Add("YOYO");
		wordItems[24].listWords.Add("MONKEY");
		wordItems[24].listWords.Add("BOY");
		wordItems[24].listWords.Add("TOY");
		wordItems[24].listWords.Add("BABY");
		wordItems[24].listWords.Add("FLY");
		
		wordItems[25].listWords.Add("ZEBRA");
		wordItems[25].listWords.Add("ZERO");
		wordItems[25].listWords.Add("ZOO");
		wordItems[25].listWords.Add("PIZZA");
		wordItems[25].listWords.Add("ZIGZAG");
		wordItems[25].listWords.Add("ZIPPER");
		wordItems[25].listWords.Add("PUZZLE");
	}
	
	public string GetAlphabetFromTag( int _index )
	{ 
		if( _index == 65 ) return "A";
		else if( _index == 66 ) return "B";
		else if( _index == 67 ) return "C";
		else if( _index == 68 ) return "D";
		else if( _index == 69 ) return "E";
		else if( _index == 70 ) return "F";
		else if( _index == 71 ) return "G";
		else if( _index == 72 ) return "H";
		else if( _index == 73 ) return "I";
		else if( _index == 74 ) return "J";
		else if( _index == 75 ) return "K";
		else if( _index == 76 ) return "L";
		else if( _index == 77 ) return "M";
		else if( _index == 78 ) return "N";
		else if( _index == 79 ) return "O";
		else if( _index == 80 ) return "P";
		else if( _index == 81 ) return "Q";
		else if( _index == 82 ) return "R";
		else if( _index == 83 ) return "S";
		else if( _index == 84 ) return "T";
		else if( _index == 85 ) return "U";
		else if( _index == 86 ) return "V";
		else if( _index == 87 ) return "W";
		else if( _index == 88 ) return "X";
		else if( _index == 89 ) return "Y";
		else if( _index == 90 ) return "Z";
		
		return "None";
		
	}

}
