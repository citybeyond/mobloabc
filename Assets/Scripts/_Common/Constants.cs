﻿using UnityEngine;

public class Constants
{
	public const float		DEFAULT_VOLUME				= 0.5f;
	public const string		PATH_EFFECT 				= "Sounds/";
	public const string 	PATH_SOUND					= "Sounds/";
//	public const int		MAX_EQUIPMENT				= 4;
//	public const int		MAX_INVENTORY				= 17;
//	public const int		MAX_WAREHOUSE				= 10;
//	public const int		MAX_MIXSLOT					= 3;
//	public const int		MAX_NEWITEM_WEAPON			= 5;
//	public const int		MAX_NEWITEM_SHEILD			= 5;
//	public const int		MAX_NEWITEM_ACC				= 10;

//	#region - Scene Name
//	public const string		Lobby		= "03_Lobby";
//	public const string		GamePlay	= "04_GamePlay";
//	#endregion
//	
//	static public Color32	ColorHealth			= new Color32(239, 34, 34, 255);
//	static public Color32	ColorLuck			= new Color32(60, 164, 32, 255);
//	static public Color32	ColorAttackDamage	= new Color32(231, 111, 26, 255);
//	static public Color32	ColorArmor			= new Color32(2, 123, 232, 255);
}